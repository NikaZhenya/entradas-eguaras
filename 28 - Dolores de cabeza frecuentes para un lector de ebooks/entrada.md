# Dolores de cabeza frecuentes para un lector de _ebooks_

En entradas anteriores he hablado sobre los dolores de cabeza frecuentes
para [un editor](https://marianaeguaras.com/dolores-de-cabeza-frecuentes-para-un-editor-de-ebooks/)
o [un cliente](https://marianaeguaras.com/dolores-de-cabeza-frecuentes-para-un-cliente-de-ebooks/)
de _ebooks_. Pero ¿qué podemos decir respecto a un lector de libros
electrónicos?

Distintos fabricantes de _ereaders_ o distribuidores de _ebooks_ nos prometen
maravillas sobre esta otra manera de leer. Sin embargo, la experiencia tiende
a ser accidentada para quienes crecieron leyendo en papel.

# 1. El libro electrónico es feo

No hay pretextos: el _ebook_ es feo. En algunos casos el diseño es tan garrafal
que se opta por no leerlo. ¿Quién o qué es el culpable?

Si piensas que es una limitación del formato, lamento decirte que no es así.
Al menos no para el estándar de los libros electrónicos: el +++EPUB+++.
Tampoco es, de manera general, culpa de la editorial.

**El _ebook_ es feo porque no es un formato prioritario**. En México los
libros electrónicos aún no llegan al 5% de la venta de libros. Con probabilidad
en España el porcentaje es mayor, pero no lo suficiente para ser una prioridad
entre los editores.

En ferias y charlas escucharás decir que el futuro del libro son los
«contenidos» digitales, como los _ebooks_ y los audiolibros. Pero por desgracia
el grueso del sector aún está aprendiendo lo que esto implica más allá de los
soportes finales.

Desde hace años y durante varios más como industria seguiremos en transición
porque los elementos mínimos para desarrollar publicaciones digitales con
alta calidad requieren volver a aprender muchas cosas. Entre ellas, que
_[la edición se hizo código](https://perrotuerto.blog/)_, y ya no solo
un sector anclado en las artes gráficas.

Mientras tanto, como lector puedes escribirle a las editoriales para
convencerlos de que un _ebook_ bien diseñado es uno con mejor posibilidades de
venta…

# 2. El cuidado editorial brilla por su ausencia

¿Cuántos _ebooks_ no te ha tocado leer con demasiadas erratas o «errores» en
la formación? Quizá más de lo que se encuentra en publicaciones impresas.

Los «errores» de la formación ---como viudas, huérfanas, ríos o callejones---
tienen su origen en que el _ebook_ simplemente no tiene formación. Lo que ves
al abrir un libro electrónico es una renderización que se hace al instante y
a partir de las pautas que se señalaron en una hoja de estilos más las 
características del dispositivo y tus preferencias guardadas.

Por ejemplo, como editores no colocamos un salto de página y luego
ponemos un encabezado de 16 pts. centrado, con márgenes arriba y abajo para
indicarte que ese es el título de una sección. En su lugar, el contenido lo
marcamos para señalar que es un encabezado. Con una hoja de estilo indicamos
que cada vez que el dispositivo vaya a renderizar un encabezado, lo haga con
pautas de diseño como esta:

```
h1 {
  font-size: 1.33em;
  text-align: center;
  margin: 3em auto;
}
```

No hay necesidad de entender el código porque solo es para evidenciar que
carecemos de elementos visuales para la «formación» de un _ebook_. ¿Y el salto
de página? Lo obtenemos al trabajar cada sección de la obra como un documento
distinto.

A esto suma que habrá renderizadores que quizá tengan especificaciones muy
puntuales y que no podemos cambiar. Ejemplos claros son el manejo de las
imágenes; según el tamaño del dispositivo y las características de su pantalla,
en algunos casos las imágenes se verán mejor o peor a lo que veríamos en una
pantalla de computadora.

Entonces, **no contamos con formación, sino con pautas de diseño que el
dispositivo mostrará sin que nosotros podamos interferir**.

Ahora bien, ¿por qué parece que existen más erratas en el _ebook_ que en el
libro impreso? No es tanto por quien lo edita o lo desarrolla ---hasta la
_Ortografía_ de la +++RAE+++ muestra este problema---, sino la manera en como
se produce el _ebook_.

Los métodos más comunes para obtener un libro electrónico son [la conversión o el desarrollo](https://marianaeguaras.com/cual-es-la-diferencia-entre-conversion-de-ebooks-y-desarrollo-de-ebooks/).
Con la conversión de manera automatizada pasa de un formato hecho para impreso
a uno digital. Ya puedes anticipar los horrores que esto acarrea…

En el desarrollo, como la mayoría de los editores emplean un proceso [cíclico](https://marianaeguaras.com/edicion-ciclica-y-edicion-ramificada-de-la-antesala-a-la-revolucion-digital/)
de edición en lugar de uno [ramificado](https://marianaeguaras.com/edicion-ciclica-y-edicion-ramificada-del-surgimiento-al-tronco-y-las-ramas/),
tenemos un proceso muy accidentado que se evidencia con una pérdida de calidad
editorial.

Como lector eres el principal afectado de estos errores metodológicos y
por lo general los editores solo se dan cuenta de estas fallas después de
varias quejas por parte de sus lectores.

Por este motivo es muy relevante que como lector de _ebook_ exijas una mejor
calidad en tus libros: solo así harás que las editoriales repiensen sus
procesos de producción.

# 3. El _ebook_ lo compro pero no es mío

En un evento sobre edición alguien muy famosa dijo que en la actualidad ya
no hay «propiedad» debido a que todo se gestiona mediante servicios y que la
edición debería seguir el mismo camino. Eso es mentira.

**La propiedad sigue existiendo, la diferencia es que ahora los propietarios
son cada vez menos y los arrendatarios cada vez más**. En los ecosistemas de
uso y desarrollo de _software_ ya es evidente. Se pasó de tener _hard
copies_ de los programas que comprábamos a estar pagando por suscripciones
mensuales o anuales para el acceso a un servicio en la nube o el uso de un
programa.

Y si bien los _ebooks_ no son _software_, sí han sido constreñidos a las
mismas dinámicas de acceso a la información. El caso más actual fue
[la decisión de Microsoft de cerrar su tienda de libros electrónicos](https://es-us.finanzas.yahoo.com/noticias/microsoft-cierra-tienda-books-elimina-165531071.html).
Todos los usuarios que tenían _ebooks_ en esa plataforma no los tendrán más:
serán borrados de sus dispositivos. Aunque por compensación Microsoft devolverá
el dinero gastado. El problema es que como lector esto hace patente que las
bibliotecas digitales son una ilusión…

Pero solo si «tu» biblioteca es en realidad una cuenta en una plataforma
como Amazon, Google o Apple. ¿Qué pasa si pierdes los accesos a tu cuenta?
¿Qué sucede si te la suspenden? ¿Qué harías si un día alguno de ellos hacen
lo mismo que Microsoft? Como lectores no queremos que nos devuelvan el dinero:
queremos los libros que adquirimos porque, bueno, se supone que son nuestros,
¿no?

Hace unos años la industria editorial había ido en contra del derecho «cero»
del lector: el derecho de tener un libro. Pennac no habló de este derecho
porque cuando escribió su ensayo supuso que el lector tenía libros por medio
de los impuestos que paga y que a su vez va a las bibliotecas públicas o por
compra directa. En la publicación de bits se hizo posible ser lectores sin
libros, consumidores sin bienes: clientes de plataformas.

El [+++DRM+++](https://es.wikipedia.org/wiki/Gesti%C3%B3n_de_derechos_digitales)
fue esa tecnología que vino a hacer patente la pérdida de ese derecho. Por
fortuna la industria editorial se ha percatado de su inutilidad y cada vez son
menos los editores que deciden poner candados digitales a _sus_ libros.

Pero aún es necesario que como lectores hagas explícito que no son _sus_ libros,
sino que por medio de una transacción económica ya son _tuyos_. Tienes el
derecho a tener una copia privada de _tus_ libros. Algo que el +++DRM+++ impide.

Los libros que compras te pertenecen, interpela a tus editoriales favoritas
para estas terminen con ese dolor de cabeza: todos pueden beneficiarse de ello.

# 4. Muchos clics para lo que se podría coger con la mano

También para mí es un suplicio el tener que mandar _ebooks_ a los dispositivos
que leo. Más si ya está empezado y tengo que estar actualizando el lugar donde
me quedé. Tan fácil que es tomar un libro, leerlo, ponerle un separador,
dejarlo y luego retomarlo.

Pero, de nuevo, no es un problema del libro electrónico o de quienes lo
desarrollan y editan. El detalle reside en la dependencia tecnológica que
implica ser un lector de _ebooks_.

No nos gusta admitirlo, pero es cierto: ese _ereader_, ese teléfono, esa
tableta o esa computadora tendrán menos vida útil que un libro impreso bien
cuidado. Cada ciertos años tendremos que adquirir un nuevo dispositivo y,
por supuesto, existirá de necesidad de respaldar y restaurar la información,
como nuestros libros.

Todo sería mucho más sencillo si existieran estándares que garanticen los menos
accidentes posibles. ¡Oh, espera! Sí los hay. **En las publicaciones digitales
existen estándares que garantizan una gran interoperabilidad**. Esto permite
que entre formatos, usuarios y dispositivos podamos acceder a la información
sin tantos clics.

El problema es que quienes fabrican los _ereaders_, desarrollan los programas
para leer _ebooks_ o venden los libros no les agrada seguirlos. El papel obliga
a muchos a seguir estándares porque es más barata la impresión siguiendo estas
reglas de juego. Sin embargo, en un contexto digital ha sido muy popular que
cada gigante tecnológico establezca sus propias reglas.

Con +++IBM++, Microsoft, Apple y demás pandilla de Silicon Valley se ha hecho
predominante un modelo de negocios cuyo principal medio de riqueza es que el
usuario tiene que estar adaptándose a cada servicio. Es como cuando compras
un boleto de avión muy barato pero terminas pagando lo mismo o más por el
equipaje y otros servicios extra que te parecen innecesarios pero que de igual
forma adquieres para mayor tranquilidad o por necesidad.

**Cada clic extra que a ti te toma un par de segundos, es una oportunidad más
de conocer más sobre ti y sobre cómo usas sus plataformas**. Lo que para
nosotros es una molestia, para ellos es la creación de perfiles más complejos
de usuarios o la generación de la idea de que _así es como funciona la
tecnología_, que les permite obtener mayores beneficios económicos.

Se trate de que te conozcan mejor para ofrecerte otros productos o servicios,
o sea porque a fuerza de costumbre te vuelves su cliente, como lector de
_ebooks_ podrías tener al menos la misma libertad que tienes al leer en papel,
pero por alguna «extraña» razón no es así.

# 5. No eres tú, no es el _ebook_, es el modelo de negocios

Al final los dolores de cabeza como lectores de libros electrónicos no son
tan «insólitos». El modelo predominante de negocios de la venta de
publicaciones reside en generarte molestias.

Como editores y desarrolladores de libros podríamos ofrecerte un mejor
material de lectura. El problema es que los tiempos y los presupuestos no son
posibles dentro de un ecosistema donde se da prioridad a la cantidad en lugar
de la calidad. También podríamos hacer más accesible la gestión de los _ebooks_,
el detalle es que en los puntos de venta que más visitas nos imponen
restricciones.

Tanto a ti, lector, como los que te ofrecemos lo que decides leer nos convendría
que ninguna persona tuviera dolores de cabeza. Pero eso exige una
infraestructura que no es muy afín a las restricciones cuyos accesos generan
riqueza.

**Como editor puedo comprometerme a ofrecerte un servicio lo menos tortuoso o
una obra lo mejor presentable para su lectura. No obstante, como lector es
conveniente que poco a poco exijas que los fabricantes o distribuidores
respeten estándares y fomenten la interoperabilidad**. Así, tendremos la
posibilidad de dar una cura a estos malestares.
