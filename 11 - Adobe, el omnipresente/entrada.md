# Adobe, el omnipresente

![](adobe.png)

Si se tiene que indicar un elemento tan anclado en la edición mexicana como el 
horror ante las viudas y huérfanas, sin titubeo podemos decir que es el uso de 
InDesign para la maquetación de obras. Lo mismo cabe decir del ámbito gráfico 
con Photoshop o Illustrator. Incluso es posible indicarlo para casi toda la 
producción cultural que se hace a través de una computadora: Adobe está presente 
en todos lados.

Aunque se parte del quehacer editorial en México, pienso que se puede 
generalizar al ecosistema editorial en español, aunque en el campo de las 
ciencias exactas el paradigma en la publicación sea TeX.

**¿Qué hay de malo con Adobe? Nada.** La intención de este artículo no va en
contra de los productos de Adobe, que en pequeña o gran escala han mostrado su
pertinencia en relación con la relativa corta curva de aprendizaje y los 
resultados «profesionales» obtenidos. Tampoco se pretende disuadir para el 
abandono de Adobe en pos de otras herramientas (libres) para el trabajo 
editorial.

De lo que se quisiera hablar es sobre cómo *un entorno de trabajo* ha pasado a 
ser *el método de trabajo* para la publicación, y los inconvenientes que 
*pueden* desprenderse de ello.

## Adobe como entorno de trabajo

Principios de los ochenta, Warnock y Geschke deciden abandonar Xerox para
iniciar el desarrollo de PostScript. Siguiendo lo que después sería la tradición 
de Silicon Valley, desde la cochera de Warnock se fundó su *startup*, la cual 
llevaría el mismo nombre al arroyo cercano: Adobe.

Al parecer en ese lugar se elaboraban ladrillos de adobe para la creciente
costa oeste. Siglos más tarde, en ese mismo lugar de empezarían a erigir varias
de las grandes empresas tecnológicas impulsoras de la era digital.

El éxito de PostScript fue tal que incluso desde su primer año de fundación
Adobe se convirtió en una empresa redituable. Semejantes resultados ayudaron a 
que PostScript se convirtiera en uno de los primeros estándares para la 
impresión digital.

Pero no se detuvieron ahí, con la intención de adentrarse en las oficinas, Apple 
y Adobe hicieron una alianza. En 1985 el fruto fue cosechado: la primera 
impresora láser y la primera impresora en red. La LaserWriter, manufacturada por 
Apple y con soporte PostScript, fue uno de los pilares para la idea de trabajar 
documentos de oficina en formato digital o para su posterior impresión.

Poco tiempo después Adobe decidió crear su propia especificación de fuentes
digitales nombrada Type 1. Esto desató que Apple creará su propia 
especificación, la TrueType, que posteriormente sería vendida a Microsoft.
La popularidad de la TrueType obligó a Adobe a abrir la especificación de Type 1 
y posteriormente llegar a un acuerdo con Microsoft para el desarrollo de una 
especificación abierta: OpenType.

Aunque esto puede entenderse como una batalla perdida, Adobe continúo 
orientándose al trabajo profesional que depende de la impresión. Así es como
a finales de los ochenta lanza Illustrator y luego Photoshop.

Con los programas de diseño y el PostScript, Adobe constituiría una base para su 
crecimiento, el cual se fue engrosando con la propuesta de una «oficina sin 
papeles»: el PDF.

El archivo PDF presentaba varias desventajas a otros formatos o tecnologías
existentes. Por un lado, su gran peso lo hacía poco adecuado para su uso en
la incipiente *web* y redes de oficinas. En cuanto uso como documento de 
impresión, la popularidad de PostScript lo opocaba.

La popularización del PDF fue lenta y debido al menos a los siguientes factores:

1. Mejoramiento de la infraestructura del internet, que permitió conexiones a
   mayores velocidades.
2. Ofrecimiento gratuito de Adobe Reader para el uso y lectura de archivos PDF.
3. Versatilidad al poder ver en la pantalla lo mismo que salía en el papel.

Con PostScript, Illustrator, Photoshop y PDF, Adobe ya estaba abarcando la
mayoría del mercado profesional para la impresión. Sin embargo, quedaba una
tarea pendiente: el desarrollo de una aplicación para la maquetación digital
de documentos, también conocida como *desktop publishing* (DTP).

La incursión de Adobe en ese mercado había sido infructuoso, por lo que a 
mediados de los noventa decidió adquirir la empresa encargada de desarrollar 
PageMaker. Cinco años después, en 1999, salió al mercado InDesign.

Casi diez años después, en 2008:

* InDesign se consolidó como el DTP por excelencia para la publicación digital. 
* El PDF se convirtió en un estándar ISO.
* Adobe After Effects ya había sido lanzado al mercado.
* Adobe se había expandido más allá de la esfera profesional centrada en la 
  impresión al adquirir Macromedia y, con ello, a Flash, Dreamweaver, Fireworks, 
  entre otros programas.

**Había mucho que festejar: los productos de Adobe ya estaban presentes en
cualquier tipo de proyecto cultural**. Y casi otros diez años después, el modo
Adobe se ha consolidado como la manera de crear productos culturales, desde 
gráfica, pasando por animación y video, hasta publicaciones.

## Adobe como método *de facto*

Lo que históricamente se ha constituido como una empresa para ofrecer un entorno
de trabajo para casi cualquier tipo de proyecto cultural, poco a poco fue 
mutando en un método.

Entre un entorno y un método hubo un intermediario muy significativo: el aparato
publicitario. **Adobe continuamente ha promocionado sus productos como una
solución**, como un entorno para profesionales y creativos de la edición, sea
gráfica, multimedia o textual. 

¿Cuántos editores profesionales no les agrada la idea de ser asociados con un 
entorno que da solución a su trabajo del día a día? ¿Cuántos de ellos no les 
agradan las ideas de «profesionalismo» y «libertad creativa»? ¿Cuántos, al 
final de cuentas, no prefieren una manera fácil de hacer las cosas?

Microsoft Office también es un entorno de trabajo, pero para soluciones «de
oficina». Sin embargo, la mayoría de las veces su uso se justifica por lo
práctico que es optar por formatos omnipresentes como el DOCX, XLSX o PPTX. 
En cambio, cuando se habla de Adobe, la practicidad nunca es por una cuestión
de formato, tan indiferente es esto que bien se trabaja con formatos abiertos
que privativos. La practicidad en Adobe es por la relativa facilidad y libertad
posible a través de sus programas para obtener productos con calidad 
profesional y gran flexibilidad creativa. 

Su paradigma no es una oficina, un cubículo o un empleado, pese a que la mayoría 
de las veces sus usuarios son de está índole; su estandarte es un espacio 
abierto, una mesa de trabajo y un profesionista independiente. ¿A cuántos 
diseñadores o editores no nos fascina esta idea?

Esta idea publicitaria necesita un fuerte soporte para ser significativa y
efectiva. De la estrategia que ha seguido Adobe para tener presencia en todo 
proyecto cultural, lo que me parece más interesante es su enfoque visual. Sus 
programas tienen, de un uso básico a intermedio, un enfoque primordialmente 
«WYSIWYG».

Aunque el término «WYSIWYG» se originó por una manera de tratar el texto 
digital, donde «lo que ves es lo que obtienes», en el caso de Adobe este enfoque 
se ha aplicado a casi cualquier esfera de la producción cultural. Lo que se hace 
en InDesign, Illustrator, Photoshop, etc., es lo que se obtiene en el archivo
final o en la impresión.

El trato del contenido digital, no solo para un buen resultado final, sino
también para su conservación y distribución, sin lugar a dudas también es una
cuestión metodológica: ¿qué pasos tenemos que seguir para tener un formato
apto para la producción pero que al mismo tiempo se preserve y nos permita
llevar a cabo nuestro trabajo?

El *software* ha ayudado a concretar la idea de centralización de cualquier
tipo de trabajo a través de una máquina. Sin embargo, como usuarios, este ideal 
es insuficiente, debido a que resulta molesto brincar de programa a programa, de 
formato a formato, de codificación a codificación.

Adobe ha permitido la radicalización de ese ideal al ofrecer en un solo entorno,
el Adobe Creative Cloud, la solución a proyectos de diversa índole. En un día
regular de trabajo quien diseña o edita se las ve con tres entornos:

* el de oficina, lo más seguro que con Microsoft Office;
* el explorador *web*, probablemente Chrome o Safari, y
* el de producción, la paquetería de Adobe.

La simplificación y centralización de entornos poco a poco ha impactado en la
manera de ejecutar un proyecto, hasta tal punto que si las tareas no se hacen
a través del *software* por *de facto*, surgirán problemas.

No obstante, no solo se espera que el profesionista aprenda este método de
trabajo. El modo Adobe no solo implica una exigencia al usuario, sino también
una normalización del uso de Adobe para incluso otros aspectos en el trabajo
que no fueron contemplados. El usuario promedio espera que Adobe ya tenga una
solución a sus necesidades, por lo cual solo sea necesario tomar una 
capacitación, como recurrentemente sucede cuando se me pide impartir talleres
sobre «libros electrónicos».

«Libros electrónicos» entrecomillado porque es común que se piense que la
imposibilidad de hacer publicaciones digitales es por una falta de 
«actualización» o un desconocimiento sobre alguna funcionalidad ya ofrecida por
Adobe. Sin embargo, el carácter problemático de este formato de publicación
es de una naturaleza distinta: el problema reside en el modo en como se ha
estado trabajando con una publicación. La dificultad nace cuando Adobe no es
capaz de dar una solución; sin embargo, como instructor el reto yace en
demostrar en solo unas cuantas horas que **Adobe no lo es todo**.

## Adobe y piratería

El carácter limitado de Adobe para desarrollar publicaciones digitales con 
calidad es un nicho relativamente muy pequeño en hablahispana, así que ese
problema, junto con muchos otros (como la digitalización y el OCR), quedan 
relegados. 

El grueso de los usuarios de Adobe, al menos en el mundo hispanohablante, es 
para el diseño gráfico, la edición de efectos especiales y el diseño editorial. 
Y durante años Adobe ha ofrecido y mejorado la manera en como se usa su
*software* para el cumplimiento de estas tareas. Por este motivo, los problemas
relativos a estos quehaceres no son comunes y, de existir, probablemente se 
deban a errores del usuario.

Esta practicidad y potencia no solo han popularizado los productos de Adobe,
también ha atraído algo «indeseable»: la piratería. El uso de *software*
pirata de Adobe es tan común, como habitual es encontrar que los diseñadores
gráficos o editores solo sepan utilizar Adobe para la consecución de sus
proyectos.

Esto no es ningún secreto, ni entre profesionales ni entre los ejecutivos
de Adobe. Una de las soluciones a las que se llegaron fue dejar de vender
el *software* como programa y, en su lugar, ofrecer una membresía mensual
para el uso de toda la paquetería, con descuentos para alumnos y profesores.

En una proyección a largo plazo, el uso de membresías es igual o incluso más
redituable que la venta individual de licencias de uso, ya que permite que
varios usuarios al fin puedan adquirir productos originales de Adobe. Sin duda
es un ganar-ganar.

No obstante, al menos en países en vías de desarrollo como México, aún con
esta caída de precios no es posible el uso legal de la paquetería de Adobe.
Para muchos diseñadores o editores el pago mensual de 49.99 USD tiene un
impacto directo en sus bolsillos. Por otro lado, el piratero de *software* es 
una práctica tan normalizada que incluso se lleva a cabo cuando no existe
necesidad.

Al final de cuentas, **la piratería de *software* no es tan indeseable** como
se pensaría en un principio. El modo Adobe no se centra en el uso legal o
ilegal del *software*, ni la buena salud del entorno de trabajo ofrecido por
esta compañía tiene sus bases en la adquisición de licencias de uso. 

Lo primordial en el modo Adobe es la prolongación de su método de trabajo. El
futuro de Adobe no reside en los programas que desarrolla, sino en la
terciarización de sus actividades. El mercado generado por Adobe no solo implica
competidores que también busquen desarrollar *software* semejante, sino también 
toda esa gama de soporte adicional que florece alrededor como los talleres, las 
certificaciones, las capacitaciones, el soporte técnico, etcétera.

**La piratería permite a Adobe la diversificación de sus actividades y el
mantenimiento de su estructura como el modo *de facto* en el diseño**. Si Adobe
no ha sido punitivo, como otras empresas de desarrollo de *software*, es debido 
a que existe una ventaja estratégica en la piratería de su *software*. Si Adobe 
ha decidido dejar de vender programas y en su lugar ofrecer membresías para su 
uso, es porque el modelo económico de la cultura de Silicon Valley apunta a uno 
basado en servicios. Si Adobe tomó la decisión de hacer del PDF un formato 
abierto, es debido a que su omnipresencia le ha permitido tener una cuota de
mercado sin necesidad mantenerlo privativo. 

Si en el futuro Adobe ofrece gratuitamente sus aplicaciones, será porque es el 
nuevo camino de la cultura de Silicon Valley (véase a Google, Apple e incluso a 
Windows, por ejemplo) y a que ya no existirá la necesidad de mantenerlos 
privados, debido a que las estrategias que se han estado tomando le permitirán 
ofrecer sus productos gratuitamente. Y la piratería ayuda a la consecución de 
estos objetivos. 

## Adobe y currículo

La omnipresencia de Adobe no solo es por su enfoque visual, por sus resultados
profesionales, por el ofrecimiento de un solo entorno, o por un modo de trabajo 
que se prolonga incluso con su piratería. Su presencia en todos lados también
tiene sus bases en dos factores:

1. El conocimiento de su paquetería es un requisito necesario para casi 
   cualquier vacante en el sector cultural.
2. Su aprendizaje no empieza en el «mundo real» del trabajo, sino desde la etapa
   de preparación profesional en universidades, institutos, diplomados o
   talleres.

Lo que Adobe ha provocado es lo que casi toda compañía tecnológica aspira, y lo
que varias instituciones públicas o privadas desearían: hacer del dominio del
*software* una cuestión pedagógica cuya carga curricular se dé durante la etapa
de preparación profesional. **Si el uso de la paquetería de Adobe se considera
«sencillo» e «intuitivo» en gran medida se debe a que su aprendizaje empieza
desde la época universitaria**. ¿Quién no recuerda el quebrantamiento de cabeza
que era entender el uso de cada una de las herramientas de InDesign, Photoshop
o Illustrator cuando se tenía una tarea o un examen? ¿Qué tan distinto sería la 
adopción de TeX en el mundo editorial si su aprendizaje se diera con 
anticipación, en lugar de lidiar con él cuando así lo exige el trabajo?

Al menos en México el uso de Adobe supuestamente se da durante el periodo de
educación universitaria o, en todo caso, que no es responsabilidad de la empresa
o institución que da trabajo al profesionista. La competencia pedagógica
recae en las instituciones educativas o en el interesado, lo cual en muchos
casos no es económicamente accesible para cualquier estudiante.

Los costos requeridos para ello, desde la adquisición de equipo de cómputo
adecuado, pasado por el pago de una matrícula o de un taller, hasta *tal vez*
el pago de una licencia, no son absorbidos ni promocionados por Adobe. Es en 
este sentido que otras empresas tecnológicas aspiran a tener el grado de 
penetración que ha tenido Adobe: la necesidad de publicidad, de talleres
gratuitos o de demostraciones es prácticamente nula, incluso al punto que su 
*software* se vuelve sinónimo de producción cultural. 

Ya no se habla de aprender *desktop publishing*, sino de aprender InDesign; no 
se habla de mapa de bits, sino de Photoshop; no son vectores, sino Illustrator, 
etcétera. La extrapolación entre *software*, formatos y métodos es tal que uno 
de los retos cuando se demuestra que Adobe no lo es todo para la publicación, es 
distinguir entre una y otra cosa, algo tan básico, pero al mismo tiempo tan 
ignorado por gran parte del sector editorial.

El beneficio que acarrea la disminución de la membresía para alumnos o 
profesores es claro: **el modo Adobe no solo se sostiene por un entorno 
suplantado como método de trabajo, sino también por la exigencia curricular
por parte de empleadores y educadores**.

## ¿pos-Adobe?

¿Y qué de malo tiene? Nada. Entonces, ¿cuál es el problema? La dificultad surge
cuando desde Adobe se pretende obtener una calidad de trabajo para lo que de
principio no forma parte de su ecosistema. Las publicaciones digitales han 
puesto está carencia en medio del debate; sin embargo, es una cuestión que ha
existido desde el mismo surgimiento de Adobe.

Adobe hace uso del enfoque WYSIWYG para todo lo relativo a la producción 
cultural; es decir, **el modo Adobe tiene su principal enfoque en la dimensión
visual** de un proyecto. Y si bien para lo que atañe a la edición existe el 
empleo de estilos de párrafo y de carácter, así como la posibilidad de importar 
XML, la realidad en el día a día de la edición en México es que los estilos son 
más una excepción que una regla y la importación es prácticamente desde archivos 
DOCX.

**El enfoque visual no es problemático hasta que existe la necesidad de una
estructura consistente**. Esta dificultad ha sido percibida desde sus inicios
por dos nichos: la publicación en ciencias exactas y la redacción de
documentación de *software*. Así que no importa qué tanto cuidado se tenga
con una publicación digital a través de Adobe, su relación 
costo-tiempo-resultado es menor a otros medios para el desarrollo de esta índole
de publicaciones.

Si una publicación digital a partir de InDesign tiene sus carencias o falta
de control (donde  «control» implica el aprendizaje de tecnologías *web* y la
estructura de un EPUB, intención contraria a lo que se pretende con este 
programa) no es porque el usuario no sea capaz de desarrollar una publicación
digital con calidad técnica y control editorial. Se debe a que InDesign no fue
pensado para la publicación digital, sino para la maquetación de impresos.

En una «era» donde es necesario la publicación multiformato, esto implica que:

* o se acepta la baja calidad de una publicación digital bajo el argumento de
  la limitación tecnológica o la poca exigencia del lector promedio,
* o se implementa un método cíclico de edición que implica un aumento 
  proporcional de costos y tiempos,
* o se busca un nuevo método que venga a encarar este reto (véanse los artículos
  sobre edición cíclica y ramificada, [empezando por aquí](http://marianaeguaras.com/edicion-ciclica-y-edicion-ramificada-de-la-antesala-a-la-revolucion-digital/)).

Desde esta perspectiva tenemos las consideraciones metodológicas, técnicas y de
cuidado editorial. Pero otro punto es la conservación de los documentos.

Todo proyecto editorial «exitoso» requerirá por lo menos una reedición de la 
obra. Partiendo del supuesto que la mayoría de las editorial o editores solo 
apuestan por proyectos que serán «exitosos», esto implica que en un par de años 
se  necesitarán de nuevo los últimos archivos editados de una obra.

Debido a las correcciones al vuelo durante la maquetación en InDesign, es casi 
seguro que ese archivo «final» ya solo esté disponible en INDD. ¿Qué pasa cuando 
la versión de InDesign ya no es compatible con el archivo? ¿Qué pasaría si Adobe 
descontinua InDesign, como lo hizo con PageMaker? ¿Qué sucedería si otra empresa 
entra a la escena del DTP y el usuario empieza a migrar a su ecosistema, como le 
sucedió a QuarkXPress cuando llegó InDesign?

**Los proyectos editoriales con miras a conservarse a un largo plazo no pueden
estar a merced de lo que le sucede a las empresas de *software***. Por estos
motivos, principalmente en las esferas de influencia anglosajona o germánica
la migración se ha dado hacia archivos XML. El XML también ha sido el formato
por defecto para repositorios académicos, debido principalmente a su apertura y 
su capacidad para el intercambio de información o de publicación automatizada.

Aunque InDesign permite la importación de archivos XML, la imposibilidad de
automatización por el mismo carácter privativo del programa ha inclinado a
varios editores a optar por otras vías, principalmente de la familia TeX,
como [ConTeXt](https://es.wikipedia.org/wiki/ConTeXt) o [TeXML](https://en.wikipedia.org/wiki/TeXML).

¿Que en la automatización se pierde mucho de la rigurosidad ortotipográfica?
Cierto, pero a muchas editoriales anglófonas o germáfonas parece no importarles.
¿Debería ser relevante para hablahispana? Quizá lo discutamos en algún otro
momento.

Mientras tanto, no hay necesidad de buscar un puerto seguro si algún día Adobe
deja de ser sinónimo de una profesión, porque las alternativas han estado ahí
desde hace mucho tiempo. Únicamente es cuestión que el editor promedio se dé
cuenta que el control estructural de una obra es primordial y que **Adobe, el
omnipresente, no es omnipotente**…
