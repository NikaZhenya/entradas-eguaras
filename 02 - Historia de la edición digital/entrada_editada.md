# 2. Historia de la edición digital

**«Historia de la edición digital»** es la segunda entrega sobre edición
digital, de **Ramiro Santa Ana Anguiano**, de **Nieve de Chamoy**. Debido 
a la extensión, más larga que otras entradas, hemos preferido publicar 
este artículo en archivos descargables.

Los formatos disponibles en los que se encuentra el
artículo son +++EPUB+++, +++MOBI+++, GitBook y +++PDF+++. Gracias a esta variedad, puedes
elegir el formato que te resulte más cómodo para leer.

También se pueden **descargar los archivos editables** para apreciar los
métodos descritos por Ramiro en este artículo.

Los cuatros archivos fueron creados por Ramiro desde la metodología
propuesta en el artículo; a saber, _single source publishing_ junto con
_online publishing_.

**[Todos los formatos se encuentran alojados en
GitHub](https://github.com/NikaZhenya/historia-de-la-edicion-digital)**,
también el registro del proceso de edición.

A partir de un
[Markdown](http://www.genbeta.com/guia-de-inicio/que-es-markdown-para-que-sirve-y-como-usarlo),
como archivo «madre», se crearon los siguientes formatos:

* **+++EPUB+++** desde cero y mediante los _scripts_ de Perro
  Triste. **[Clic para
  descargar](https://github.com/NikaZhenya/historia-de-la-edicion-digital/blob/master/ebooks/produccion/historia-de-la-edicion-digital.epub?raw=true)**.
* **+++MOBI+++**, desde +++EPUB+++, con la herramienta de conversión de Amazon:
  KindleGen.  **[Clic para
  descargar](https://github.com/NikaZhenya/historia-de-la-edicion-digital/blob/master/ebooks/produccion/historia-de-la-edicion-digital.mobi?raw=true)**.
* **+++PDF+++**, mediante Pandoc y LaTeX. **[Clic para
  descargar](https://github.com/NikaZhenya/historia-de-la-edicion-digital/raw/master/ebooks/produccion/historia-de-la-edicion-digital.pdf).**
* **GitBook** desde cero (donde solo se divide el documento de
  Markdown en las secciones correspondientes ya que los archivos base
  de GitBook ya son Markdown). **[Clic para
  visualizar](https://nikazhenya.gitbooks.io/historia-de-la-edicion-digital/content/)**.

## Temas que aborda «Historia de la edición digital»

A modo de resumen, a continuación incluimos un punteo de los temas que
Ramiro desarrolla en este artículo:

* Cómo se introdujeron las computadoras u ordenadores a la
  cotidianidad del público general.
* Qué son los lenguajes de marcado y el lenguaje de marcas ligero.
* Por qué la industria editorial no adoptó TeX, un programa pensado
  para el cuidado tipográfico y composición de textos.
* Qué son los enfoques relacionados al tratamiento del texto digital
  +++WYSIWYG+++ y +++WYSIWYM+++, y cómo afectan a la edición digital.
* Cómo y por qué surgió la _desktop publishing_ (+++DTP+++).
* La ideas entorno a la «libertad» en la edición que plantean el
  _software_ privativo y el _software_ libre.
* El surgimiento de la _web_ y la consiguiente aparición del +++HTML+++, el
  +++XML+++ y el +++XHTML+++.
* La aparición del +++EPUB+++ y los estándares W3C.
* Diferencia entre *edición digital* y *publicación digital*.
* Propuesta para la automatización y sistematización de la edición
  digital
* Qué son _online publishing_ y _simple source publishing_ y qué
  relación poseen con el control sobre la edición y las versiones de
  archivos.

**Palabras clave:  **Adobe Creative Suite, Amazon, Apple, código fuente,
+++CSS+++, cuidado editorial, _desktop publishing_ (+++DTP+++), _ebook_, +++EPUB+++,
formateo, formato, Gimp, +++GNU+++, _hardware_, +++HTML+++, +++IBM+++, Illustrator,
InDesign, Inkscape, LaTeX, lenguaje de marcado, LibreOffice, libro
impreso, Markdown, +++MOBI+++, OpenOffice, +++PDF+++, Photoshop, PostScript,
proyecto Gutenberg, publicación digital, Quark+++XP+++ress, repositorios,
Scribus, _simple source publishing_, _software_, _software_ libre, TeX,
W3C, Word, WordPerfect, +++WYSIWYG+++, +++WYSIWYM+++, +++XHTML+++, +++XML+++.

A diferencia del resto de los contenidos de este blog, **«Historia de la
edición digital» posee [Licencia Editorial Abierta y
Libre](https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre)
(+++LEAL+++)**.

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «Historia de la edición digital».
* Título en el _blog_ de Mariana: «Historia de la edición digital».
* Fecha de publicación: 10 de octubre del 2016.
* [Repositorio](https://github.com/NikaZhenya/historia-de-la-edicion-digital).
* [Revisión de Mariana](https://marianaeguaras.com/historia-de-la-edicion-digital/).

</aside>
