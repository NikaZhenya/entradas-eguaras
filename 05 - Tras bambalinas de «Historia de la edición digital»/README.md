# Tras bambalinas de «Historia de la edición digital»

Enlace al *blog* de Mariana Eguaras: [https://marianaeguaras.com/como-generar-4-formatos-de-un-contenido-en-un-dia/](https://marianaeguaras.com/como-generar-4-formatos-de-un-contenido-en-un-dia/).

Ojo: en el *blog* se publicó con el título «Cómo generar 4 formatos de un contenido en un día».
