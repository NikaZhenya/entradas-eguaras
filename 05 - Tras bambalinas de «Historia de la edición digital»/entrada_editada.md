# 5. Tras bambalinas de «Historia de la edición digital»

Hace unos meses atrás publicamos el artículo **«[Historia de la edición
digital](@index[2])»**
en **cuatro formatos digitales diferentes: +++EPUB+++, +++MOBI+++, +++PDF+++ y GitBook**.
Estos formatos se crearon según la metodología del _single source_ y
_online publishing_, abordada en [esta
entrada](@index[4]).

Aunque te cueste creerlo, los cuatros formatos fueron procesados y
creados en un día, en el tiempo de una jornada de trabajo.

Como anticipamos en el
[repositorio](https://github.com/NikaZhenya/historia-de-la-edicion-digital)
del artículo, en esta entrada se explican los pasos llevados a cabo para
su edición y publicación.

## Proceso de desarrollo

A continuación se comentarán unas impresiones de pantalla omitiendo la
parte técnica, ya que los [_scripts_
utilizados](https://github.com/NikaZhenya/Pecas) están
pasando por grandes cambios en aras de la posibilidad de una interfaz
gráfica. [Estas son las
herramientas](https://github.com/NikaZhenya/Pecas), por si quieres
echarle un ojo.

### 1. Clonación del repositorio, 14:22 h

![Clonación del repositorio, 14:22 h.](../img/img_05-01.jpg)

El proyecto es un repositorio para:

1. tener un gestor de versiones que permita puntos de guardado,
   evitando el respaldo de archivos cada vez que se termine o se empiece
   una tarea importante, y,
2. habilitar la conexión a un servidor para salvaguardar el proyecto
   si la computadora deja de funcionar.

Para este caso utilizamos [GitHub](http://github.com/) como servidor de
nuestro repositorio, ya que es una plataforma estable y pública. De esta
manera, no tenemos que preocuparnos por el mantenimiento y, en este
caso, para que además cualquiera pueda descargarlo.

Por ello, se genera el repositorio en GitHub y a continuación se clona
(léase «descarga») a la computadora.

Por supuesto, es posible crear repositorios privados para que solo
nosotros o un equipo de trabajo tenga acceso, pero no es lo que se
pretende en esta publicación.

### 2. Adición de recursos, 14:24 h

![Adición de recursos, 14:24 h.](../img/img_05-02.jpg)

A continuación se añaden al proyecto las imágenes y el texto editado por
Mariana. Y, antes de seguir con el siguiente paso, preferí ir a comer
porque lo había olvidado, je.

### 3. Revisión del archivo original, 15:05 h

![Revisión del archivo original, 15:05 h.](../img/img_05-03.jpg)

Con Mariana estuvimos editando el texto entre el formato +++RTF+++ y +++ODT+++, ya
que no cuento con Microsoft Office ni tenía el deseo de trabajar con los
formatos +++DOC+++ o +++DOCX+++.

Entre el +++RTF+++ y +++ODT+++ luego suceden cosas «extrañas» en el formato, por lo
que se ha de examinar el archivo para constatar que todo está en orden.

### 4. Conversión del archivo original, 15:08 h

![Conversión del archivo original, 15:08 h.](../img/img_05-04.jpg)

Mediante [Pandoc](http://pandoc.org/) se convierte el texto original a
Markdown. Este proceso es uno de los más breves e importantes ya que así
obtenemos el formato más apto para nuestro archivo madre.

### 5. Revisión del archivo madre, 15:12 h

![Revisión del archivo madre, 15:12 h.](../img/img_05-05.jpg)

Markdown es un lenguaje de marcado ligero, que permite dar estructura al
texto (encabezados, bloques de cita o itálicas, por ejemplo) con una
sintaxis fácil de aprender y de leer.

La consecuencia de esta versatilidad es, sin duda, una simplificación en
la estructura. Markdown es un lenguaje con el que podemos determinar una
[gran variedad de
elementos](https://daringfireball.net/projects/markdown/syntax.php),
pero sus posibilidades se quedan cortas si lo comparamos con +++HTML+++ o TeX.

Sin embargo, **es en su simplicidad por lo que Markdown es el lenguaje
más óptimo para el archivo «madre»** de cada uno de los formatos de
nuestra publicación.

La idea detrás de esto es ir de lo simple a lo complejo, del Markdown a
otros formatos que, por su naturaleza, requieren ajustes particulares.

En este paso solo se da un vistazo al archivo madre y es más adelante
donde agregaremos otros elementos.

### 6. Creación del proyecto para +++EPUB+++, 15:12 h

![Creación del proyecto para +++EPUB+++, 15:12 h.](../img/img_05-06.jpg)

Una de las primeras herramientas de Perro Triste es `pt-creator`, la
cual nos crea un proyecto base para un +++EPUB+++. Con esto evitamos empezar
el proyecto desde cero o a partir de la copia de otro anterior, ya que
nos implementa una estructura de archivos y carpetas convencional a
cualquier +++EPUB+++.

### 7. Conversión del archivo madre a +++HTML+++, 15:14 h

![Conversión del archivo madre a +++HTML+++, 15:14 h.](../img/img_05-07.jpg)

Para el +++EPUB+++ requerimos al menos un archivo +++HTML+++, el cual tiene su
origen en el archivo madre que convertimos en +++HTML+++ a través de Pandoc.

En este punto es cuando también se añaden elementos adicionales al +++HTML+++,
como son las clases de párrafos, los identificadores o las imágenes.

### 8. División del archivo +++HTML+++, 15:32 h

![División del archivo +++HTML+++, 15:32 h.](../img/img_05-08.jpg)

Por lo general, un +++EPUB+++ tiene un documento +++XHTML+++ por cada sección de la
obra, así que ahora dividimos el archivo +++HTML+++ por medio de p`t-divider`.

Este _script_ divide el documento de entrada cada vez que encuentra un
encabezado de primera jerarquía (`h1`), permitiéndonos especificar el tipo
de sección que se trata y generando automáticamente un +++XHTML+++.

Ups, alguien llama a la puerta. Hora de suspender un poco…

### 9. Creación del +++EPUB+++, 16:08 h

![Creación del +++EPUB+++, 16:08 h.](../img/img_05-09.jpg)

Ahora ya contamos con todo lo necesario para nuestro +++EPUB+++, por lo que
procedemos a crearlo con `pt-recreator`.

Este es uno de los _scripts_ más importantes ya que **evita que nos
preocupemos de la correcta introducción de metadatos o de la creación de
todos aquellos archivos propios del +++EPUB+++**, como las tablas de
contenidos.

Así pues, poco a poco nos acercamos al ideal donde quien edita o quien
diseña puede centrarse exclusivamente en su trabajo.

### 10. Validación del +++EPUB+++, 16:12 h

![Validación del +++EPUB+++, 16:12 h.](../img/img_05-10.jpg)

Crear el +++EPUB+++ es una cuestión, pero otra es desarrollar correctamente un
+++EPUB+++. Para saber si nuestro _ebook_ tiene coherencia interna, utilizamos
[Epubcheck](https://github.com/IDPF/epubcheck/releases), el validador
oficial.

Como `pt-recreator` automatiza la creación de la «médula» del +++EPUB+++, los
posibles errores o advertencias que nos indique Epubcheck estarán
relacionados con nuestra labor de marcado o de inclusión de recursos.
(En ocasiones bien específicas algún error es ocasionado por una
herramienta de Perro Triste que se van depurando conforme se van
identificando).

Si el +++EPUB+++ tiene errores, solo basta con corregirlos y regenerar de
nuevo el +++EPUB+++ con `pt-recreator`, un proceso tan sencillo que incluso
este _script_ guarda los metadatos ya introducidos.

Para evitar la gran mayoría de los errores, recomiendo dos cosas:

1. **usar formatos +++XHTML+++ en lugar de +++HTML+++**, ya que así no se permiten
   ambigüedades en las etiquetas, y
2. **ver siempre los archivos en Firefox antes de crear el +++EPUB+++**,
   debido a que este explorador libre no te muestra el archivo si tiene
   errores, caso contrario a Safari o Chrome.

### 11. ¡+++EPUB+++ listo! 16:14 h

![¡+++EPUB+++ listo! 16:14 h.](../img/img_05-11.jpg)

**En menos de dos horas terminamos el +++EPUB+++, sin errores y con un gran
control sobre la estructura y el diseño** y eso que una hora se nos fue
entre la comida y la llamada a la puerta.

¿Acaso el proceso podría simplificarse y estar más enfocado a las
necesidades editoriales? ¡Sí, es en lo que estamos trabajando!

### 12. ¡Conversión del +++EPUB+++ a +++MOBI+++! 16:14 h

![¡Conversión del +++EPUB+++ a +++MOBI+++! 16:14 h.](../img/img_05-12.jpg)

Para el archivo legible para Kindle no tenemos que hacer gran cosa.
Amazon nos pone
[KindleGen](https://www.amazon.com/gp/feature.html?ie=UTF8&docId=1000765211)
a nuestra disposición, una herramienta que nos permite convertir el +++EPUB+++
a su formato privativo.

**¡En menos de un minuto ya tenemos listo el _ebook_ para Kindle!**

### 13. Creación del proyecto para LaTeX, 16:16 h

![Creación del proyecto para LaTeX, 16:16 h.](../img/img_05-13.jpg)

Ahora es momento de concentrarnos en la **creación del +++PDF+++**. Para esto
no usaremos el _software de facto_ en el mundo editorial
([InDesign](https://marianaeguaras.com/usas-indesign-sacale-mas-partido-con-estas-entradas/)),
sino una herramienta más antigua y potente:
[TeX](https://es.wikipedia.org/wiki/TeX).

Para facilitarnos la creación del +++EPUB+++, emplearemos un conjunto de
[macros](https://es.wikipedia.org/wiki/Macro) conocido como
[LaTeX](https://es.wikipedia.org/wiki/LaTeX). Por el momento, no existe
un _script_ de Perro Triste para crear un proyecto base de LaTeX. Por
tanto, se copia y pega manualmente una plantilla que se encuentra en el
repositorio de estas herramientas.

### 14. Conversión del archivo madre a TeX, 16:18 h

![Conversión del archivo madre a TeX, 16:18 h.](../img/img_05-14.jpg)

De nueva cuenta, convertimos el archivo madre a través de Pandoc, pero
ahora para tener un formato TeX. A partir de aquí se empieza la labor de
cuidado editorial característico de un impreso, por lo que nos tomamos
un tiempo para corregir cajas, viudas, huérfanas, etcétera.

### 15. ¡+++PDF+++ listo! 17:57 h

![¡+++PDF+++ listo! 17:57 h.](../img/img_05-15.jpg)

Después del meticuloso cuidado en LaTeX (ni tanto, porque Mariana
encontró varios detalles al +++PDF+++), **¡obtuvimos un +++PDF+++ en poco más de
hora y media!**

El +++PDF+++ puede modificarse para que sea una salida apta para impresión,
pero en este caso solo optamos por la posibilidad de una lectura
digital.

### 16. Creación del proyecto para GitBook, 18:09 h

![Creación del proyecto para GitBook, 18:09 h.](../img/img_05-16.jpg)

Por último, también quisimos tener una opción completamente _online_.
Para esto optamos por [GitBook](https://www.gitbook.com/), un proyecto
creado por GitHub, inicialmente para la creación de documentación de
_software_.

### 17. Clonación del repositorio de GitBook, 18:18 h

![Clonación del repositorio de GitBook, 18:18 h.](../img/img_05-17.jpg)

Una vez que configuramos el libro en GitBook, procedemos a descargar el
repositorio. En este punto es donde la publicación comprende dos
repositorios, [uno para el formato de
GitBook](https://github.com/NikaZhenya/historia-de-la-edicion-digital-gitbook)
y otro [para el resto de los
formatos](https://github.com/NikaZhenya/historia-de-la-edicion-digital).
Esto se debe a que GitBook actualiza automáticamente la publicación
cuando se mandan cambios al servidor.

### 18. División del archivo madre, 18:32 h

![División del archivo madre, 18:32 h.](../img/img_05-18.jpg)

El formato que utiliza GitBook es Markdown, por lo que solo es necesario
crear una serie de archivos en este formato para copiar y pegar cada una
de las secciones. Posteriormente, en otro Markdown se crea la tabla de
contenidos y ya tenemos todo para publicar este formato.

### 19. ¡GitBook listo! 19:26 h

![¡GitBook listo! 19:26 h.](../img/img_05-19.jpg)

**Después de poco más de una hora, el artículo ya está disponible en
línea** cuando se mandan los cambios al servidor.

**En un lapso de cinco horas desarrollamos cuatro formatos del
_ebook_**, que cualquier persona puede descargar o leer en +++EPUB+++, +++PDF+++,
+++MOBI+++ o en línea. Mientras tanto, ¿ya hay hambre de nuevo, no?

## _Single source_ y _online publishing_

A partir de un archivo madre se obtuvieron tres formatos de una misma
publicación:
[+++EPUB+++](https://github.com/NikaZhenya/historia-de-la-edicion-digital/blob/master/ebooks/produccion/historia-de-la-edicion-digital.epub?raw=true),
[+++PDF+++](https://github.com/NikaZhenya/historia-de-la-edicion-digital/raw/master/ebooks/produccion/historia-de-la-edicion-digital.pdf)
y
[GitBook](https://nikazhenya.gitbooks.io/historia-de-la-edicion-digital/content/).
Y a partir del +++EPUB+++ se creó el formato
[+++MOBI+++](https://github.com/NikaZhenya/historia-de-la-edicion-digital/blob/master/ebooks/produccion/historia-de-la-edicion-digital.mobi?raw=true).
Esto es un ejemplo del _single source publishing_, donde a partir de un
tronco en común (en este caso el Markdown) se crearon distintas «ramas»
para cada uno de los formatos.

De esta manera, todos tienen un elemento en común al mismo tiempo
contemplan sus particularidades. Y además hacen que estos cambios
específicos no afecten al resto de los formatos.

Una vez terminado el desarrollo de los diversos formatos, Mariana cotejó
cada uno de ellos para depurar errores. Este proceso de repechaje
también exige una atención especial (está en la lista de deseos de Perro
Triste). Sin embargo, por el momento nos acotamos a modificar y recrear
cada uno de los archivos. Proceso que no consume más tiempo que el
primer desarrollo.

En el caso de existir algún problema, el _online publishing_ permite
recuperar información a partir de puntos de control e incluso tener el
texto disponible en línea con una simple actualización.

Por ejemplo, a partir de ahora desde los distintos enlaces pueden
descargarse o leerse el artículo con la seguridad de que siempre se
accederá a la versión más actualizada, ¡porque no tenemos que copiar y
pegar archivos en otro lugar!

Por las imágenes mostradas, más de uno pensará que se trata de procesos
complejos para la mayoría de las personas que publican un libro…

Lo único que puedo mencionar es que ese es el motivo por el que **en
Perro Triste estamos desarrollando estas herramientas**, así como en
Nieve de Chamoy es donde se prueban y afinan en el día a día del
quehacer editorial.

Al final, la gran pregunta es: **¿qué otras posibilidades existen para
facilitar y automatizar la creación de diversos formatos con tiempos
semejantes a un día de jornada laboral?**

Existe la convicción de que quien edita o quien diseña no debe dejar de
lado el cuidado que exigen sus profesiones. Pero, para que esto suceda,
debe haber un empoderamiento tecnológico que convierta las actuales
exigencias del mundo editorial en posibilidades de mejora en lugar de
dificultades.

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «Tras bambalinas de “Historia de la edición digital”».
* Título en el _blog_ de Mariana: «Cómo generar 4 formatos de un contenido en un día».
* Fecha de publicación: 8 de febrero del 2017.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/05%20-%20Tras%20bambalinas%20de%20%C2%ABHistoria%20de%20la%20edici%C3%B3n%20digital%C2%BB/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/como-generar-4-formatos-de-un-contenido-en-un-dia/).

</aside>
