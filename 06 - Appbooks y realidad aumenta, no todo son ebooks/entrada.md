# *Appbooks* y realidad aumenta, ¡no todo son *ebooks*!

Las posibilidades tecnológicas actuales posibilitan una clase de publicaciones 
cuya metodología poco tiene que ver con la tradición editorial. Esta esencial
diferencia se debe a que se trata de libros electrónicos que sobresalen por
su alta interactividad o propuestas que no se constriñen al paradigma de
la página, la lectura lineal de una obra y las narrativas que permite este
soporte. Este tipo de productos editoriales es lo que últimamente se conoce 
como *appbooks*.

Este término proviene principalmente por la popularización de las tiendas de
aplicaciones como Google Play o App Store. Sin embargo, 
**los *appbooks* anteceden a los teléfonos inteligentes y las tabletas**. 
De hecho, tienen su antecedente cuando se empezó a experimentar con la escritura 
a través de la computadora y cuyos resultados no se pensaron para la impresión.

Por este motivo los *appbooks* han existido no solo como aplicaciones para 
iOS o Android, sino también en forma de CD interactivos (¿quién no recuerda la
enciclopedia interactiva de Salvat?), plataformas *web*, videojuegos o aplicaciones 
con realidad aumentada.

## Metodologías de desarrollo variable según el tipo de narrativa buscada

**Para los *appbooks* no existe un programa especialmente diseñado para su creación**, 
en su lugar, se utilizan una serie de diferentes tecnologías, por ejemplo:

* Páginas HTML estáticas en los que únicamente se usa HTML, CSS y JavaScript. Estas
son una de las propuestas más tempranas que se basaron principalmente en la narrativa
hipertextual, aunque no se reduce únicamente a esto, sino también al empleo de
programación para cambiar dinámicamente el texto o desatar otros eventos, como en 
[CLIteratura](http://cliteratu.re/).

* Aplicaciones *web* que interactúan con un servidor para obtener información. La
originalidad narrativa de estas aplicaciones tiende a ser en su manera de organizar
una gran cantidad de información textual, por lo que tienden a ser diccionarios, como
[*Stanford Encyclopedia of Philosophy*](https://plato.stanford.edu/), enclicopedias,
como Wikipedia, o catálogos, como el que en Nieve de Chamoy estamos creando para
las obras del [Museo Soumaya](http://www.soumaya.com.mx/index.php/esp).

* Aplicaciones híbridas en las que se mezclan elementos *web* con el uso del
*hardware* del dispositivo móvil. De esta manera se hace posible que el *appbook* no
tenga que abrirse desde un explorador, sino que esté como una aplicación independiente,
donde la narratividad aún tiene un foco principalmente textual al menos al momento de
organizar la información, como es la *app* de [Jazz at Lincoln Center](https://itunes.apple.com/mx/app/jalc/id1007748257?mt=8)
o la aplicación que en estos días colgaremos sobre los cien años del [Futbol Club Atlas](http://www.atlasfc.com.mx/).

* Aplicaciones nativas que utilizan directamente las posibilidades que ofrece el
dispostivo. Estas aplicaciones se desarrollan así para obtener una gran optimización 
para una narrativa interactiva, como 
[*Our Choice*](https://itunes.apple.com/us/app/al-gore-our-choice-plan-to/id432753658?mt=8)
o la [*Guía del Prado*](http://www.laguiadelprado.com/EN/index.html).

* *Software* creado con motores de videojuegos. Las posibilidades narrativas se
aproximan más a la jugabilidad y por lo general ya contemplan ciertas caracteres habituales
en los videojuegos, como un sistema de puntuación o la interacción con personajes virtuales,
como es perceptible en el trabajo que realiza [Concretoons](http://cartuchera.concretoons.com/index.html),
los videojuegos independientes como [*The Mammoth*](https://inbetweengames.itch.io/mammoth-download) o
la aplicación [*En busca de Kayla*](https://itunes.apple.com/mx/app/en-busca-de-kayla/id1167229912?mt=8)
que le desarrollamos a la editorial Sexto Piso.

* *Software* que involucra [realidad aumentada](https://es.wikipedia.org/wiki/Realidad_aumentada).
Esta clase de *appbook* buscan alimentar la narrativa textual de una publicación impresa al 
ofrecer elementos multimedia que se desatan al enfocar el dispositivo a la obra, como
[*La leyenda del ladrón*](http://www.pitboxmedia.com/la-leyenda-del-ladron-libro-con-realidad-aumentada/)
o la que desarrollamos para la [Universidad Nacional Autónoma de México](https://itunes.apple.com/mx/app/unam-futuro/id1042823048?mt=8).

## También existen limitaciones… principalmente económica

En cualquier ámbito profesional o institucional de trabajo siempre tenemos el poblema de 
que por lo general el tiempo destinado a un proyecto no es el deseado para el 
resultado más óptimo. Por lo general existe la fortuna de que esos detalles no sean
perceptibles para el público en general; por ejemplo, los callejones de un impreso
tienden a sobresalir entre otros editores. Es decir, muchas veces esos
errores no afectan el uso general de un producto.

Sin embargo, **en el desarrollo de *appbooks* los descuidos son lo
primero que advierte el usuario**. ¿Cuántas veces no ha sido molesto usar una aplicación cuya 
navegación no es «amistosa» o hace cosas «inesperadas»? Los detalles que pueden encontrarse 
en un *appbook* por lo general afectan la navegación general de la obra, lo que puede llevar 
a una evaluación negativa o devolución.

En la creación de *appbooks* ha de tenerse presente que estos detalles estarán presentes 
si el tiempo es escaso. Esto es relevante si la publicación es para un tercero, ya que
se ha de indicar para evitar futuros problemas. En este sentido, es ideal que los *appbooks* 
cuenten con tiempos de pruebas y de optimización según los dispositivos de salida. Estos 
tiempos pueden ser de algunas semanas a varios meses, según la complejidad de la publicación.

«Fase de pruebas» y «fase de optimización» no son procesos comunes dentro
de la producción del libro: **los *appbooks* tienen una metodología más afín al desarrollo
de *software* o de videojuegos**. Por lo general esto también implica limitaciones donde

* quienes editan carecen de conocimiento para el desarrollo de *software* por lo que el
*appbook* presenta problemas de desempeño, o
* quienes programan desconocen el control de la calidad editorial, ocasionando que el
*appbook* tenga una baja calidad en la edición.

La recomendación para los que desean crear este tipo de publicaciones es que en un primer
momento se decida por un tipo de *appbook* de los que se mencionaron en la sección anterior
y que paulatinamente se vaya aumentado el grado de complejidad. Pero principalmente, que se
tenga en cuenta que la inclusión de un artista digital y alguien que programe es indispensable, 
porque en la contratación externa existe la posibilidad de una falta de comunicación,
ya que la jerga editorial les es desconocida, así como los conceptos fundamentales para el
desarrollo de *software* o del arte digital no forman parte de la cultura general de quien 
edita, lo que puede ir en detrimento de la calidad.

Estas limitaciones de tiempo y de técnica acarrean que los costos de producción de un
*appbook* no sea el que se piensa en un principio, más si no existe una planificación
adecuada. ¡Se está desarrollando *software*!, **el costo de creación de *appbooks* es igual
o mayor al de un impreso**. Si se piensa que el desarrollo de un *appbook* implica tiempos
y esfuerzos similares al desarrollo de un libro electrónico estándar (EPUB, MOBI o IBOOKS),
es mejor que este entusiasmo se guarde para un proyecto muy importante…

## El lugar actual de los *appbooks* en el mundo editorial

Por la disparidad metodológica y las diversas limitantes que implica el desarrollo de aplicaciones,
**los *appbooks* no han llegado para reemplazar a los libros**, ni a los *ebooks*, al menos no
durante los siguientes cinco o diez años. El lugar de esta clase de publicaciones no es dentro
del grueso de la producción de libros, sino de excepciones dentro del mundo editorial.

Cuidado, que este carácter marginal no nos engañe. Esto no quiere decir que su potencial
comercial sea escaso. Al contrario, un *appbook* bien desarrollado y publicitado puede fácilmente
hacerse un contenido viral que genere millones de descargas.

Sin embargo, una alta calidad en el desarrollo y una buena estrategia de *marketing* digital
orientada a la venta de aplicaciones no son campos dominados por el sector editorial. El sector
general del desarrollo de *software* tampoco tiene un completo dominio, ya que en su mayoría
se orientan a creación de sistemas y soluciones empresariales, no a la creación de aplicaciones
interativas para el público general. **Los estudios de videojuegos son los que tienen una
mayor ventaja para el desarrollo de *appbooks***, debido a que son los que cuentan con un mayor
dominio y experiencia de las características técnicas y de las narrativas interactivas necesarias
para interesar al usuario. De hecho esto queda fácilmente comprobable al ver quién está detrás
de nuestros *appbooks* favoritos.

¿Qué nos queda en el mundo editorial? ¡Experimentación! **Los *appbooks* pueden ser una muestra
de cómo será posible «deconstruir» la lectura** y, por ende, de los profundos cambios que
representan las publicaciones digitales para los procesos editoriales. Esto se debe a que, por 
un lado, recuperar la inversión necesaria para el desarrollo de un *appbook* es aún más 
difícil que agotar una edición de un impreso, por lo que
o se parte del supuesto que la inversión no se recuperará; o se crean si la entidad editorial
cuenta con esa posibilidad de producción de manera orgánica, sin personal contrado de manera
externa, con una clara idea técnica de lo que se hará y con un calendario realista, o se 
ofrece como un servicio editorial a otras entidades públicas o privadas cuyo objetivo es ampliar
su oferta de productos o servicios, en varias ocasiones no relacionadas al contexto editorial.

Por otro lado, en el mundo editorial todavía es muy difícil separar la idea de «obra» a la de
«libro», en donde muchos casos este último término quiere decir «libro impreso». La asociación
había sido tan normal que el simple hecho de hablar de diseño fluido, como es posible en libros
estándar, ya es una noción difícil de entender porque cambia la idea de lo que se entiende por
«página». A esta complicación sumemos que en muchos *appbooks* la idea de «página» es tan difusa,
sino que inexistente…

Ejemplos rápidos de esta dificultad los tenemos en Nieve de Chamoy todos los días. Cada vez que
hacemos una aplicación a un cliente, en muchos casos profesionales de la edición, lo primero a
esclarecer es que **en el desarrollo de los *appbooks* es indiferente la cantidad de páginas**.
Para su creación lo que es relevante es la cantidad de dinámicas que se podrán hacer,
los recursos que se tienen y la cantidad de imágenes, videos o audios a incluir (e incluso también
el formato de origen de los contenidos). La complejidad no es proporcional a la cantidad de texto, 
como es en la corrección o traducción. La «página», la «cuartilla», o las cantidades de palabras
o de caracteres son tan fundamentales para calcular los costos en el mundo editorial que por lo 
general la creación de un presupuesto para *appbooks* ya es una causa de conflicto.

Además, si entre quienes editamos la publicación de libros electrónicos estándar es un reto, mayores
dificultades enfrentamos en el desarrollo de *appbooks*. Si entre los profesionales de la edición
aún es muy difícil desembarazar la idea de «obra» de la de «impreso» o que **la página ya no es
el único paradigma para la edición**. Ya podemos imaginarnos lo complicado que esto puede ser para
el lector en general, que sin dudas estimará o rechazará la propuesta narrativa de un *appbook* 
dentro de un universo digital donde se convive con las películas y los videojuegos…

Para finalizar, les comparto una pequeña lista de reproducción de *appbooks* de diversa índole, la
cual empleamos en talleres, seminarios o pláticas. Si conocen un video que sea relevante para
esta lista, ¡no olviden comentarlo!

[Lista a insertar](https://www.youtube.com/playlist?list=PLqfNe0w1_scIpNUjgmbRjPfDOUugSGz-a)

