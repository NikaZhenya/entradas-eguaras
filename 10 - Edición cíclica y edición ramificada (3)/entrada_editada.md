# 10. Edición cíclica y edición ramificada: un vuelo seguro y constante

## Un vuelo seguro y constante

Hasta aquí se ha descrito cómo es posible la producción de diversos
formatos y posteriores modificaciones sin dependencia a un soporte
final. Pero hay ocasiones en que el proyecto completo se arruina:
archivos corruptos, pérdida de documentos, malfuncionamiento de la
computadora, etcétera.

La generación de respaldos, entendidos como copias adicionales
almacenadas físicamente o en la «nube», se vuelve un requisito
indispensable para los proyectos de cualquier índole. De esta manera,
cuando aparece un fallo se retoma alguna de las copias.

Sin embargo, esto acarrea los siguientes inconvenientes:

* Mayor necesidad de espacio cada vez que se crea una copia.
* Posibilidad de confusión entre los respaldos y el proyecto actual.
* Falta de control en los cambios, principalmente en grandes equipos.
* Dificultades al trabajar de manera separada o remota.
* Desconocimiento de casi todas las modificaciones que se han llevado
  a cabo.

**El descuido en la información afecta la calidad de la edición**. Por
lo que el mantenimiento de la información no solo es una tarea
secundaria o «técnica», sino una responsabilidad editorial.

El **octavo elemento** metodológico es el control de versiones. En un
videojuego existe la posibilidad de guardar la partida a cada paso o en
algunas situaciones críticas. A partir de estos cambios guardados es
posible regresar o probar nuevos caminos. Si el desarrollo es
satisfactorio estos otros senderos se convierten en el juego principal.

Un control de versiones cuenta con esos elementos. A partir de un
programa es posible monitorear las carpetas y archivos de un proyecto.
Se hace posible tener conocimiento de todo lo hecho, sin posibilidades
de confusión ni necesidad de mayor espacio, porque se emplean puntos
guardado.

En un videojuego no se respalda el juego una y otra vez para guardar una
partida, solo se almacenan los datos necesarios para regresar a ese
punto de guardado. **En un controlador de versiones no hay respaldos,
sino información para volver a cada punto de control**.

Esto implica que el espacio necesario para guardar un proyecto cae
drásticamente. También resuelve la dificultad de trabajar de manera
independiente, ya en el tronco como en las ramas e incluso en sus partes
pueden colocarse puntos de control en cualquier momento. Así, se puede
saber si se está trabajando con versiones anteriores o si hay
sobrescritura, porque cada punto de control tiene descripción, fecha,
autor, identificador e indicador de qué se creó, modificó o eliminó.

En un videojuego el uso de partidas guardadas permiten explorar más a
fondo la trama del juego debido a que en cualquier momento es posible
regresar. En un controlador de versiones hay tal flexibilidad que es
posible experimentar ciertas implementaciones (como
[_regex_](https://es.wikipedia.org/wiki/Expresi%C3%B3n_regular), p. ej.)
sin el peligro de corromper un proyecto. Esto se traduce a que yace la
posibilidad de adquirir mayores conocimientos de edición digital sin el
temor de arruinarlo todo.

Pero no solo eso. Un proyecto con un control de versiones es lo que se
conoce como [repositorio](https://es.wikipedia.org/wiki/Repositorio) y
estos tanto pueden estar en una computadora como en la «nube». El
**noveno elemento** es alojar los proyectos en un servidor para que
cualquier colaborador, desde cualquier lado y en cualquier momento,
pueda «subir» o «bajar» información para realizar su trabajo.

El +++SSP+++ pasa a ser
[+++SSOP+++](@index[4])
(_single source and online publishing_). El +++SSOP+++ se utiliza de
manera muy extensiva para el desarrollo de _software_ o investigaciones
científicas, pero también cabe emplearse en el quehacer editorial.

**La edición ramificada es una expresión particular del +++SSOP+++**.
El nexo es importante ya que el +++SSOP+++ se enfoca a un control en los
procesos, principalmente técnicos. Mientras tanto, los elementos metodológicos
de la edición ramificada hacen que este control se traduzca en un mayor
cuidado editorial, donde las tecnologías digitales y la automatización
de procesos sirven de apoyo a quien edita.

La «revolución» digital no va en contra de los conocimientos adquiridos
durante siglos, sino que revisita, depura o elimina técnicas y
metodologías que no son las más pertinentes para llevar a cabo una
tarea. En la edición, la «revolución» digital es la «apertura» del
«gremio» al uso de las tecnologías de la información en pos de técnicas
más aptas pero sin olvidar lo que hace de una obra un «buen»
libro.@note

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «Edición cíclica y edición ramificada (3)».
* Título en el _blog_ de Mariana: «Edición cíclica y edición ramificada: un vuelo seguro y constante (3/3)».
* Fecha de publicación: 18 de julio del 2017.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/10%20-%20Edici%C3%B3n%20c%C3%ADclica%20y%20edici%C3%B3n%20ramificada%20(3)/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/edicion-ciclica-edicion-ramificada-vuelo-seguro-constante/).

</aside>
