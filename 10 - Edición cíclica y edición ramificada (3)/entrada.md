# Edición cíclica y edición ramificada

## Edición cíclica

### Vuelo seguro y constante

Hasta aquí se ha descrito cómo es posible la producción de diversos soportes 
y posteriores modificaciones sin dependencia a un soporte final. Pero hay 
ocasiones en que el proyecto completo se arruina: archivos corruptos, pérdida 
de documentos, malfuncionamiento de la computadora, etcétera.

La generación de respaldos, entendidos como copias adicionales almacenadas
físicamente o en la «nube», se vuelve un requisito para los proyectos de
cualquier índole. De esta manera, cuando existe alguna falla se retoma 
alguna de las copias.

Sin embargo, se acarrean los siguientes inconvenientes:

* Mayores necesidades de espacio cada vez que se crea una copia.
* Posibilidad de confusión entre los respaldos y el proyecto actual.
* Falta de control en los cambios, principalmente en grandes equipos.
* Dificultades al trabajar de manera separada o remota.
* Desconocimiento de casi todas las modificaciones que se han llevado a cabo.

**El descuido en la información afecta la calidad de la edición**. Por
lo que el mantenimiento de la infomación no solo es una tarea secundaria
o «técnica», sino una responsabilidad editorial.

El [**octavo elemento**]{#ocho} metodológico es el control de versiones. En un
videjuego existe la posibilidad de guardar la partida a cada paso o
en algunas situaciones críticas. A partir de estos cambios guardados, 
es posible regresar o probar nuevos caminos. Si el desarrollo es 
satisfactorio, estos otros senderos se convierten en el juego principal.

Un control de versiones cuenta con esos elementos. A partir de un
programa es posible monitorear las carpetas y archivos de un proyecto. 
Se hace posible tener conocimiento de todo lo hecho, sin posibilidades de
confusión ni necesidad de mayor espacio, porque se emplean puntos guardado.

En un videojuego no se respalda el juego una y otra vez para guardar 
una partida, solo se almacenan los datos necesarios para regresar a ese 
punto de guardado. **En un controlador de versiones no hay respaldos,
sino información para volver a cada punto de control**.

Esto implica que el espacio necesario para guardar un proyecto cae 
drásticamente. También resuelve la dificultad de trabajar de manera 
independiente, ya que en el tronco, como en las ramas e incluso en sus
partes pueden colocarse puntos de control en cualquier momento. Así se 
puede saber si se está trabajando con versiones anteriores o si hay 
sobreescritura, porque cada punto de control tiene descripción, fecha, 
autor, identificador e indicador de qué se creó, modificó o eliminó.

En un videojuego el uso de partidas guardadas permiten explorar más a
fondo la trama del juego debido a que en cualquier momento es posible
regresar. En un controlador de versiones hay tal flexibilidad que es
posible experimentar ciertas implementaciones (como *regex*, p. ej.)
sin el peligro de corromper un proyecto. Esto se traduce a que yace
la posibilidad de adquirir mayores conocimientos de edición digital 
sin el temor de arruinarlo todo.

Pero no solo eso. Un proyecto con un control de versiones es lo que
se conoce como repositorio y estos tanto pueden estar en una computadora
como en la «nube». El [**noveno elemento**]{#nueve} es alojar los proyectos
en un servidor para que cualquier colaborador, desde cualquier lado
u hora, pueda «subir» o «bajar» información para realizar su trabajo.

El SSP pasa a ser *single source and online publishing* (SSOP). El
SSOP se utiliza de manera muy extensiva para el desarrollo de *software*
o investigaciones científicas, pero también cabe emplearse en el
quehacer editorial.

La edición ramificada es una expresión particular del SSOP. El nexo es
importante ya que el SSOP se enfoca a un control en los procesos,
principalmente técnicos. Mientras tanto, los elementos metodológicos
de la edición ramificada hacen que este control se traduzca en un mayor
cuidado editorial, donde las tecnologías digitales y la automatización
de procesos sirven de apoyo a quien edita.

La «revolución» digital no va en contra de los conocimientos adquiridos
durante siglos, sino que revisita, depura o elimina técnicas y metodologías
que no son las más pertinentes para llevar a cabo una tarea. En la edición,
la «revolución» digital es la «apertura» del «gremio» al uso de las
tecnologías de la información en pos de técnicas más aptas pero sin
olvidar lo que hace de una obra un «buen» libro.[^1]

[^1]: Se ha insistido en el juego «revolución-apertura» ya que la 
«revolución» en la era digital no siempre se traduce el cambio del
*status quo* sino el adveniemiento de otros actores más «cerrados». 
*El código es ley*, por lo que no solo es necesario pensar la era digital 
como «revolución» sino asimismo como «cierre» o «apertura». Tal vez
una historia de la edición desde un planteamiento de «revoluciones» que 
implican «cierres» y «aperturas» sería interesante…
