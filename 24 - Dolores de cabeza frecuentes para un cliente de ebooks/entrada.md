# Dolores de cabeza frecuentes para un cliente de _ebooks_

A veces cuando solicitamos servicios para la producción de un libro 
electrónico el resultado no es lo que esperábamos. Quizá el fallo
estuvo en la comunicación. Para ello existen ciertas pautas que
nos permiten enmendar estas dificultades.

## 1. Fidelidad: ¿qué tanta uniformidad queremos en el diseño?

Aunque no sea de mi agrado, la mayoría de las personas que buscan
servicios para _ebook_ ya tienen un libro formado. Por lo general
se trata de una obra impresa que a continuación se busca su
correlato digital.

Cuando se da el caso, ya existe una idea precisa de _cómo queremos
que se vea nuestro libro electrónico_. Es decir, tenemos la 
expectativa de que mi proveedor produzca un _ebook_ con un diseño fiel 
a mi maquetación.

Por las posibilidades y limitantes de cada formato, la fidelidad es 
muy variable. ¿Tu obra es narrativa, ensayo o académico? Ten seguridad 
que se puede lograr una alta fidelidad. ¿Tu obra es más artística, 
gráfica o para un público infantil? Quizá tengas que hacer unas 
concesiones en el diseño.

Esto no se debe a que una publicación digital estandarizada no pueda
ser fiel al diseño original, sino a alguno de los siguientes factores.

Uno. Por lo general una obra que no es puro texto corrido basa su
diseño en ese lienzo fijo que traducimos como «página». _Incluso la
rebeldía ante la retícula exige una conciencia del espacio limitado
que se tiene para hacer una propuesta distinta_.

En la publicación digital este lienzo es solo una analogía: siempre 
varía. Por ejemplo, tienes una obra cuyas imágenes, pies de foto o 
recuadros requieren de una ubicación precisa dentro de la «página» o
dentro de un conjunto de elementos. En un _ebook_ veremos una y otra 
vez violentada estas normas de diseño porque la «página» en realidad 
es inexistente, es solo emulada…

Dos. A diferencia del impreso, en la publicación digital dependemos de
qué ha implementado cada _ereader_ o _software_ de lectura. Pese a
que el +++EPUB+++ tiene posibilidades estandarizadas muy amplias,
muchos desarrolladores no las permiten todas en el _hardware_ o
_software_ que te ofrecen.

Por ejemplo, la limitación en la implementación de funcionalidades 
mediante JavaScript, como los formularios o animaciones; el uso de
[hojas de estilo aurales](https://www.w3.org/TR/CSS2/aural.html) para
indicar cómo se ha de escuchar un texto, o el uso de [recursos foráneos](https://w3c.github.io/publ-epub-revision/epub32/spec/epub-spec.html#dfn-foreign-resource),
como la inclusión de archivos +++PDF+++ dentro de un +++EPUB+++. (Suena 
extraño pero en ocasiones es más conveniente poner un largo anexo de 
tablas como +++PDF+++ que recrearlo en +++HTML+++ o convertirlo en 
imágenes).

Tres. Entre más complejo sea el diseño de nuestra publicación, se
requerirá de más tiempo para obtener una mayor fidelidad. Es decir,
el proveedor va a cobrar más por su trabajo por las habilidades y
horas necesarias.

¿Qué tan importante es preservar esta uniformidad a través de los
distintos soportes ofrecidos de nuestra obra? ¿Qué tanto se puede ser
flexible para no elevar de manera precipitada los tiempos y los 
costos?

No es cuestión de qué se puede o no, sino qué me permite el 
distribuidor de la obra —iTunes, Google Play, Amazon, p. ej.— o el 
vendedor de _ereaders_. Pero también, qué tanto estoy dispuesto a 
esperar o a gastar para tener mi _ebook_.

## 2. Velocidad: ¿qué tan rápido quiero tener mi _ebook_?

Existe un prejuicio muy extendido de pensar que el _ebook_, al ser un
soporte «menor», «más sencillo» o «más feo», es más rápido producirlo. 
En un mercado donde el paradigma de objeto de consumo es el impreso, 
se anticipa que otros soportes no estén a su «altura».

Sin embargo, esto no quiere decir que sea más fácil producirlo. La
común subcontratación de «conversión» a _ebook_ explicita que sus
procesos de producción

* no son dominados y muchas veces tampoco entendidos por el profesional
  de la edición,
* requieren habilidades y conocimientos que no formaban parte de la
  tradición editorial y
* se perciben como muy distintos a los procesos tradicionales de 
  publicación.

Esto nos lleva a que es muy conflictivo juzgar la cantidad de tiempo
destinado a un _ebook_ por su aspecto. Tenemos que considerar que mucho
del trabajo implicado en este soporte queda oculto: por lo general son
horas destinadas a la limpieza del formato y no tanto a la creación de
un archivo final.

Con esto tendremos como consecuencia una regla corriente en la contratación 
de servicios: a mayor velocidad, a) menos calidad o b) mayor costo.

¿En realidad tengo prisa en tener en mis manos un _ebook_ si el impreso
aún no está listo, la presentación todavía no ha sido programa o su compra
no representará ni el 5% de las ventas estimadas? ¿Por qué tendría que
tener apuro por publicar si esto va en denuesto de la calidad editorial?

Como proveedores podemos tenerte listo un _ebook_ incluso en horas, pero
es muy difícil asegurar que su calidad editorial o técnica sea la mejor
posible. Con metodologías tradicionales de publicación no es sencillo
tener una obra multiformato buena, bonita y barata.

## 3. Economía: ¿cuánto estoy dispuesto a pagar?

Muchos de nosotros no podremos estar de acuerdo en cuánto respeto hemos
de tener al diseño o a la calidad editorial de nuestras obras. Sin
embargo, será casi unánime que todos queremos lo más barato posible.

Como proveedor rara vez he tenido un cliente que con franqueza me diga
«Quiero lo más barato». Muchas veces como clientes hacemos cabildeo para
negociar el precio más bajo para la idea ya preconcebida de cómo 
queremos que sea nuestro _ebook_.

Al final como clientes esperamos la mejor atención y calidad posible.
Lo problemático aquí es la preconcepción. ¿Qué certeza tenemos que el
resultado que imaginamos sea posible con los recursos que disponemos?

Ojo, no me refiero solo a recursos económicos, sino también técnicos,
de saberes y de contactos. Siempre lo he indicado: cualquiera de nosotros
puede producir libros, sean impresos o digitales. Sin embargo, dejando
a un lado las generalidades,

* ¿qué tanto implica producir una publicación con una alta calidad?,
* ¿con cuántos conocimientos cuento como para poder distinguir entre una
  «buena» o una «mala» ejecución?, y
* ¿a quiénes conozco que puedan ayudarme con el desarrollo, a un «experto» 
  en el tema o a «ese fulano que le hizo un libro a mi abuela»?

No hay regla general donde _a mayor presupuesto, mayor calidad_ si el
único recurso abundante es el económico. Sin recursos técnicos, de saberes
o de contactos, no importa cuánto dinero tengas, es casi seguro que tu 
proyecto no se ejecutará como tú esperabas —o pensarás que te dieron 
liebre, cuando es gato—.

Caso contrario, si se cuentan con todos los recursos, menos el económico, 
existe una gran seguridad que tu proyecto saldrá de la manera planeada 
aunque más tarde de lo esperado. Cada centavo invertido rendirá más 
simplemente porque sabes cómo gastarlos.

¿Qué tan certero es que queremos el mayor rendimiento económico y qué
tanto es pretender ahorrarnos unos centavos? ¿Qué tanto estoy dispuesto
a sacrificar el diseño o el cuidado editorial de mi obra con tal de que
me salga un 10% más barato? ¿Lo vale?

Por ahorrarme un par de pesos o euros preferiré negociar y dar más margen
a mi proveedor, que imponerle tiempos e incluso rechazar servicios que
_creo_ periféricos.

## 4. Atención del proveedor: ¿quiero seguir recomendaciones?

Desconfiar de lo ofrecido por un proveedor o comparar distintos proveedores
me parece una práctica necesaria y saludable. Nuestro historial de 
contrataciones nos ha demostrado que muchas veces el proveedor nos ofrece
lo que más le conviene y no lo que necesitamos.

Pero nos puede llevar a un escepticismo que va en contra de los objetivos 
que buscamos en nuestro proyecto. Casos comunes en la edición tenemos:

* Disminución del presupuesto para la portada de nuestro libro, pese a que
  es su carta de presentación.
* Eliminación de lecturas de pruebas porque _creemos_ que son innecesarias,
  aunque esto nos permite depurar lo más usado o disfrutado: el texto.
* Conversión automática de formatos para ofrecer una mayor cantidad de
  soportes, sin importarnos la experiencia del lector y el sabor que le
  quedará sobre la obra que le ofrecemos.

Si tu libro no tiene el recibimiento que esperabas. Si tus impresos están 
más en la bodega que en circulación. Si tu _ebook_ no llega ni a las diez
descargas. Tal vez no sea por la calidad de su contenido, sino por cómo el 
lector lo juzga a primera vista —la portada—; cuánto cree que te esforzaste 
para dar un producto «terminado» —cuántas erratas detectó—, o qué piensa 
de ti como editor o escritor —qué tanto placer le viene a la memoria al 
leer tu nombre o el de tu editorial—.

Como editores de manera constante cometemos el error de recomendar lo que
nos parece lo mejor sin tener en cuenta el presupuesto. Si como cliente
somos francos de los recursos económicos disponibles y expectativas, va a 
ser más sencillo ofrecerte un servicio a tu medida o ahorrarnos dolores de 
cabeza.

¿Es cierto que quieres escuchar recomendaciones o solo quieres identificar
si un proveedor quiere timarte? ¿Has considerado que algunos proveedores
incluyen en su costo el tiempo destinado a contestar tus dudas?

Imagina que una recomendación para un «buen» libro puede ser tan amplia 
como preguntar, ¿cuál es el mejor vino, cerveza o queso? Estas cuatro cosas 
son objetos de uso, pero también de culto.

Un «experto» te interpelará con un «¿“Bueno” para qué?» o «¿Cuánto quieres 
invertir en un buen _x_?» o «Eso varía mucho, según tu gusto o cuánto 
quieras pagar». 

No te sorprenda que el libro también tiene ese panorama: no es fácil hacer 
una recomendación sin información previa sobre presupuestos o expectativas. 

## 5. TL;DR: Mucho blablablá, quiero la receta

_Too Long; Didn't Read_: ¿cómo puedo contratar a un proveedor de _ebook_
sin frustrarme en el intento?

___Uno_. Si no quieres recomendaciones, sino medios para detectar el timo, 
tendrás que forjarte tu propia opinión__. Lee _blogs_ y documentación antes
que noticias o videos de _reviews_.

___Dos_. Si deseas recomendaciones, sé sincero con lo que tienes y lo que
quieres__. La franqueza ahorra tiempo a todos y facilita pedir más de una
opinión para después compararlas.

___Tres_. ¿Buscas lo más fiel a tu diseño, lo más rápido y barato 
posible?__ El _ebook_ que estás buscando es una plena exportación de cada 
página a imágenes.

Desventaja: el lector no tendrá una buena experiencia. No podrá seleccionar 
el texto —será imagen como el resto de los elementos—. El libro electrónico 
tendrá un peso considerable —las imágenes pesan más que el texto digital—. 

De manera personal lo desaconsejo. No es un _ebook_ sino una emulación y lo 
mejor es utilizar un +++PDF+++ para este propósito si es posible.

___Cuatro_. ¿Ambicionas una gran fidelidad en el menor tiempo posible?__ 
Puedes optar por un +++EPUB+++ fluido o fijo con una extensa hoja de 
estilos personalizada. Fijo si para ti es importante el respeto al diseño 
en relación con la «página».

Desventaja: vas a requerir un presupuesto al menos cuatro veces mayor a un 
+++EPUB+++ recomendado en cualquiera de los otros puntos. No solo considera 
la inflexibilidad temporal: son escasos los proveedores con esta capacidad 
de trabajo.

En mi opinión el _ebook_ no tendría que ser un soporte parasitario o una
metáfora al diseño del impreso. Para el cliente se precipita el costo sin
un beneficio directo al lector —mayor costo de producción igual a un
+++PVP+++ más elevado—. El proveedor tiene que invertir mucho tiempo para 
satisfacer una «necesidad» que al final termina por ser necedad.

___Cinco_. ¿Pretendes lo más fiel y barato posible?__ Se te puede ofrecer 
un +++EPUB+++ fluido con una pequeña hoja de estilos personalizada que 
abarque la «esencia» de tu formación. 

Desventaja: tendrás que ser paciente y tener en cuenta que la fidelidad 
puede no ser mayor al 90%.

Tengo un sentimiento encontrado con este tipo de +++EPUB+++. ¿Cómo podemos 
estar de acuerdo con el grado de similitud? En narrativa, ensayo o
publicación académica la fidelidad puede ser muy aproximada con pocas
líneas de código. 

Sin embargo, en libros infantiles o de arte, revistas, periódicos o 
publicaciones experimentales la vara de medida se relativiza. _La edición 
estandarizada supone un diseño «tradicional» —léase «aburrido»— de la 
obra; hay una gran deuda y elementos por mejorar_.

A esto súmese que por lo general la paciencia es lo que falta en un 
ecosistema de servicios donde hemos consagrado que _entre más rápido, 
mejor_.

___Seis_. ¿Ansias lo más rápido y barato posible?__ Lo tuyo es un 
+++EPUB+++ fluido sin hoja de estilos personalizada o una muy mínima 
—estilos particulares en ciertos encabezados, por ejemplo—. 

Desventaja: no esperes un buen diseño e, incluso, una gran calidad técnica 
o editorial.

Este es el tipo de proyectos que prefiero. El cliente es indiferente e 
incuso agradecido por el diseño por defecto que le ofreces. La comunicación 
es breve. El tiempo de espera es de días. El pago puede salir en en menos
de dos semanas.

No somos los únicos que gustamos de estos trabajos. El mercado está 
inundado de proveedores que pueden ejecutarlos. Para un cliente sin opinión
informada es fácil darles un +++EPUB+++ convertido de manera automática en
detrimento de su calidad editorial o técnica.

Si eres de esos clientes, pide una muestra tangible y verifica ese 
+++EPUB+++ con [EpubCheck](http://validator.idpf.org/). Un proveedor que
conoce sus procesos no tendría que tener ni siquiera una advertencia. Si
las tiene, pregunta el motivo. Procura indicar que lo verificarás con esa 
herramienta, ya que no soporta todas las versiones de +++EPUB+++ 
disponibles.

Si el proveedor no te quiere dar una muestra, pregúntale qué usa para
producir los _ebook_. Si usa InDesign, es probable que no conozca la 
estructura de un +++EPUB+++. Si usa Sigil o Pandoc, cabe la posibilidad de 
que el proveedor sea fiable, pero pide que te haga un +++EPUB+++ versión 3 
y no 2.

Si usa otro programa, búscalo en internet. Con el puro _landing page_ 
puedes darte idea si es un programa para «uso rudo» o un «producto 
milagro». Encuentra editores que hablen sobre el programa en _blogs_ o 
redes sociales. Ve si el desarrollador confía en hacer público el código 
del programa. Indaga si la herramienta ha creado comunidades o sitios de 
soporte técnico.

Si el proveedor se rehusa a decirte lo que usa o no sabe cómo responder
cuestiones básicas de diseño o de versiones, tienes una alerta roja 
enfrente de ti.

¿Listo para solicitar tu primer servicio de desarrollo de _ebook_?
