# Entradas de *blog*

Aquí están presentes las entradas publicadas en el [*blog* de Mariana Eguaras](http://marianaeguaras.com/edicion-digital-2/) 
sobre edición digital.

# Antología

*Edición digital como metodología para una edición global* es una antología
que contiene las entradas. Puedes descargalo en:

* [EPUB 3.0.1](https://github.com/NikaZhenya/entradas-eguaras/raw/master/ebooks/edicion_digital_como_metodologia_para_una_edicion_global.epub).
* [EPUB 3.0.0](https://github.com/NikaZhenya/entradas-eguaras/raw/master/ebooks/edicion_digital_como_metodologia_para_una_edicion_global_3-0-0.epub).
* [MOBI](https://github.com/NikaZhenya/entradas-eguaras/raw/master/ebooks/edicion_digital_como_metodologia_para_una_edicion_global.mobi).

## Donaciones

🌮 Dona para unos tacos con [ETH](https://etherscan.io/address/0x39b0bf0cf86776060450aba23d1a6b47f5570486).

:dog: Dona para unas croquetas con [DOGE](https://dogechain.info/address/DMbxM4nPLVbzTALv5n8G16TTzK4WDUhC7G).

:beer: Dona para unas cervezas con [PayPal](https://www.paypal.me/perrotuerto).

## Páginas hermanas

Taller de Edición Digital: [ted.perrotuerto.blog](https://ted.perrotuerto.blog/).

_Edición digital como metodología para una edición global_: [ed.perrotuerto.blog](https://ed.perrotuerto.blog/).

Pecas, herramientas editoriales: [pecas.perrotuerto.blog](https://pecas.perrotuerto.blog/).

Blog: [perrotuerto.blog](https://perrotuerto.blog).

## Licencia

En el *blog* de Mariana los textos fueron publicados con [licencia de Creative Commons Reconocimiento-NoComercial-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-nc-sa/4.0/),
pero la licencia de origen de estos escritos están bajo [Licencia Editorial Abierta y Libre (LEAL)](https://gitlab.com/NikaZhenya/licencia-editorial-abierta-y-libre).
