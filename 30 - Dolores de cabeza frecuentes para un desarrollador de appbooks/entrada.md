# Dolores de cabeza frecuentes para un desarrollador de _appbooks_

Hace un año empezamos una serie sobre los dolores de cabeza frecuentes
para un [editor](https://marianaeguaras.com/dolores-de-cabeza-frecuentes-para-un-editor-de-ebooks/),
un [cliente](https://marianaeguaras.com/dolores-de-cabeza-frecuentes-para-un-cliente-de-ebooks/)
y un [lector](https://marianaeguaras.com/dolores-de-cabeza-frecuentes-para-un-lector-de-ebooks/)
de _ebooks_. Con esta publicación empezaremos una serie sobre
las dificultades para un desarrollador, un cliente y un usuario
de _appbooks_.

En lo personal prefiero enfocarme a [metodologías ramificadas](https://marianaeguaras.com/edicion-ciclica-y-edicion-ramificada-de-la-antesala-a-la-revolucion-digital/)
de producción editorial. Pero entre los constantes _gigs_ con
características similares, a veces es un buen respiro desarrollar
proyectos de índole distinta. En varias ocasiones esto implica
el interés por desarrollar algún _appbook_.

Sin embargo, ese respiro puede tornarse en una sofocación. __En
el desarrollo de _appbooks_ hay un delicado margen que puede
desestabilizar el proyecto__. En parte por su dificultad técnica,
pero también porque el soporte final tiende a no ser el esperado
por parte del cliente o el desarrollador-editor.

Entonces, hay unas cuestiones básicas que vuelven muy delicado
la publicación de un _appbook_:

* El término «appbook» es ambiguo.
* Los recursos y requisitos mínimos por lo general son ignorados.
* Las expectativas son muy altas.
* La narrativa tiende a no ser convencional.
* El principal aliado es el que por tradición se ha considerado
  la némesis de la lectura y la cultura escrita.
* Las metodologías y las herramientas de producción no son las
  comunes en un contexto editorial.

## El camino de la negación:
   la definición del _appbook_

¿Qué es, después de todo, un _appbook_? La definición mínima
que manejo junto con Programando +++LIBRE+++ros es que __el appbook
es una publicación no estandarizada__.

Pero ahora, ¿qué es una publicación «no estandarizada»? Se trata
de una obra con las siguientes características:

* __Su producción no implica las metodologías y las
  herramientas comunes en la edición__. Está más emparentado
  con el desarrollo de _software_ o de videojuegos que con la
  publicación de un libro.
* __Su reproducción no es sencilla__. Se trata de un producto único
  cuyos frutos rara vez pueden ser reutilizados para otro proyecto.
* __La experiencia de lectura pretende no ser la misma a la ofrecida
  por lo que conocemos como «libro»__. Los _appbooks_ tienden a ser
  ambiciosos respecto a sus recursos narrativos: ¿cómo leerlo
  ---si es que en realidad se «lee»---?, ¿cómo desplegar el argumento
  o la historia que vaya más allá de lo que ofrece el paradigma de
  la página?
* __Su distribución o su comercialización no es a través de los
  canales habituales para el editor__. Los _appbooks_ se ofrecen
  como lo haría un programa de cómputo.
* __Su posición en el mercado por lo regular no es redituable__. Este
  tipo de proyectos editoriales destacan por sus costos y bajo consumo;
  incluso menor al de un libro electrónico ---que en habla hispana
  no supera el 5% de la facturación en el sector---.

__Un _appbook_ se define a través de lo que no tiene en común
con otro tipo de publicación__ como un libro, un periódico o
una revista. Esto se debe a que el producto es tan diverso que
no es sencillo meterlo en una sola definición.

Aún así contempla varias semejanzas con otros tipos de productos
editoriales «no estandarizados» como el _fanzine_, algunos libros
objeto, los juegos de mesa, los mazos de cartas u otro tipo de
producto editorial que contemple más de una de las características
mencionadas. Todos estos soportes, aunque dispares, tienen estos
elementos en común.

La diferencia específica del _appbook_ es que, a diferencia de
los otros soportes, se trata en principio de _software_. Un conjunto
de unos y ceros producidos por un compilador a partir de varios
archivos elaborados con lenguajes de programación. No hay nada
en esa descripción que implique alguna clase de tradición editorial.

Entonces, ya pueden imaginarse cuando alguien viene con la intención
de que lo apoyes para desarrollar un _appbook_. __Entre lo que
el cliente se imagina que es un _appbook_ a lo que el editor
cree y puede hacer hay una enorme diferencia__.

Si todavía es complicado explicarle al cliente las posibilidades
y los límites de un libro electrónico, la asertividad en la planificación
de un _appbook_ es una tarea más compleja. ¿Cómo indicarle al
cliente que su idea, aunque buena, es inviable por los recursos
técnicos, económicos o de tiempo con los que se cuenta? ¿Cómo
explicarle que su gran proyecto tiene graves deficiencias en
la experiencia de usuario? Pero con mayor ahínco, ¿cómo encaminar
la discusión para dejar en claro que quizá lo que llama «_appbook_»
puede ser más bien otra clase de producto no editorial o algo
que más bien podría ser un _ebook_?

En fin, __por lo general el cliente y el desarrollador concuerdan
en lo que no quieren, pero rara vez están en común acuerdo de
lo que desean__.

## Hacer más con lo mismo:
   el nacimiento de un _appbook_

Y aún con esa falta de consenso muchos _appbooks_ inician su
desarrollo. Al tratarse de un producto no estandarizado ---no
hay metodologías, herramientas o reglas de formación claras---,
los costos se disparan.

Incluso el _appbook_ con menos gracia implica una serie de procesos,
regresos y esfuerzos que en términos económicos es hacer menos
con lo mismo. Pero esto en más de una ocasión es obviado.

__La producción de un _appbook_ muchas veces supone la elaboración
de otro soporte con los mismos recursos con los que se cuenta
para llevar a cabo la publicación de los formatos ordinarios__
como el +++PDF+++ de impresión, el +++EPUB+++ y el +++MOBI+++.

Entonces ya tenemos un problema de ausencia de acuerdos, ahora
súmese un punto de partida dentro de la precariedad económica
y laboral. Con estos dos factores ya es posible imaginarse que
el producto final tendrá sus deficiencias. Sin embargo, __el
entusiasmo del cliente y del editor puede ser tan grande que
no les permite ver las posibilidades reales del proyecto__.

Con mucha facilidad el desarrollador también puede creer que
el proyecto es posible con los recursos actuales. Por desgracia
esto es un reflejo de una falta de experiencia o un deseo de
producir algo distinto.

## Idea rica, ejecución pobre:
   el crecimiento de un _appbook_

Entre discrepancias y carencias de principio también tenemos
un tercer factor que determina la calidad del _appbook_: las
capacidades de quien lo desarrolla. __Las necesidades técnicas
que requiere un editor para desarrollar un _appbook_ no son fáciles
de adquirir__. Así como el perfil más idóneo ---el programador
de profesión--- para su ejecución tiende a estar poco familiarizado
con el mundo del libro.

De uno y otro lado hay buenas ideas para desarrollar un _appbook_.
Pero __en un _appbook_ la falta de capacidad técnica o de cuidado
editorial será lo primero que resalte__.

Por un lado tenemos la idea del editor sobre cómo narrar la historia
o cuál presentación del texto o lectura proponer. Y en esta búsqueda
los recursos técnicos limitan la ejecución de la idea porque
esta solo es posible a través de los saberes con los que se cuenta.
No sorprenderá que, para alguien con más formación en la producción
de libros que de _software_, se termine por ofrecer un producto
con ciertas deficiencias en la experiencia de usuario, en la
estructuración de datos o en la optimización del producto.

Ante estas carencias, precariedades y faltas de consenso tiende
a ser preferible desarrollar _appbooks_ entre editores u otros
colaboradores que ya llevan tiempo trabajando juntos. De lo contrario
se corre el riesgo de generarse muchas tensiones.

Por otro lado están las ideas del programador sobre cuáles son
las tecnologías o los lenguajes más pertinentes para ejecutarlos.
No obstante, en un _appbook_ la manera de presentar el texto
es muy importante: una característica que tiende a ser pasada
por alto por una profesión que se caracteriza por ver al texto
como un tipo de dato más en su inventario.

Debido a ello los grupos de trabajo que surgen a través de equipos
de desarrollo tienden a incorporar una gran cantidad de profesionales.
Esto eleva los costos de producción y aumenta los tiempos. Pero,
principalmente, __relega a la figura del editor a un trabajo
secundario en lo que se supone es experto: el texto y el «libro»__.
A varios profesionales del libro esta organización del trabajo
es poco digerible.

__El desarrollo de un _appbook_ requiere la fusión de varias
disciplinas en pos del texto en un contexto de producción donde
este ha perdido relevancia en la producción cultural de «vanguardia»__.
Así que en las grandes ideas aún queda un largo camino por recorrer
para depurarlas y ejecutarlas de la manera más fiel a como se
proyectan.

## De la vanguardia al cliché:
   la historia de vida del _appbook_

«De la página a la pantalla» es una frase muy común cuando hablamos
de edición digital. Entre uno y otro soporte existen traslados
y analogías aunque también rupturas y necesidad de producción
de nuevos conceptos. __El _appbook_ pretende posicionarse en
el lado de la vanguardia o transgresión y no en la herencia y
la tradición__.

Pero lo que surge como una idea «vanguardista» pronto se revela
como otro lugar común dentro de la pretensión de producir _el_
producto que reemplazará al libro. Un soporte no puede revolucionar
una industria si no demuestra que su elaboración es posible dentro
de una línea de producción en lugar de tratarse de una excepción.
__En los _appbooks_ se da el extraño caso de querer ser un producto
de vanguardia al mismo tiempo que busca su aprobación como soporte
válido para una industria__.

El resultado que tenemos es un producto del cual se puede hablar
mucho, pero su uso por lo general no es extraordinario ni revolucionario
ni, mucho menos, cumple con lo que promete: una nueva forma de
pensar la presentación de material textual.

## La vuelta de tuerca:
   el videojuego como recurso para el _appbook_

En la búsqueda de nuevas propuestas narrativas, de uso y de reproducción
del texto el videojuego ofrece un buen recurso. Pienso al videojuego,
su cultura y sus modos de producción como un «recurso» porque
__en el desarrollo de _appbooks_ existe una constante aspiración
de no ser confundido por un videojuego__.

En parte porque el _appbook_ dentro de su transgresión aún quiere
hacer honor en darle prioridad al texto. Pero también debido
a que el _appbook_ pensado como un tipo de videojuego provoca
al menos dos conflictos.

__El _appbook_ como videojuego lo hace susceptible a ser catalogado
y apreciado según los parámetros de su jugabilidad y no de su
calidad editorial__. Esto no es del agrado para la mayoría de
los editores quienes, por experiencia personal, se jactan de
carecer de un contacto con la industria del juego.

Al final, el libro en este contexto quiere seguirse pensando
como _el_ producto cultural por excelencia, cuando la atención
del público y del mercado hace mucho se inclinó hacia los productos
cinematográficos, el _software_ y el videojuego.

__El _appbook_, a diferencia de varios productos editoriales
«por excelencia», tiene la intención de entretener al usuario__.
Se trata de una función que muchos editores no tienen el interés
de ofrecer, debido a otras tareas consideradas más relevantes
como la reproducción del conocimiento o de las técnicas artísticas
o literarias.

Por otro lado, __el _appbook_ como videojuego tiene la desventaja
comercial de tener que competir como un producto dentro de la
industria del juego__. Si de entretenimiento de trata, muchos
de los que revisamos este _blog_ sabemos que el libro en nuestro
tiempo se trata más como un producto formativo o de preservación
que de diversión.

La percepción del videojuego como «recurso» para el _appbook_
no solo sirve para esquivar estas dificultades. Cuando una área
de nuestra cultura es tratada como recurso, existe ya un nexo
para pensarlo como una entre varias posibilidades.

Por el momento el videojuego se ha mostrado como el recurso más
útil y fácil de asimilar entre un amplio público que comprende
a los editores, los lectores y los _gamers_. Pero bien podrían
buscarse otros recursos más apegados al campo del desarrollo
de _software_ u otros rubros del desarrollo tecnológico.

__¿Cómo podría ser un _appbook_ que propone una narrativa para
presentar el texto, pero al mismo tiempo propone un lenguaje
o una herramienta para producirlo y un soporte para reproducirlo?__

## Un final abierto:
   las enseñanzas que deja la producción de un _appbook_

El panorama es desolador… pero si se percibe la producción de
_appbooks_ como un objeto que ha de «salvar» a la industria del
libro en un campo de competencia entre otros productos culturales.

__El _appbook_ tal vez no ofrezca el nuevo paradigma para la
industria, pero quizá sí ofrece una serie de conocimientos que
pueden ayudarnos a repensar lo que es la edición__.

En su falta de cuidado técnico o editorial existe un aprendizaje
muy importante: la producción de _appbooks_ sacan a quien hace
libros de su zona de confort. La obligación de usar metodologías
o herramientas ajenas a la tradición editorial ayuda a ofrecer
otra clase de recursos para cuidar, experimentar, imaginar y
repensar el material central de nuestra profesión: el texto.

Entonces no es un final que muchas personas en la industria del
libro esperarían. __La producción del _appbook_ muchas veces
causa sofocación, aunque también fomenta la generación de nuevos
enfoques para reflexionar sobre la práctica y la teoría editorial__.

El desarrollo de _appbooks_ no es sencillo pero ¿cuál tipo de
aprendizaje lo es?
