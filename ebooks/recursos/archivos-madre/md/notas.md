
Se ha insistido en el juego «revolución-apertura» ya
que la «revolución» en la era digital no siempre se traduce el cambio
del *status quo* sino el advenimiento de otros actores más «cerrados».
*El código es ley*, por lo que no solo es necesario pensar la era
digital como «revolución» sino asimismo como «cierre» o «apertura». Tal
vez una historia de la edición desde un planteamiento de «revoluciones»
que implican «cierres» y «aperturas» sería interesante…

Un falso positivo se da cuando una palabra es marcada como errónea, 
aunque en realidad sí es una palabra existente, pero no contenida en 
el diccionario.
