# ¿Qué son la conversión y el desarrollo de _ebooks_?

El mercado ofrece una gran diversidad de proveedores de servicios para la
producción de _ebooks_ estandarizados ---+++EPUB+++, iBooks, +++MOBI+++---.
Unos son más baratos que otros; algunos mejor que los demás.

Entre editores y autores rara vez está clara estas diferencias de precios.
Por lo general obedecen a la manera en como se produce el libro electrónico.
Existen al menos dos medios en como puede lograrse: mediante conversión o
desarrollo.

Veamos en qué consiste cada una.

## Conversión: en la piel del _ebook_

__El medio en apariencia más barato y sencillo para producir libros electrónicos
es, sin duda, la conversión__. A través de un programa, un _plug-in_ o una
plataforma _web_ el usuario convierte un archivo a las salidas deseadas.

Muchas de las veces la conversión tiene como archivos base documentos de Word o
publicaciones maquetadas en InDesign. Si se trata de documentos de Word, el
procedimiento más sencillo es utilizar las herramientas que nos ofrece el
gestor de bibliotecas [Calibre](https://calibre-ebook.com). Con un par de clics
tendremos diversos formatos electrónicos de nuestra obra, incluyendo +++EPUB+++
para cualquier dispositivo excepto Kindle y +++MOBI+++ para cualquier _ereader_
de Amazon.

Cuando se trata de documentos de InDesign, la producción del +++EPUB+++ se
lleva a cabo a partir de una simple exportación del documento a ese formato.
Con ello es posible subir ese archivo a cualquier distribuidor y serán ellos
quienes se encarguen de convertirlo a sus propios formatos propietarios.

¿Muy sencillo, no es cierto? Sin embargo, podemos llegar a tener al menos tres
grandes problemas:

1. Los metadatos de nuestra obra son erróneos.
2. Los distribuidores la rechazan por tener errores técnicos.
3. El archivo está bien, pero su diseño es un desastre.

Es aquí donde __el servicio de conversión muta en curaduría__. Varias de estas
dificultades pueden solventarse usando las configuraciones avanzadas de
exportación o usando utilidades con interfaz gráfica. Es común que varios de
estos detalles se arreglen con un par de clics.

No obstante, para un usuario promedio puede llegar a ser un proceso tortuoso.
Esto hace tomar la decisión de contratar a un proveedor cuyo trabajo será
convertir el archivo con la calidad deseada.

En algunas ocasiones las dificultades pueden ser más complejas. Uno de los
casos que puede darse es una serie de errores porque la estructura +++HTML+++
no es correcta. Otro ejemplo más es una deficiencia en el diseño que requiere
modificación de la hoja de estilos +++CSS+++.

Aunque se trata de aspectos y lenguajes distintos, estos y __varios de los
inconvenientes más graves en los procesos de conversión tienen un mismo
origen: código redundante, ininteligible y sobrante__.

Esto se debe a la manera en como se convierte el documento a partir de las
estructuras generadas por el editor o el autor. En efecto, varias de las
dificultades se originan porque el documento no fue marcado a partir de estilos
de párrafo o de caracteres y en su lugar se empleó la edición directa de cada
una de las estructuras que comprenden la obra.

En algunos casos sí se usan los estilos. Sin embargo, en aras de mejorar el 
aspecto visual del impreso, la persona que diseña hace uno que otro cambio
manual: un ajuste de puntaje, algunos saltillos de línea, etcétera.

Todo esto que no se ve, _pero que está presente en la estructura_, de una u
otra forma el programa tratará de traducirlo a las estructuras convenientes
para un libro electrónico. __Ya pueden imaginarse lo desastroso que será el
proceso cuando se hacen cambios de diseño a diestra y siniestra__…

¿Quieren convertir sus libros sin ayuda y que al menos les parezcan aceptables?
Todo comienza en el uso sistemático de los estilos de párrafo o de caracteres
que nos ofrecen los programas que utilizamos para editar o maquetar nuestra
obra. No importa cuál _software_ utilicen, todos tienen esta posibilidad.

## Desarrollo: en las entrañas del _ebook_

Si ya han pasado por el medio anterior, pero se han frustrado en el intento,
no es porque la producción de _ebook_ sea un proceso del otro mundo. __La
dificultad en producir un libro electrónico como uno quiere es casi siempre
por fallas metodológicas__-

En los formatos impresos estamos acostumbrados a trabajar a partir del diseño.
Es decir, _por como vemos la obra esperamos que así sea su resultado_. De ahí,
la conversión se encarga de «traducir» su diseño a estructuras y hojas de
estilos.

Para el caso de los libros electrónicos el enfoque es inverso. __En un _ebook_
las estructuras condicionan al diseño__. Un libro electrónico limpio, ligero
y lindo ---desde ahora las llamaré las «tres eles»--- se obtiene cuando se cura
y se cuida el código que está de fondo. Con esa consistencia y simplificación
es muy sencillo dar órdenes uniformes de cómo mostrar la publicación.

Los medios para conseguir este control y resultados tienen una regla muy
sencilla: la publicación no se convierte en su formato final sino, a lo sumo,
a un formato intermedio.

Sigamos con el ejemplo de los documentos de Word o de InDesign. En lugar de
convertirlos a +++EPUB+++, se convierten a +++HTML+++. A continuación, con
algún programa ---el más popular es [Sigil](https://sigil-ebook.com)--- se
empiezan a trabajar el resto de los archivos que comprenden un libro electrónico
para al final generarlo.

Esta manera de trabajar tiene __ventajas muy claras__:

* Mayor control.
* Mayor uniformidad.
* Mayor calidad editorial.
* El producto final es de tres eles.

Pero también __un par de desventajas__:

* Mayor tiempo de producción.
* Bifurcación del proyecto o esperar el cierre de la edición del impreso.

Para una persona que viene de un enfoque visual de producción de libros,
hay __otras desventajas__:

* Se requieren conocimientos al menos de +++HTML+++ y +++CSS+++.
* Implica trabajar pensando en estructuras y luego en diseño.
* Se emplean diversas herramientas ajenas al diseño editorial.

Por estos motivos, es más común ver que este tipo de trabajo sean desempeñados
por proveedores externos. __Entre conversores o desarrolladores de _ebooks_,
los últimos cobran más por sus servicios, porque son más especializados__.

¿Quieres producir libros electrónicos con la mayor calidad posible? Vale la
pena empezar a ver a las publicaciones como estructuras y a conocer lo que
comprende las entrañas de un +++EPUB+++.

## Recomendación: ¿qué quiero y qué puedo esperar?

Como desarrolladores de +++EPUB+++ tendemos a cometer el error de pensar que
todos los clientes esperan el mejor resultado posible. En varias ocasiones me
he tropezado con que el cliente lo único que quiere es poder leer su obra en
su dispositivo. Entonces, ¿qué te conviene más, la conversión o el desarrollo?

__Si tus necesidades son personales__ o que no van más allá de un círculo
cercano, en definitiva __lo tuyo es__ realizar __la conversión con tus
propios medios__. Sea con InDesign, Calibre u otra plataforma, lo que te
interesa es tener el archivo lo más pronto y barato posible, ¿para qué contratar
a alguien?

Puede darse el caso que quieres ofrecer tu obra de manera gratuita. Quizá ya no
es para tu círculo cercano, sino __para cualquier internauta, pero__ tienes
__muy poco presupuesto__. En este caso __lo recomendable es__ elegir a alguien
que te ofrezca __un servicio de conversión__, que sea esta persona quien lidie
con los metadatos, el diseño y la validación técnica.

__Si representas una editorial donde el libro electrónica hablará mucho de tu
sello__, vas a querer ofrecerle a tu lector el formato con la mejor calidad y
diseño posible. En este sentido __la recomendación es__ tener un proveedor
confiable que te dé __un servicio de desarrollo__, no de conversión. De esta
manera existe la seguridad de poder tener publicaciones con diseños
personalizados y uniformes, más si se trata de colecciones.

No obstante, __si lo que te interesa es vivir de esto o que todos los procesos
de publicación digital se lleven a cabo de manera interna, no hay otro camino
sino aprender las entrañas de la publicación electrónica__. No importa cuántas
capacitaciones o talleres se tomen, lo primero que se tiene que hacer es
aprender las estructuras internas de cada uno de los formatos finales que se
están buscando.

Para difícil, ¿cierto? Sin embargo, todo se simplifica cuando se concibe que
__tanto el formato para impreso como el digital pueden partir de la estructura
al diseño__, con lo que se evitan esperas o bifucarciones. En su lugar, se abre
el panorama para empezar a concebir a la edición como automatización y
multiformato. Pero eso es otra historia…
