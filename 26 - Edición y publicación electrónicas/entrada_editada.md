# 26. Edición y publicación electrónicas; como proceso, producto y servicio 

La edición y la publicación son dos términos muy emparentados que, por
lo general, usamos como sinónimos. En la mayoría de los casos esto no
causa ningún problema. Sin embargo, hay veces que sí es necesario tener
presente que se trata de cosas distintas.

Para esta entrada retomo un poco [un artículo](https://marianaeguaras.com/editar-y-publicar-no-son-sinonimos)
publicado por Mariana. Pero desde mi principal contexto profesional: el
desarrollo de publicaciones digitales.

## Edición electrónica

Con la edición buscamos producir un objeto a partir del material textual
y gráfico que nos da el autor o el cliente. En general, se trata de una
serie de procesos para dar con una salida legible y accesible al lector.

Esto implica que existe una diferencia cualitativa entre el material de
base y los formatos finales. **El [quehacer editorial](https://marianaeguaras.com/como-las-herramientas-analiticas-pueden-mejorar-el-quehacer-editorial/)
existe para mejorar la calidad de la obra**.

Gran parte de los procesos editoriales ocurren tras bambalinas. En
muchas ocasiones se dan por supuestos. En efecto, **la edición es una
profesión, un arte y una tradición**. Pero **también es un método** que
implica la ejecución de procedimientos. Este se hace muy claro en un
contexto de edición digital en el que se buscan múltiples formatos
finales.

**¿Qué es la edición digital?** No es, como podría pensarse, el *ebook*.
Por supuesto, la edición digital *produce* libros electrónicos. Sin
embargo, la edición es un proceso, no un producto. **La edición
electrónica es el desarrollo de** formatos ---+++PDF+++, +++EPUB+++,
etcétera--- **a través de una computadora**.

Si usas una computadora para hacer publicaciones, no importan los
programas o procesos que utilices, ya es edición digital. No se trata
con tinta o acetatos y papel, sino con bits que se muestran como fuentes
y píxeles y que, quizá, una impresora los traduzca en material tangible.

Este desarrollo de formatos en sí no implica su publicación. Al editar a
través de una computadora el material no necesariamente llega al
público. Hasta este punto aún nos encontramos tras bambalinas.

Solo el editor, los colaboradores y el autor o el cliente tienen
conocimiento de que la obra *se está editando*. Es decir, que la
publicación está en proceso de mejorar su calidad antes de su
disposición al público.

## Publicación electrónica como proceso

Una vez que los involucrados en la edición de una obra dan su visto
bueno, puede procederse a su disposición pública. En un contexto de
publicación impresa el procedimiento empieza en la preprensa, pasando
por su impresión, distribución y comercialización en los puntos de
venta.

**La edición es la gestación de la obra detrás de un muro; la
publicación como proceso es el salto de esa muralla al exterior**. Las
fases habituales implican que la obra, al publicarse, se «abre» al
lector.

**¿Qué es la publicación electrónica como proceso? La disposición
pública del *ebook* en alguna plataforma**. El procedimiento implica la
subida de archivos a sitios como Amazon, iTunes o Google Play para que
el usuario pueda adquirirlos y leerlos.

Este proceso puede llegar a ser un dolor de cabeza. Si bien las tres
plataformas mencionadas con anterioridad se llevan la mayoría de la
cuota del mercado, existen cientos de sitios en donde es posible
comercializar la obra.

Para facilitar la tarea, existen servicios de distribución donde es
posible llevar la publicación a centenares de tiendas electrónicas desde
una sola plataforma, por ejemplo, [Bookwire](https://www.bookwire.de/es).

## Publicación electrónica como producto

La publicación también se usa para señalar a un objeto. En la tradición
editorial la publicación se trataba de un impreso. Ahora también es el
archivo que el lector lee desde un dispositivo electrónico.

Por ello, **la publicación electrónica como producto es un formato que
solo puede usarse a través de un dispositivo**. El impreso se trata de
un objeto tangible que tomamos con nuestras manos. La publicación
digital es un archivo que únicamente puede leerse desde una computadora,
tableta o teléfono inteligente.

Con el advenimiento de la edición digital fueron estos nuevos formatos
los que se llevaron el foco de atención. En la actualidad, lo más
relevante de la «revolución» digital es el cambio que ha generado en la
infraestructura y en los procesos para producir una publicación.

## Edición y publicación electrónicas como servicio

La transformación de la industria editorial está en su comienzo. Décadas
atrás de su surgimiento y experimentación están abriendo paso a
procedimientos cada vez más maduros. Sin embargo, aún estamos lejos en
volver a tener el control de los principales procesos, como ya se tenían
en el contexto análogo.

En esta situación, como editores, autores o clientes podemos sentirnos
un tanto abrumados. En diversos espacios dedicados a la edición y la
publicación vemos la gran cantidad de ofertas para poder pasar de
nuestra materia prima a una obra con alta [calidad editorial](https://marianaeguaras.com/publicar-con-calidad-editorial-cuatro-pilares-de-la-produccion-de-un-libro/).

Ahí, la edición y la publicación ya no son solo tratadas como procesos o
producto. **El contexto digital ha permitido la proliferación de la
edición y de la publicación como servicio**.

No es que antes fuera inexistente. Las [ediciones por encargo](https://marianaeguaras.com/libros-por-encargo/)
son una buena muestra de ello. La diferencia radical es que **en la edición
y en la publicación electrónicas es cada vez más común que profesionales de
la edición ofrezcan sus servicios**. La organización tradicional de un
autor que manda un texto para ser dictaminado, editado y publicado no es
ya el único ecosistema.

Poco a poco los ingresos bajo concepto de «servicios» empiezan a generar
un ecosistema editorial distinto. Antes, el autor no absorbía el riesgo
en la inversión, por lo que a cambio cedía sus
[derechos](https://marianaeguaras.com/derechos-de-autor-diferencia-entre-morales-y-economicos/)
al editor ---la relación «tradicional» autor-editor---. Ahora el autor
paga al profesional de la edición, mientras que este realiza un servicio
del cual el autor no pierde los derechos.

Al menos eso se supone… La popularización de este ecosistema en un
imaginario donde persiste la relación tradicional autor-editor genera
más de un problema. En buscadores de internet o redes sociales diversos
autores encuentran un sin fin de ofertas de servicios editoriales que
les prometen hacer de su obra un
[*bestseller*](https://marianaeguaras.com/libro-longseller-mejor-uno-bestseller/).

Como la mayoría de los casos, los autores noveles desconocen los
ecosistemas editoriales, es común que varias de esas ofertas sean
apócrifas. Las estafas pueden ser de varias maneras, la «editorial»:

- utiliza un nombre semejante a una editorial de renombre para simular
  que el autor será parte del catálogo de la editorial original;
- cobra una suma irreal e intenta convencer al autor de que es el
  costo estándar por editar y publicar;
- presume de su alta calidad editorial cuando en realidad publican la
  obra con un deficiente cuidado, si lo hay;
- promete que la obra estará en librerías y cobra por la impresión de
  miles de ejemplares, cuando en realidad imprime unas decenas que
  nunca se distribuyen;
- asegura fama al autor, pero simula las relaciones públicas
  habituales como presentaciones de libros, entrevistas o promoción de
  la obra, o
- garantiza que el trabajo se realizará en un par de semanas y pide un
  pago completo para luego desaparecer.

La manera de evitarlo es exigir claridad al posible editor de tu obra
sobre los precios, los tiempos, los servicios que prestará y en quién
tendrá los derechos.

Mariana es una de las editoras que como autor necesitas. Para satisfacer
todas tus necesidades, hemos habilitado un [nuevo servicio](https://marianaeguaras.com/ebooks/)
para ofrecerte la edición y la publicación electrónica de tu *ebook*.

Ahora ya es posible desarrollar tu obra en formato estándar +++EPUB+++
para todos los distribuidores y en +++MOBI+++ para ser descargado
directamente en dispositivos y aplicaciones Kindle; además tendrás
disponible algunas estadísticas de tu obra como las de [este ejemplo](http://ed.perrotuerto.blog/ebooks/recursos/epub-automata/logs/pc-analytics/analytics.html).

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «Edición y publicación electrónicas».
* Título en el _blog_ de Mariana: «Edición y publicación electrónicas; como proceso, producto y servicio».
* Fecha de publicación: 27 de enero del 2019.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/26%20-%20Edici%C3%B3n%20y%20publicaci%C3%B3n%20electr%C3%B3nicas/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/edicion-y-publicacion-electronicas-como-proceso-producto-y-servicio/).

</aside>
