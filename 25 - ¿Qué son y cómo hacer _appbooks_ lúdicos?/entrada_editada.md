# 25. ¿Qué es y cómo hacer _appbooks_ lúdicos?

Muchos de nosotros cuando hablamos de _appbooks_ se nos viene
a la mente un tipo de aplicación muy particular. Se trata de una
_app_ que desde una tableta nos ofrece un libro muy interactivo;
por lo general, con gráficos 2D y para un público infantil. Ejemplos
concretos son [Alice in America](https://itunes.apple.com/es/app/alice-in-america/id422073519?mt=8)
o la antología de [Allan Poe](https://itunes.apple.com/es/app/ipoe-vol-1-edgar-allan-poe/id507407813?mt=8).

Como lo mencionaba en [otra entrada](https://marianaeguaras.com/appbooks-y-realidad-aumentada-no-todo-son-ebooks),
__los estudios de videojuegos tienen mayor ventaja__ para el
desarrollo de este tipo de publicaciones. Pero eso no ha de intimidarnos
porque, por fortuna, el sector editorial aún está en proceso
de experimentar con esta clase de libros electrónicos sin el
temor de no satisfacer expectativas editoriales.

En realidad, el sector aún desconoce bastante de la edición y
la publicación digitales. No se diga sobre _ebooks_, cuyo modo
de producción dista mucho de los procesos editoriales tradicionales.
Estos _appbooks_ están más cercanos al desarrollo de _software_
que de la edición de libros.

Esta clase de _appbooks_ se desarrolla de manera más eficiente
si utilizamos un motor de videojuego. Sea que quieras aprender
a hacerlo o buscar un equipo que te apoye, un primer paso es
saber qué herramientas se pueden utilizar.

## Del centro a la periferia editorial

En la tradición editorial y [cíclica](http://marianaeguaras.com/edicion-ciclica-y-edicion-ramificada-de-la-antesala-a-la-revolucion-digital)
estamos acostumbrados a trabajar con maquetadores de texto. El
estándar de la industria es la _suite_ de Adobe, que ofrece al
editor un entorno y flujo de trabajo único.

En [la edición ramificada](http://marianaeguaras.com/edicion-ciclica-y-edicion-ramificada-del-surgimiento-al-tronco-y-las-ramas)
se hace uso extensivo de lenguajes de marcado, notaciones de
objetos y _scripts_. Aquí ya no existe un entorno único y en
varios casos tampoco hay interfaces gráficas, lo que provoca
que sea un modo de trabajo minoritario.

Si bien tienen sus diferencias, ambos tipos de edición buscan
salidas estandarizadas para la industria. Para impreso, +++PDF+++;
para digital, +++EPUB+++ ---y de ahí formatos propietarios de
Apple o Amazon---, +++XML+++ o +++JSON+++.

__El desarrollo de _appbooks_ se distancia del modo tradicional
de hacer libros y de los formatos esperados en el quehacer editorial__.
La planeación y ejecución siguen metodologías del desarrollo
de _software_.

En este contexto, para crear _apps_ como las de Alice o Poe se
requiere de una organización para el desarrollo de videojuegos.
[¿Videojuegos y libros?](https://marianaeguaras.com/el-videojuego-y-el-libro-la-gamificacion-de-la-edicion),
para muchos editores se trata de una extraña alianza, aunque
existen varios antecedentes previos a la entrada del código en
la edición donde se busca que el lector _juegue_ con el libro.

El libro como objeto artístico o como receptáculo de información
pasa a ser un objeto lúdico. El grueso editorial no ha prestado
mucho interés en publicaciones para _jugar_.

Si te dedicas a la publicación de crucigramas o de sopas de letras
con seguridad conoces de primera mano el desdeño de los editores
que se dedican a libros _serios_. Al parecer el _juego_ se hace
sublime cuando se usa para géneros literarios tradicionales,
como la novela _Rayuela_.

En la periferia del libro serio es donde nacen otra clase de
publicaciones con diferentes intenciones hacia el lector. Este
es el caso no solo de los libros de autoayuda, sino también de
las publicaciones cuya pretensión es entretener.

__El centro sigue la ley del papel y en su periferia el bit es
la norma _de facto_.__ Ahí es donde los _appbooks_ lúdicos pueden
desarrollarse. Lejos del editor, más cercanos al desarrollador
de videojuego y entusiastas.

## Surgimiento de un híbrido

Hace ya un tiempo que existen paqueterías de _software_ pensadas
exclusivamente para el desarrollo de videojuegos: los motores
de videojuego. Y los _appbooks_ lúdicos son un híbrido.

Por un lado, los _appbooks_ buscan que el lector juegue mientras
lee. Los editores conscientes de localizar su génesis de los
_appbooks_ en la edición, quieren dar primacía al contenido textual.
Por el otro, en el deseo de ofrecer un mayor entretenimiento
se valen de herramientas ajenas a los modos de producción hegemónicos
en la industria del libro. Su aliado ---debido a que no lo reconoce
como parte de su familia--- es el videojuego.

__De la cultura del libro el _appbook_ toma su concepción como
un producto editorial. De la cultura del videojuego usa sus métodos
de producción para concebirse.__ Hay claras ventajas al respecto:

- Permite experimentar con otro tipo de interacciones para
  los lectores, provocando incluso nuevas experiencias de lectura.
- Es atractivo para un público joven que ha crecido con dispositivos
  digitales porque puede funcionar como un soporte introductorio
  a la cultura del libro.
- Se trata de un nicho de mercado relativamente virgen _para
  el sector editorial_, por lo que es una fiebre del oro y
  un nuevo Viejo Oeste.

Pero también hay desventajas:

- Por dar prioridad al contenido textual se suelen limitar
  los tipos de jugabilidad, por lo que el usuario puede aburrirse
  con más facilidad.
- Por ser productos principalmente experimentales no tienden
  a tener buena calidad, lo que puede provocar una pérdida
  de la inversión.
- Por tratarse de propuestas que compiten con otro tipo de
  contenidos digitales el lector juzgará su calidad en comparación
  con otros _media_, como películas o videojuegos, ignorando
  que el primer juicio debería darse en relación con otros
  libros.

## Motores de videojuego

Un [motor de videojuego](https://es.wikipedia.org/wiki/Motor_de_videojuego)
facilita al desarrollador la gestación de una serie de productos
interactivos, no únicamente videojuegos. Por lo general, los
motores son un entorno de desarrollo integrado que da casi todas
las herramientas necesarias para crear esta clase de productos.

Los motores pueden ser para crear experiencias 3D o 2D. Pueden
soportar varios lenguajes de programación. Así como pueden usarse
en diferentes sistemas operativos y tener distintas salidas,
no solo para tabletas.

Lo anterior da flexibilidad, ahorra tiempo y permite concentrarse
en elementos más emocionantes: el diseño, los gráficos, las animaciones,
las experiencias de usuario, las mecánicas de juego o la trama.

Existen varios motores de videojuegos. Algunos de ellos propietarios,
por lo que no se tiene acceso al código o quizá sea necesario
un pago. Otros son abiertos o libres, por lo que pueden usarse
y modificarse sin problemas.

A continuación veremos algunos de ellos.

### [Unity](https://unity3d.com/)

- Licencia: propietaria
- Costo: gratuito - $125 dólares al mes
- Sistemas operativos: Windows,  macOS y Linux
- Salidas: +25, incluyendo _web_, móviles, +++PC+++ y videoconsolas
- Lenguajes de programación: C#
- Soporte +++VR+++: sí
- Soporte +++AR+++: sí

Sin duda, el motor más popular es Unity. Los sistemas operativos
soportados para su uso son prácticamente todos los disponibles.
Entre los motores disponibles es el que más salidas contempla,
por lo que no es una preocupación si el _appbook_ es para tabletas,
teléfonos, computadoras personales, exploradores _web_ o consolas.
También cuenta con soporte para realidad virtual (+++VR+++) y
realidad aumentada (+++AR+++).

Fue muy cómodo cuando lo utilicé para proyectos editoriales con
+++AR+++ o para _appbooks_ lúdicos 2D. Su uso multiplataforma
ayudó a que no tuviera problemas para migrar de macOS a de nuevo
Linux.

En ese momento tenía soporte para UnityScript, un lenguaje de
programación basado en JavaScript. De manera extensiva, me facilitó
su uso ya que mis conocimientos en lenguajes de la familia C
son escasos.

En la actualidad, solo soporta un lenguaje de programación: C#.
C# y C++ son lenguajes muy populares dentro del desarrollo de
videojuegos. Si estás interesado a entrar de lleno en la creación
de _appbooks_ lúdicos es recomendable aprender al menos las bases
de estos lenguajes.

Unity puede usarse de manera gratuita si no se exceden ingresos
de $100.000 dólares anuales. Cuando se alcanza esa suma es necesario
pagar una licencia mensual de $125 dólares. Además, existe la
posibilidad de pagar de $25 a $35 dólares mensuales como individuo
con acceso a diversos materiales didácticos.

Mi experiencia con Unity es buena. No podría dar una evaluación
de su estado actual. Como sea, si te son indiferentes los usos
de licencia, Unity, sin duda, es una buena opción.

### [Unreal Engine](https://www.unrealengine.com/)

- Licencia: propietario con acceso al código fuente
- Costo: gratuito - 5% de regalías
- Sistemas operativos: Windows,  macOS y Linux
- Salidas: +15, incluyendo _web_, móviles, +++PC+++ y videoconsolas
- Lenguajes de programación: C++ y Blueprint
- Soporte +++VR+++: sí
- Soporte +++AR+++: sí

Unreal Engine es un motor que, cuando estaba investigando qué
_software_ usar para ciertos proyectos, me pareció un entorno
más completo que Unity. Por lo mismo me hizo pensar que era más
complejo. Si a esto sumamos que mi dominio de C es mínimo, me
incliné a usar Unity por el desaparecido UnityScript.

No puedo tener una opinión por este motivo. Sin embargo, su uso
y oferta de salidas es muy similar a la de Unity. Si tienes conocimientos
de C o quieres entrarle de lleno al desarrollo de contenidos
interactivos, quizá es muy buena idea probar antes con Unreal
Engine.

Además ofrece un interesante lenguaje de [programación visual](https://es.wikipedia.org/wiki/Programación_visual)
---aunque parece que para el siguiente año [Unity ya soportará
algo similar](https://www.reddit.com/r/Unity3D/comments/9r8mxa/visual_scripting_is_coming_to_unity_20192_as_a)---.
Con esto se disminuye la curva de aprendizaje para personas que
no venimos del campo del desarrollo de _software_, ya que de
manera visual es posible dar vida a nuestros proyectos.

Unreal Engine también es _software_ propietario, con la particularidad
que sí permite acceso al código fuente. Su uso es gratuito hasta
que en cada trimestre registres ganancias de al menos $3000
dólares. A partir de ahí es necesario ceder el 5% de las regalías.

Pese a que ofrece el código, o su uso en principio es gratuito
como el de Unity, no hay que olvidar que existe una diferencia
entre [lo gratuito, lo abierto y lo libre](https://marianaeguaras.com/lo-gratuito-lo-abierto-y-lo-libre-o-la-gratuidad-el-acceso-abierto-y-la-cultura-libre).
De nueva cuenta, si los usos de licencias te son irrelevantes,
Unity o Unreal Engine son muy buenas opciones.

### [Godot Engine](https://godotengine.org/)

- Licencia: abierta - [+++MIT+++ License](https://en.wikipedia.org/wiki/MIT_License)
- Costo: gratuito
- Sistemas operativos: Windows,  macOS y Linux
- Salidas: Windows,  macOS, Linux, _web_, iOS y Android
- Lenguajes de programación: GDScript, VisualScript y C#
- Soporte +++VR+++: sí
- Soporte +++AR+++: sí

Godot Engine ofrece muchas opciones similares a las de Unity
o Unreal Engine. La primera diferencia es que se trata de un
motor desarrollado por una comunidad. No solo es gratuito, también
es abierto. Esto nos da más flexibilidad para los que nos azotamos
con las licencias de uso.

Sus salidas son mucho más reducidas, pero abarca lo más común
para el usuario, a excepción de las videoconsolas. Soporta computadoras
personales, juegos _web_ y dispositivos iOS o Android.

Originalmente, su desarrollo se inclinó a los juegos 2D, aunque
ya existe soporte 3D. En lo personal, ahora me encuentro jugando,
aprendiendo y experimentando con este motor no solo por ser abierto,
sino también por los lenguajes de programación que pueden usarse.

¿Tengo que volver a repetir que mis conocimientos de C son pocos?
Si a eso sumamos que en la edición ramificada usamos muchos _scripts_,
algunos de ellos escritos con [Python](https://www.python.org/),
[GDScript](http://docs.godotengine.org/en/3.0/getting_started/scripting/gdscript/gdscript_basics.html)
me pareció interesante. Este lenguaje de programación ha sido
desarrollado por la misma comunidad de Godot y está basado en
Python.

Para mi interés personal, pienso que es una ganancia. Mientras
aprendo a usar Godot también, al fin, empiezo a familiarizarme
con Python. Además, su documentación me parece muy completa y
amigable.

Si tu interés es desarrollar _appbooks_ lúdicos en ecosistemas
abiertos o libres; si vienes de otro campo distinto al desarrollo
de _software_ o si careces de conocimientos de C, entonces Godot
Engine es tu mejor opción.

### [Cocos2d](http://www.cocos2d.org/)

- Licencia: abierta - [+++MIT+++ License](https://en.wikipedia.org/wiki/MIT_License)
- Costo: gratuito
- Sistemas operativos: Windows,  macOS y Linux
- Salidas: Windows,  macOS, Linux, _web_, iOS, Android y Windows Phone
- Lenguajes de programación: C++, C#, Python, JavaScript, Lua, Objective-C, Swift
- Soporte +++VR+++: ?
- Soporte +++AR+++: no

Cocos2d no es un motor, sino una familia de _software_ que permite
desarrollar videojuegos para diversas plataformas. Sus salidas
también son escasas comparadas a las de Unity o Unreal Engine.
Pero, como Godot, las salidas que ofrece son las más comunes
para los usuarios que no juegan con videoconsolas.

Al tratarse de una familia, pueden usarse un gran número de lenguajes
de programación. Esto es una gran ventaja, pero también un potencial
problema. __Cocos2d fue lo primero que utilicé para proyectos
editoriales.__ Su pluralidad no hace sencillo decidir cuál usar,
cómo instalarlo o cómo configurar y gestionar el proyecto.

Fue mi primera prueba no tanto por su tipo de licencia, sino
porque en aquel tiempo me pareció que fue el _software_ que se
usó para Alice o Poe. Recuerdo haber extraído los archivos de
las _apps_ y en alguna de ellas ver la mención de Cocos2d. (No
lo recuerdo con exactitud y puede ser que esta información no
sea exacta).

Desarrollar con Cocos2d fue toda una aventura. Aprendí muchas
cosas, pero también sentí que el ritmo de desarrollo fue torpe
por cuestiones ajenas al proyecto. Colapsos, error de dependencias,
cosas raras en la compilación, etcétera.

No puedo opinar del estado actual de Cocos2d, pero sí puedo indicar
que la experiencia con Godot ha sido menos dolorosa.

Cocos2d no soporta +++AR+++ porque, en general, [no soporta proyectos
3D](https://discuss.cocos2d-x.org/t/ar-in-cocos-creator/40803/3).
Al parecer, la implementación de +++VR+++ es posible, aunque
ha sido más bien como experimentos de usuarios de su comunidad
y no como un elemento por defecto.

## ¡A probar!

Sin duda existen muchísimos más motores de videojuego. Algunos
son muy especializados, otros más de uso general. Unos más son
muy robustos y por ahí también hay los que son más experimentales.

Sea como sea, si quieres desarrollar _appbooks_ lúdicos o buscar
a un equipo de personas que te ayuden en tu proyecto, al menos
ya sabes por dónde empezar.

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «¿Qué son y cómo hacer _appbooks_ lúdicos?».
* Título en el _blog_ de Mariana: «¿Qué es y cómo hacer _appbooks_ lúdicos?».
* Fecha de publicación: 11 de diciembre del 2018.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/25%20-%20%C2%BFQu%C3%A9%20son%20y%20c%C3%B3mo%20hacer%20_appbooks_%20l%C3%BAdicos%3F/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/que-es-y-como-hacer-appbooks-ludicos/).

</aside>
