# 1. Edición digital como edición «desde cero»

Por iniciativa de Mariana Eguaras estaremos publicando una serie de
entradas dedicadas a la **publicación digital**, sus **características,
ventajas y retos**, así como **su relación con los medios impresos** y
cómo estas cuestiones afectan directamente a los procesos necesarios
para producir cualquier publicación.

Si bien ya tenemos planeadas el contenido de las primeras entradas,
cualquier sugerencia sobre temas a tratar será bienvenida. En la medida
de lo posible, la redacción no será técnica, sino afín al público en
general o a quienes se dedican a la edición.

Sin embargo, también se tiene que tomar en cuenta que algunos
tecnicismos ya son imprescindibles para el medio editorial. Así como la
jerga de los tipógrafos, impresores o diseñadores es de habitual
conocimiento para quienes editamos, del mismo modo el lenguaje del
desarrollador _web_ o de _software_ empieza a formar parte de nuestro
bagaje cultural.

En esta primera entrada haremos una **comparación general entre algunos
de los métodos más comunes para producir un libro electrónico estándar
en formato +++EPUB+++**. En su momento se hablará con más detalle sobre
la historia del +++EPUB+++.

Lo que ahora nos interesa es que, entre los distintos tipos de formatos
existentes en el mercado del libro electrónico, el +++EPUB+++, desde sus
inicios, fue pensado como un tipo de archivo para _ebooks_ que sobresale
por su **versatilidad, ligereza y seguimiento de estándares _web_** que
aseguran una uniformidad en el código, y también un **completo control
sobre la edición**.

Al tener estas características, el formato +++EPUB+++ es fácilmente
convertible a formatos privativos como los empleados por Amazon o Apple,
asegurando así un ahorro de recursos y de tiempo al momento de crear una
publicación digital.

Esta flexibilidad también ha permitido el desarrollo de diversos
programas de cómputo que facilitan la creación de +++EPUB+++ a cualquier
persona. Con un par de clics desde el procesador de textos (Writer o
Word) o maquetador (InDesign) instantáneamente tenemos un archivo
+++EPUB+++ a través de una conversión de formatos.

En principio, esto es una gran ventaja para autores independientes o
para editoriales que no desean invertir «esfuerzos adicionales» en la
producción de un libro electrónico. Sin embargo, existen al menos **dos
desventajas** en estos procesos de publicación:

1. La limpieza del código, el control del diseño y la calidad de la
   edición en muchas ocasiones son inferiores a lo que es posible obtener a
   través de otros procesos.
2. Se pierde por completo de vista que lo más importante de la
   revolución digital dentro del mundo de la edición no es el libro
   electrónico, sino los procesos que ahora son posibles y que no
   representan algo «extra» al momento de producir un libro, sea impreso o
   digital. En su lugar, se trata de un conjunto orgánico de elementos que
   pueden ayudar tanto en el ámbito técnico de la publicación como en la
   calidad de la edición.

El libro electrónico es el punto de relieve más característico de la era
digital en la edición, pero es solo la punta del _iceberg_. Para poder
ir paulatinamente más a fondo tenemos que empezar a familiarizarnos con
**lo que está detrás de la producción de un _ebook_**.

Como primera cuestión, existe cierto escepticismo sobre la necesidad de
producir una publicación «desde cero». Es decir, se tiende a preferir el
uso de conversores que, sin importar la estructura o el contenido de la
publicación, automáticamente crean un archivo +++EPUB+++.

¿Para qué aprender lenguajes de etiquetado, como
[+++HTML+++](https://es.wikipedia.org/wiki/HTML) o
[Markdown](https://es.wikipedia.org/wiki/Markdown)? ¿Para qué
preocuparnos por las hojas de estilo, como
[+++CSS+++](https://es.wikipedia.org/wiki/Hoja_de_estilos_en_cascada) o
[+++SCSS+++](https://es.wikipedia.org/wiki/Sass_%28lenguaje_de_hojas_de_estilo%29)?
¿Para qué reflexionar sobre el potencial de los lenguajes de
programación para generar otras experiencias de lectura o para mejorar
el cuidado de la edición, como
[JavaScript](https://es.wikipedia.org/wiki/JavaScript),
[Python](https://es.wikipedia.org/wiki/Python),
[Ruby](https://es.wikipedia.org/wiki/Ruby) o
[C++](https://es.wikipedia.org/wiki/C%2B%2B)?

Porque aunque el objetivo sea un objeto físico o digital, si empezamos a
enfocarnos en su proceso de producción, poco a poco nos daremos cuenta
de su pertinencia.

## Características del ejercicio de comparación

Para mostrar las ventajas y desventajas que presentan los conversores de
+++EPUB+++, en contraste con el desarrollo de una publicación digital
«desde cero», se hará una comparación en la producción de una misma obra, pero
con diferentes métodos.

Con la intención de que este ejercicio sea lo más «real» posible, se optó
por tomar [la edición del Proyecto Gutenberg del _Don
Quijote_](http://www.gutenberg.org/ebooks/2000). Además, para mantener
una uniformidad en la edición se ha partido del mismo texto en formato
+++HTML+++ y se ha usado la misma hoja de estilos +++CSS+++.

Algunas dudas respecto a estas decisiones pueden ser:

* **¿Por qué utilizamos la edición del Proyecto Gutenberg si hay
  mejores ediciones disponibles en línea como puede ser la publicada
  por el [Instituto
  Cervantes](http://cvc.cervantes.es/literatura/clasicos/quijote/default.htm)?**
  La edición usada en este ejercicio es de dominio público, así como,
  a diferencia de la edición de
  [Wikisource](https://es.wikisource.org/wiki/El_ingenioso_hidalgo_Don_Quijote_de_la_Mancha),
  fue sencillo descargar el libro entero en un solo archivo.
* **¿Por qué se parte de un texto previamente formateado, en lugar de
  vaciar el texto directamente desde los archivos descargados del
  Proyecto Gutenberg?** Al momento de hacer esta comparación se
  detectaron algunos descuidos en la edición que se decidieron
  enmendar (aunque sin duda se podrán encontrar más, ya que un cuidado
  estricto no es la meta de este ejercicio). Por otro lado, el formato
  del texto casi siempre se presenta como pesadilla para quienes
  editamos, provocando una mayor inversión de tiempo, recursos y
  esfuerzo únicamente para limpiarlo, cuestión que trataremos en otra
  entrada.
* **¿Por qué se emplea la misma hoja de estilo en lugar de recrear el
  diseño en cada uno de los métodos empleados?** El rediseño también
  involucra una mayor inversión de tiempo, recursos y esfuerzo, además
  de que el empleo de la misma hoja de estilo deja patente la
  pertinencia del diseño _web_ para el mundo de la edición y la
  flexibilidad de poder utilizar esta hoja en los diversos métodos
  empleados (y obras), tema del que hablaremos en una futura entrada.
* **¿Cuáles son los métodos empleados en este ejercicio?** El libro se
  desarrolló de cuatro maneras distintas: desde
  [InDesign](https://www.adobe.com/products/indesign.html), siendo la
  forma más común para la mayoría de los editores o diseñadores
  editoriales; con [Jutoh](http://jutoh.com/), como ejemplo de
  _software_ pensado para facilitar la creación de publicaciones
  digitales; mediante
  [Sigil](https://github.com/Sigil-Ebook/Sigil/releases/tag/0.9.6),
  por ser uno de los programas más utilizados para la creación de
  +++EPUB+++, y «desde cero», lo cual involucra una intervención
  directa en los archivos necesarios para el desarrollo de un
  +++EPUB+++.

## Comparativa de tiempos de producción: la eficacia del método «desde cero»

Uno de los mayores mitos que existe sobre la pertinencia de la creación
de libros +++EPUB+++ «desde cero» es que este modo de trabajar requiere
mayores tiempos de producción. Sin embargo, el desarrollo «desde cero»
no quiere decir que todo ha de hacerse de manera manual. Como lo
estaremos viendo en otras entradas, mucho del trabajo al momento de
desarrollar un +++EPUB+++ es monótono y fácilmente automatizable a
través de la creación de [_scripts_](https://es.wikipedia.org/wiki/Script).

Cuando hablamos de la creación de un libro +++EPUB+++ «desde cero» nos
referimos a que no existe un _software_ intermediario que nos ayude a
llevar a cabo esta tarea más allá de un [editor de texto
plano](https://es.wikipedia.org/wiki/Editor_de_texto), o [de
código](https://es.wikipedia.org/wiki/Editor_de_c%C3%B3digo_fuente), y
el uso de [la línea de
comandos](https://es.wikipedia.org/wiki/Interfaz_de_l%C3%ADnea_de_comandos).

En un primer momento esto puede sonar como un proceso demasiado complejo
y que requiere mucha inversión de tiempo para aprender a llevarlo a
cabo. Pero nada está más lejos de la realidad, si bien el desarrollo
«desde cero» tiene sus retos, como cualquier otro método, es una
habilidad que está al alcance de cualquiera que cuente con una
computadora.

Si obviamos el tiempo empleado en darle formato al texto, en la
siguiente gráfica podemos observar que en el desarrollo de la misma obra
y diseño **el método «desde cero» es el más eficaz**. Esta diferencia se
debe principalmente al tiempo que debe de emplearse en asociar las
etiquetas +++HTML+++ a los estilos creados con anterioridad.

![Comparativa de tiempos de producción: la eficacia del método «desde cero».](../img/img_01-01.jpg)

Tanto en InDesign como en Jutoh es necesario asociar cada estilo
+++CSS+++ a estilos de párrafo o de caracteres; la diferencia entre
ambos estriba en que en InDesign la asociación es más intuitiva y sencilla que
en Jutoh. Por el otro lado, con Sigil o «desde cero» no existe la necesidad de
asociación, ya que esta se detecta automáticamente al «enlazar» la hoja
de estilos +++CSS+++ a cada uno de los archivos del contenido del libro;
sin embargo, el método «desde cero» presenta la ventaja de que no existe la
necesidad de recrear el árbol de directorios o de importar archivos a un
programa, como Sigil, para poder crear el archivo +++EPUB+++.

## Tamaño del +++EPUB+++: la incidencia de las imágenes y el código «basura»

En cuanto al tamaño del archivo +++EPUB+++, existen al menos dos
factores que afectan directamente en su peso, que son **1)** las imágenes
incrustadas, y **2)** la cantidad de código «basura».

La mayoría de los libros +++EPUB+++ solo presentan una o dos imágenes
para la portada o la contraportada, quizá una más si se incluye una fotografía
del autor. Aunque se trate de un par de elementos, por lo general estas
imágenes tienden a representar los **archivos con mayor peso en un
+++EPUB+++** si existen alguna mezcla de estas circunstancias:

1. la publicación es relativamente breve,
2. las imágenes son más grandes de lo necesario,
3. las imágenes no tienen una buena compresión, o
4. las imágenes están en un formato poco pertinente.

Ninguna de estas posibilidades son las que afectan al peso de estos
archivos +++EPUB+++ debido a que todas están utilizando la misma imagen
de 204 +++KB+++.

**La diferencia de tamaños tiene una relación directa con la cantidad de
código «basura»**. Cuando hablamos de la existencia de código «basura»
queremos decir que, por los mismos resultados, algunos conversores
añaden líneas adicionales de código. Sea para dejar patente el
_software_ que se utilizó para crear el archivo, o porque el método que
asocia los estilos +++CSS+++ con los estilos de párrafo o de caracteres
es la recreación del archivo +++CSS+++ bajo sus propios lineamientos y
convención de nombres.

Esto produce al menos **dos desventajas**:

1. peso adicional e innecesario a la publicación, y
2. un cambio de nomenclatura que puede dificultar una ulterior
   modificación del diseño.

En el caso de los métodos con InDesign o Jutoh, el peso del libro se
incrementó por el código «basura» existente; no obstante, la diferencia
de peso entre los +++EPUB+++ hechos con Sigil o «desde cero» es de otra
índole que involucra la estructura interna del libro.

Sin extendernos demasiado, a partir de la tercera versión del formato
+++EPUB+++ existen dos archivos para la tabla de contenidos. El formato
antiguo es el +++NCX+++, mientras que el formato introducido en la
tercera revisión del +++EPUB+++ es el [+++XHTML+++](https://es.wikipedia.org/wiki/XHTML)
o +++HTML+++.

Por defecto, Sigil solo te genera una tabla de contenido en formato
+++NCX+++. Esto puede ser un potencial problema si consideramos que
algunos de los lectores de libros más recientes solo soportan la visualización
de tablas de contenidos en formatos +++XHTML+++ o +++HTML+++.

Debido a esta circunstancia, **el +++EPUB+++ creado «desde cero»
contiene** tanto **una tabla de contenidos en formato
+++NCX+++** (para lectores de libros antiguos) **y otra en
+++XHTML+++**, lo cual representa 11 +++KB+++ de
peso adicional para una diferencia de solo 5 +++KB+++ entre el libro
desarrollado con Sigil y el creado con este método.

![Tamaño del +++EPUB+++: la incidencia de las imágenes y el código «basura».](../img/img_01-02.jpg)

## Errores y advertencias: verificación de los +++EPUB+++

Una de las principales ventajas que acarrea el proceso de producir un
libro +++EPUB+++ a través de conversores o _software_ intermediario es
que precisamente está pensado para usuarios con poco interés o conocimiento
de +++HTML+++ y +++CSS+++. La creación del libro, por lo general
es de manera visual e incluso en algunos casos la curva de aprendizaje es
relativamente corta.

Sin embargo, **además del cuidado editorial y del aspecto estético, un
libro electrónico también tiene que tener una estructura interna
coherente**. Es decir, si la finalidad es desarrollar un +++EPUB+++ con
calidad «profesional», este no debe de contener errores o advertencias
frutos de inconsistencias en el lenguaje de etiquetado o de la hoja de
estilos, en los metadatos, en los tipos de formatos aceptados o en la
compresión de las imágenes.

Para que la búsqueda de inconsistencias sea efectiva y no nos demore
mucho tiempo, existen los **verificadores de +++EPUB+++**. La
herramienta oficial para verificar estos archivos se llama Epubcheck, la cual
tiene una versión [en línea](http://validator.idpf.org/) como otra disponible
para [su descarga](https://github.com/IDPF/epubcheck/releases).

Sin embargo, por lo general un +++EPUB+++ no solo se verifica mediante
Epubcheck, sino con al menos otro _software_, sea por requerimiento del
cliente, sea simplemente para tener otra herramienta de verificación. En
el caso de nuestro ejercicio hemos utilizado tanto Epubcheck como una
extensión para Firefox denominada
[BlueGriffon](http://www.bluegriffon-epubedition.com/BGEV.html)
(verificador que varios de nuestros clientes piden utilizar).

Ahora bien, la gráfica que se muestra solo abarca los errores y
advertencias detectados por BlueGriffon ya que ninguno de los +++EPUB+++
presentó inconsistencias al ser verificados con Epubcheck. También cabe
resaltar que la cantidad de errores o advertencias es poca debido a que
se emplearon los mismos textos en formato +++XHTML+++ y hojas de
estilos, así como los metadatos fueron generados automáticamente por cada uno
de los métodos. (La creación «desde cero» utilizó [un _script_](https://github.com/NikaZhenya/Pecas) 
desarrollado inicialmente por el **colectivo [Perro Triste](https://github.com/ColectivoPerroTriste)**
y que se ejecuta a través de la línea de comandos).

El error presente en el +++EPUB+++ hecho con InDesign indica que este
programa utilizó un tipo de compresión no válida para la imagen de la portada,
mientras que la advertencia es exactamente la misma a las que detecta en
los +++EPUB+++ creados con Jutoh y Sigil: un elemento presente en los
[metadatos](https://marianaeguaras.com/que-son-los-metadatos-de-un-libro-y-cual-es-su-importancia/)
se considera obsoleto.

En realidad, **resolver estos errores y advertencias no es nada
complicado;** no obstante, para un usuario que no está familiarizado con
la estructura del +++EPUB+++ el proceso para enmendarlos puede ser
frustrante. Para arreglar estos +++EPUB+++ es necesario su descompresión
de manera externa para ir directamente al archivo que presenta el problema,
modificarlo y, por último, volver a comprimir el +++EPUB+++.

![Errores y advertencias: verificación de los +++EPUB+++.](../img/img_01-03.jpg)

## Costos implícitos en la producción: programas de pago versus _software_ libre

Al momento de producir un +++EPUB+++, por lo general el usuario se
pregunta si los programas de cómputo necesarios son gratuitos o si tienen algún
costo. Afortunadamente, para la producción de un libro electrónico no es
necesario pagar por alguna licencia de _software_.

En la mitad de los cuatro métodos vistos en este ejercicio se empleó
_software_ privativo que implica costos para su uso. Tanto InDesign como
Jutoh requieren el pago de una licencia, mientras que Sigil es
_software_ libre. Para el método «desde cero» se emplearon programas de
_software_ libre ([Atom](https://atom.io/), Firefox y el _script_
desarrollado por Perro Triste) y del sistema ([la
terminal](https://es.wikipedia.org/wiki/Emulador_de_terminal)).

Un mito recurrente entre los usuarios que no están familiarizados con el
uso de _software_ libre o de código abierto es que su gratuidad es
sinónimo de baja calidad. La falsedad de este supuesto ---al menos en lo
que concierne a la producción de libros +++EPUB+++--- pudo observarse en
este ejercicio: **Sigil y el método «desde cero» arrojaron mejores
resultados**.

No obstante, tampoco puede dejarse de lado que la gran mayoría de las
editoriales emplean InDesign para la edición de sus libros impresos, por
lo que en determinadas circunstancias lo más conveniente quizá sea irse
por esta misma vía para desarrollar libros electrónicos.

Si esta no es tu situación y, al mismo tiempo, deseas mejorar la calidad
de tus +++EPUB+++, por lo que estás considerando hacer una inversión en
_software_ privativo, mejor piénsalo dos veces. Existen otras
posibilidades libres y de alta calidad que pueden acomodarse a tus
necesidades.

## Conclusión: el método «desde cero» gana esta partida

A lo largo de este ejercicio quedó evidenciado que uno de los métodos
más eficaces y eficientes para la producción de libros electrónicos
estándar es el **desarrollo «desde cero»**. Varios lectores podrán
pensar que este modo de producción implica ciertos conocimientos
técnicos complejos y una mediana o larga curva de aprendizaje.

Como experiencia, puedo compartirles que a las personas que, por medio
de Nieve de Chamoy o Perro Triste, les hemos ayudado a aprender a hacer
+++EPUB+++, no les ha tomado más de 24 horas de taller el desarrollo
de su primer libro electrónico «desde cero». Además, cabe la pena resaltar que
la mayoría de estas personas provienen de un contexto de completo
desconocimiento de los lenguajes +++HTML+++ y +++CSS+++, así
como del uso de la línea de comandos.

Sin embargo, como tampoco se trata de reducir las posibilidades de
producción a este único método, **en futuras entradas veremos cómo
elaborar libros +++EPUB+++ desde InDesign, Sigil y el desarrollo «desde
cero»**. No trataremos el método desde otros programas (como Jutoh o
iBooks Author) porque no lo considero conveniente.

Si se tiene planeado el uso de _software_ exclusivamente para crear
libros electrónicos, la recomendación es que sea _software_ libre o de
código abierto, para que no involucre costos de producción extra, y
cuyos formatos de salida sean estandarizados, como el +++EPUB+++ (el
cual sin problemas puede convertirse a formatos privativos como los usados por
Amazon y Apple); por ello, **la recomendación bajo estas necesidades es
Sigil**.

Por otro lado, para no obviar la tendencia editorial de **producir
libros con InDesign**, también se verá **la manera más óptima de
desarrollar libros +++EPUB+++ con este programa**. Pero antes de esto,
el siguiente tema a tratar será sobre un problema rutinario en el quehacer
editorial y que, aunque parezca mínimo, representa uno de los cuellos de
botella al momento de producir libros, sean impresos o digitales; es
decir, hablaremos sobre **la manera más adecuada de dar formato a un
texto**.

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «Edición digital como edición “desde cero”».
* Título en el _blog_ de Mariana: «Edición digital como edición “desde cero”».
* Fecha de publicación: 6 de septiembre del 2016.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/01%20-%20Edici%C3%B3n%20digital%20como%20edici%C3%B3n%20desde%20cero/entrada.odt).
* [Revisión de Mariana](https://marianaeguaras.com/edicion-digital-como-edicion-desde-cero/).

</aside>
