0 - Desde cero
Versión: 3.0.0
Peso: 1,182 KB
Tiempo de desarrollo: 4:17
Epub Check
  Errores: 0
  Advertencias: 0
Blue Griffon
  Errores: 0
  Advertencias: 0
Notas: posibilidad de versión 3.0.1; empleo de puro software libre y del sistema.

1 - InDesign CC
Versión: 3.0.0
Peso: 1,185 KB
Tiempo de desarrollo: 4:29
Epub Check
  Errores: 0
  Advertencias: 0
Blue Griffon
  Errores: 1
  Advertencias: 1
Notas: utilización de etiquetas XML y estilos CSS para un mayor control; empleo de software privativo.

2 - Sigil
Versión: 3.0.1
Peso: 1,177 KB
Tiempo de desarrollo: 4:23
Epub Check
  Errores: 0
  Advertencias: 0
Blue Griffon
  Errores: 0
  Advertencias: 1
Notas: sin posibilidad de versión 3.0.0; empleo de puro software libre.

3 - Jutoh
Versión: 3.0.1
Peso: 1,191 KB
Tiempo de desarrollo: 4:46
Epub Check
  Errores: 0
  Advertencias: 0
Blue Griffon
  Errores: 0
  Advertencias: 1
Notas: sin posibilidad de versión 3.0.0; empleo de software privativo.