# Edición cíclica y edición ramificada

## Edición ramificada

### Surgimiento de una idea

¿Y si cambiamos de perspectiva? ¿Si en lugar de hablar sobre la diversidad
en los soportes finales, pensamos en un método multilateral? La idea es
sencilla: **no nos concentremos en los soportes, sino en los caminos que
llevan a ellos**. El supuesto también es evidente: a múltiples formatos,
diversos senderos.

Pese a que la idea es sencilla, en el grueso editorial se interpretará
como un discurso que apuesta por la pretendida destrucción de su tradición, 
cuando en realidad atenta a un ideal metodológico, no a los conocimientos 
que a lo largo de siglos se han ido acumulando para publicar «buenos» libros. 
Una prueba de esta extrapolación es patente cuando se analiza la tradición 
editorial: las técnicas han variado a través de los siglos, pero la idea de 
ir a un punto A a uno B ha permanecido.

En la edición tradicional el punto A era igual al texto original y el
punto B, el impreso. En la edición cíclica A permanece, mientras que B 
es el fin de todos los ciclos, obteniéndose así no solo el impreso, sino 
también al menos un formato digital.

Sin embargo, en la edición ramificada no hay una consecución de A a B, sino 
un tronco (A) con diversas ramas (B, C, D…) que cesan su crecimiento, otras 
se desprenden y unas más se convierten en soporte para más ramas.

Esta concepción metodológica surgió de un campo especializado: la elaboración 
de documentación de *software*. En el surgimiento de la era digital pronto 
se vio la necesidad de textos que explicaran el uso del *software*. Sin 
embargo, debido a las distintas preferencias de consulta y de *hardware*, 
se hizo menester crear la documentación en distintos formatos.

Pronto se percibió la dificultad presente en la edición cíclica: cada nuevo
soporte implica un aumento en la inversión de tiempo y de cuidados, y por
ende, de recursos. Para combatir este problema y ahorrar en material 
impreso, a partir de los noventa surge la idea del [*single source publishing*](https://en.wikipedia.org/wiki/Single_source_publishing)
(SSP), cuya característica es el desarrollo multilateral de diversos formatos.

Aunque la idea tiene poco más de veinte años, es ahora que empieza a 
expandirse para el resto del quehacer editorial. En la actualidad la 
edición ya no solo es digital, sino también la publicación: **los libros 
electrónicos han puesto en el centro de la discusión el problema metodológico 
que el sector editorial viene arrastrando desde el inicio de la «revolución» 
digital**.

### El tronco y las ramas

Pero en concreto, ¿qué es la edición ramificada? Lo primero que podría
entenderse es una apertura en todo sentido. No obstante, el término
«*single source*» ayuda a esclarecer el [**primer elemento**]{#uno} de esta
metodología de trabajo: la edición empieza con un «archivo madre».

El «archivo madre» es un documento a partir del cual se crean el resto 
de los formatos: es el tronco. Este archivo puede ser tanto el texto 
original del autor como el documento editado, ya que el elemento mínimo
necesario es que esté en un lenguaje de marcado.

El archivo madre obedece a una dimensión estructural a partir de un 
conjunto de etiquetas que indican cada elemento del texto (véase [aquí](http://marianaeguaras.com/el-formato-de-una-publicacion-cuello-de-botella-en-la-edicion/) 
para más información). La edición del texto puede darse antes o 
después de esta estructuración.

Lo recomendado es que la edición y la estructuración se den en 
conjunto, porque antes de la publicación, quien edita es uno de los
principales conocedores de la obra. La desventaja es que se requiere 
saber al menos un lenguaje de marcado, pero esto se solventa al recurrir 
a un lenguaje de marcas ligero.

Los lenguajes de marcas ligeros son cómodos de leer, fáciles de escribir 
y sencillos de convertir. Por este motivo, es preferible un lenguaje
«ligero», como Markdown, que uno «completo», como XML, HTML o TeX.
(Véase la [entrada original](https://raw.githubusercontent.com/NikaZhenya/entradas-eguaras/master/08%20-%20Edici%C3%B3n%20c%C3%ADclica%20y%20edici%C3%B3n%20ramificada/entrada.md)
de este artículo como ejemplo).

El [**segundo elemento**]{#dos} de este método es ir de lo simple a lo complejo.
Cada soporte presenta sus propias particularidades. En el impreso se 
requieren ajustes manuales por cuestiones ortotipográficas o de diseño; 
en el EPUB a veces es preciso ordenar gráficas o tablas para una mejor 
visualización; en la lectura *online* sin inconvenientes se saca provecho
de la [visualización interactiva](https://d3js.org/) de información, etcétera.
Para evitar la herencia de características, como sucede en la metodología 
cíclica, la edición ramificada parte de un documento con los elementos 
comunes, para después hacer ajustes según el caso.

El traslado del archivo madre a cada uno de los soportes finales exige,
en la mayoría de los casos, el uso de un nuevo lenguaje. Si se parte de
Markdown, se quiere XHTML para un EPUB, XML para InDesign o TeX para 
LaTeX o ConTeXt. Si bien se recomienda el conocimiento de estos lenguajes, 
el [**tercer elemento**]{#tres} es *el uso de conversores para el ahorro de tiempo*.

Al trabajar con un lenguaje de marcado, es posible automatizar la traducción
a otros lenguajes. Sin embargo, como es de esperarse en la traducción por 
una máquina, por lo general es necesario realizar modificaciones y cotejos. 
*Los conversores no son una solución final* pero evitan el trabajo monótono.

El mejor *software* que puede ayudarnos en esta tarea es [Pandoc](http://pandoc.org/). 
Este programa es de los más poderosos y libre que podemos encontrar. Y si
bien en algunos casos el resultado no es el esperado, el tiempo involucrado
en los ajustes es menor a volver a formatear el documento.

Esta inversión de tiempo permite diferenciar la edición ramificada de muchos 
*software* milagro que se ofrecen en el mercado. Esta clase de programas se 
autoperciben como la «solución» de varios problemas que el editor encuentra 
al momento de publicar en múltiples soportes.

La edición ramificada no es un *software* ni un lenguaje, mucho menos una 
solución y tampoco un entorno de trabajo (guiño a Adobe). La edición 
ramificada es una metodología que puede manifestarse de múltiples maneras, 
unas más acabadas que otras. Al ser un método también cuenta con la 
posibilidad de pulirse o de descartarse si crea inconvenientes.

Por ello, el [**cuarto elemento**]{#cuatro} es que la edición ramificada
solo está pensada cuando existe la posibilidad de múltiples soportes. 
Si la obra se proyecta como un «libro objeto» o una publicación artesanal 
basada en métodos análogos, este método no tiene cabida.

Los conversores no son lo único que permiten el ahorro de tiempo. 
El [**quinto elemento**]{#cinco} consiste en que el tamaño del equipo de trabajo es 
proporcional a la agilización y división del trabajo. Una diferencia nítida 
entre la edición cíclica y la ramificada es que en la última ningún soporte 
final parte de otro, permitiéndose el trabajo paralelo en la producción de 
cada soporte.

Cada soporte emprende su camino de manera simultánea a partir del archivo
madre, resaltando el [**sexto elemento**]{#seis} metodológico: la edición 
ramificada es edición sincrónica, independiente y descentralizada.

Uno de los temores que surgen en la edición ramificada es que
la simultaneidad y autosuficiencia puede dar cabida a una divergencia en 
el contenido, ya que no existe un mecanismo centralizado para el control 
de la edición. No obstante, en muchos casos la edición cíclica implica 
una gran pérdida de control, debido a que el encargado del cuidado editorial 
tiende a desconcer los procesos «periféricos» y «adicionales» a la 
producción del soporte impreso.

Si hablamos de «control», en el contexto digital **el cuidado de una obra
no solo debe de ser editorial, sino también técnico**. Si no hay dominio
sobre el código, no existe la seguridad de que el texto, aún con lenguaje
de marcado, sea fácil de convertir o sencillo de leer y analizar.

Este inconveniente disminuye drásticamente si con previsión el archivo 
es editado y estructurado. Así se evita tener que manipular el contenido 
manualmente. Otra posibilidad es tumbar la rama y generar de nuevo el 
contenido. Si bien es la solución más cómoda, en algunos casos la consecuencia 
es volver a pulir detalles derivados de la conversión. Por ello, una solución 
más sería la creación de *software* que monitoree cada uno de los soportes 
para detectar algún desvío en el contenido sin importar el lenguaje de 
marcado empleado.

No obstante, en más de una ocasión existen correcciones de último momento
que también han de añadirse a cada uno de los soportes. La corrección manual 
involucra más cotejos e incluso abre la puerta para más erratas. El 
[**séptimo elemento**]{#siete} llama al [uso de diccionarios](http://marianaeguaras.com/como-usar-el-diccionario-del-usuario-en-indesign-para-evitar-errores-de-maquetacion/)
y de expresiones regulares ([*regex*](https://es.wikipedia.org/wiki/Expresi%C3%B3n_regular)) 
para evitar o automatizar las modificaciones. El diccionario permite detectar 
erratas en el archivo madre. El uso de *regex* facilita corregir los archivos 
sin la necesidad de ir caso por caso.

El uso del diccionario es recomendable únicamente para el cotejo de posibles
erratas, sea caso por caso o mediante la creación de una lista ordenada
con las palabras dudosas. En cuanto a *regex*, hay que tener cautela con
su uso. El dominio de las expresiones regulares se adquiere con el tiempo, 
por lo que las primeras implementaciones tienen que ser básicas y después 
de hacer sido aplicadas en pruebas.

Continuando con la idea de un *software* que monitoree las ramas, este
también tiene que permitir la corrección automatizada. La lógica sería: 
1) realizar una corrección manual en el archivo madre que 2) de manera
automática el programa implemente los mismos cambios en cada soporte.
