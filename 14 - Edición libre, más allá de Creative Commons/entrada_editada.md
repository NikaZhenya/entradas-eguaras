# 14. Edición libre, más allá de Creative Commons

> El siguiente artículo supone un conocimiento de lo que es Creative
> Commons y el _copyleft_. Si no se cuentan con los elementos, se
> recomienda revisar [el sitio de Creative Commons](https://creativecommons.org/), 
> [los tipos de licencias Creative Commons](https://creativecommons.org/licenses/) y 
> [la entrada en Wikipedia sobre _copyleft_](https://es.wikipedia.org/wiki/Copyleft).

Hace algunos años, cuando empezaba a revisar escritos de Lawrence Lessig
---impulsor de las licencias Creative Commons (+++CC+++)---, encontré una muy
buena traducción de _Free Culture_. Esta edición en español tiene una
licencia +++CC+++ y está disponible su versión digital sin costo alguno. Pero
es un +++PDF+++ y yo quería un +++EPUB+++. Revisando el sitio de esta editorial
encontré casos similares con otros títulos a su disposición con +++CC+++ y en
+++PDF+++.

Me surgió una idea muy sencilla: ¿por qué no les pedía los archivos
originales *únicamente* para hacer una versión +++EPUB+++? Continuaría
respetando los términos de la licencia +++CC BY-NC+++ (usar la obra dando
atribución y sin fines comerciales).

Además, si a ellos no les agradaba la idea de que el +++EPUB+++ estuviera
disponible en otros sitios ---como en Internet Archive o LibGen---
también estaba dispuesto a entregar ese material para que solo en su
sitio estuviera disponible, sin necesidad de atribución por su
desarrollo.

Con entusiasmo les escribí. ¿Quién negaría trabajo voluntario para tener
sus libros en otros formatos? Pasaron algunos días y la respuesta fue
clara: no estaban interesados en este momento y, si lo quería, podía
hacerlo por mi cuenta a partir del +++PDF+++: *no podían entregarme los
archivos originales*.

**Hacer un +++EPUB+++ a partir de un +++PDF+++ no es una tarea difícil, pero es un
trabajo innecesario. **Cuando partes de un +++PDF+++ existe la necesidad de
volver a darle formato a todo el libro. No es complicado localizar
encabezados o bloques de cita.

Sin embargo, la tarea se va incrementando conforme se tiene que dividir
el cuerpo del texto de las notas, eliminar guiones de separación,
encontrar resaltes tipográficos ---itálicas, versalitas o negritas---,
sanar el cuerpo bibliográfico y verificar párrafo por párrafo que el
contenido esté en su orden correcto, ya que cuando se extrae el texto de
un +++PDF+++ en ciertas ocasiones algunos párrafos aparecen en otro lado de la
obra.

Todo esto puede evitarse si en lugar de partir del +++PDF+++ se usan los
archivos que se usaron para maquetar el documento; por lo general, los
archivos de InDesign o de algún procesador de texto, e imágenes en +++PS+++,
+++JPEG+++, +++TIF+++ o +++PNG+++.

Aún más tiempo implica si el texto del +++PDF+++ es imagen con +++OCR+++, ya que, por lo
general, al menos un 5 % de las palabras fueron mal reconocidas, o si
hay imágenes intercaladas en el texto, que también se tienen que extraer
y enmendar.

Cuando el autor o el editor no desea entregar estos archivos, implica al
menos la necesidad de realizar una lectura de la obra. Si hablamos del
caso de la prestación de un servicio, esto implica que el autor o el
editor terminarán por pagar más por el desarrollo del +++EPUB+++ debido a que
existe la necesidad de **trabajo adicional redundante** tanto en el
aspecto técnico como de cuidado editorial.

La situación con esta traducción de _Free Culture_ no es aislada. Con un
enfoque exclusivo a obras con _copyleft_ o de dominio público, la falta
de disponibilidad de los archivos originales, e incluso la indisposición
de liberarlos por el temor a la piratería ---cuando en realidad las
licencias que usan suponen ciertas reglas de uso, sin importar que sea
un +++PDF+++ o sus archivos de maquetación---, es el pan de cada día.

En este sentido podemos decir que la mayoría de los casos **estos tipos
de licencias permiten un uso *abierto* mas no *libre* de las obras**, 
pese a que se inscriben en el movimiento de «_free
culture_» y no de «_open access_».

## **Lo gratuito, lo abierto y lo libre**

Los movimientos de cultura libre y de acceso abierto a la información
tienen un antecedente en común que es ajeno a la tradición editorial: el
surgimiento del _software_ libre y su principal bifurcación, el
movimiento del código abierto.

Aquí «tradición editorial» no solo implica una serie de técnicas y de
conocimientos editoriales que surgen de tecnologías análogas desde el
surgimiento de la imprenta. También comprende las doctrinas jurídicas
que desde el siglo +++XVII+++ fueron constituyéndose en torno al quehacer
editorial y que actualmente se conocen como los [derechos de
autor](https://marianaeguaras.com/derechos-de-autor-diferencia-entre-morales-y-economicos/).

En 1983, ante la masificación de las computadoras personales y la
intromisión de grandes corporaciones en el desarrollo de _software_
dentro y fuera de las universidades estadounidenses, un grupo de
programadores ---donde el más conocido es [Richard
Stallman](https://es.wikipedia.org/wiki/Richard_Stallman)--- deciden
hacerle frente a la situación mediante el movimiento del _software_
libre, el precursor de la Free Software Foundation (+++FSF+++), el proyecto
+++GNU+++ y la licencia +++GPL+++. En general, la «libertad» se entiende en cuatro
sentidos:

1. libertad de usar el programa (libertad 0).
2. libertad de estudiar y modificar el programa (libertad 1).
3. libertad de distribuir copias del programa (libertad 2).
4. libertad de mejorar el programa (libertad 3).

Para mayor información se recomienda revisar el apartado [«Filosofía del
Proyecto +++GNU+++»](https://www.gnu.org/philosophy/philosophy.html) del sitio
de la +++FSF+++.

Estas libertades implican no solo tener el programa ejecutable, sino
también el acceso al código. Además, para evitar que posteriores
modificaciones del programa queden sin ser accesibles, es menester algún
tipo de mecanismo legal que impida que esto suceda.

Así es como nace la idea de las licencias de uso, que si bien en ningún
caso es una negación de los derechos de autor o de patentes ---al menos
no desde un marco jurídico---, sí es una flexibilización en la gestión
de lo que ahora se aglutina en el término de «propiedad intelectual». La
explicación dada por Creative Commons no podía ser más clara: 
**no es *todos* sino *algunos* derechos reservados**.

Una de las primeras licencias de uso fue la creada por la +++FSF+++: la General
Public License (+++GPL+++). Una de las principales características de esta
licencia es su carácter hereditario: todo lo que se cree a partir de un
programa liberado con +++GPL+++ tendrá que tener a su vez +++GPL+++. Desde [_El
manifiesto de +++GNU+++_](https://www.gnu.org/gnu/manifesto.es.html) se
explicita que esto se debe a que es una forma de tener garantizada la
libertad en los cuatro sentidos mencionados con anterioridad.

Sin embargo, un grupo de programadores que formaban parte del movimiento
no les agradó el carácter moralizante de lo entendido por «libertad» y
los mecanismos necesarios para defenderla. Lo hereditario se percibió
como virulento y, ante el desacuerdo, en 1998 se bifurcó el movimiento
del código abierto (_open source_).

Uno de los integrantes más conocidos es [Eric
Raymond](https://es.wikipedia.org/wiki/Eric_S._Raymond) cuyo escrito
[_La catedral y el bazar_](http://biblioweb.sindominio.net/telematica/catedral.html) 
hace patente el desapego de la cuestión moral, así como se dan una serie 
de «aforismos» prácticos para el desarrollo de _software_ en un esquema
abierto.

**Una de las principales diferencias entre el** **_software_** **libre y
el código abierto es que el primero explícitamente hace hincapié en el
carácter ético y social del desarrollo de** **_software_, mientras el
segundo se aboca más a la cuestión pragmática que implica este
desarrollo**.

En el _software_ libre, ningún programa que forme parte de un proyecto
libre puede ser privativo, cuyo autor no ha hecho público el código,
normalmente como estrategia para su venta. En el código abierto es
posible convivir con ciertos elementos privativos, si en la práctica se
quiere conseguir un producto lo más abierto posible o hacer funcionar el
_hardware_ de un dispositivo.

¿El _software_ libre o de código abierto es gratuito? Sí y no. Cualquier
persona puede descargar de manera gratuita el _software_ libre o de
código abierto, pero esto no impide que alguien pueda vender este
_software_, sea para recuperar los costos marginales o como forma de
conseguir recursos para un colectivo u organización. Por esto **lo
gratuito, lo abierto y lo libre no siempre son sinónimos**.

## **Creación y edición abierta**

La discusión dentro del desarrollo de _software_ pronto se generalizó al
quehacer cultural en general. La pretensión ética y social del
_software_ libre catapultó sus inquietudes a lo que Lessig empezaría a
trabajar bajo el término de «cultura libre».

El enfoque pragmático para el acceso al código permitió la gestación de lo
que ahora conocemos como «acceso abierto» (_open access_). Si bien lo
abierto y lo libre en muchas ocasiones conviven sin problemas, en el
acceso abierto lo primordial es la disponibilidad del contenido. En este
sentido, los colectivos, organizaciones o instituciones enfocadas al
acceso abierto no tienen la necesidad de ir más allá de la apertura en
el producto cultural *final*.

En la cultura libre se insiste en la relevancia social de la apertura en
el acceso de las creaciones culturales. Pero también la cultura libre
implica una postura ética hacia nuestra cultura. Creative Commons es una
síntesis de estos esfuerzos al ser un conjunto de licencias de uso
pensadas para aquellos *autores* que desean permitir la distribución y
uso de su obra bajo la confianza de que el usuario no empleará su
creación para otros fines a los permitidos por estas licencias.

**Creative Commons es una apuesta social y ética no punitiva respecto a
nuestra herencia cultural orientada a la** ***creación***. Y es aquí
donde empezamos a ver sus límites cuando en lugar de *creación* hablamos
de *edición*. Una edición con +++CC+++ únicamente tiene la necesidad de hacer
accesible el producto *final* de un conjunto de quehaceres culturales.

Por ejemplo, si hablamos de la edición de una
[obra](https://marianaeguaras.com/crear-una-obra-o-escribir-un-libro-aclaracion-conceptual/),
hay esfuerzos conjuntos de los autores, los editores, los diseñadores y
más personas para producir un libro. Esta edición, si tiene +++CC+++, ha de
estar a disposición pública para su distribución y uso. Sin embargo, en
ningún caso esto implica que los archivos que fueron empleados para
producir la obra tengan que ser liberados: solo existe el compromiso de
abrir el producto final.

**¿Qué inconvenientes tiene esto para la edición?** El solo tener el
acceso al producto final implica que en muchos casos será necesario un
proceso de ingeniería inversa para dar de nuevo con una serie de
contenidos útiles para la reedición de una obra.

**La extracción del texto y de las imágenes, y su reformateo o
enmendación es ingeniería inversa**, que una vez dominada la vuelve un
trabajo innecesario. La ingeniería inversa es un buen método de
aprendizaje, pero una vez pasado el umbral pedagógico se convierte en
una actividad monótona y consumidora de una enorme cantidad de tiempo.

En este sentido, **las licencias +++CC+++ dentro en el mundo editorial dan
fruto a obras abiertas mas no libres**, ediciones cuyo único acceso es
el producto final, afín para el usuario y el lector promedio, pero rara
vez útil o funcional para editores o desarrolladores interesados en
crear nuevas ediciones u otros formatos.

## **Edición libre**

Aunque al hablar de «edición» se hace mención a la «edición de
publicaciones», el aspecto editorial cubre otro tipo de productos
culturales cuya ingeniería inversa rara vez permite dar con información
con la misma calidad que los contenidos originales utilizados para su
creación. Los casos más evidentes es la edición de música o de videos,
donde la separación de las distintas capas visuales o sonoras es una
tarea muy compleja, sino inútil.

Por la falta de necesidad de liberar el contenido originario de un
producto final licenciado con +++CC+++, surge la idea de la «edición libre».
Otro de sus antecedentes pueden encontrarse en la inquietud dentro del
_software_ libre por crear documentación ---como manuales impresos---
que al mismo tiempo sean libres; es decir, que no exista la necesidad de
pagar por ellos ---aunque tampoco se impide esta posibilidad--- y que
cualquier usuario pueda reeditarlos.

La edición libre, más que antagónica a la edición abierta, es hacer
explícitas las necesidades implicadas en la edición de material mediante
la constitución de ciertas obligaciones para autores y editores para
permitir el uso directo de los archivos originales.

Así como en el _software_ libre la disponibilidad del código es
necesaria, **en la edición libre el acceso a los archivos utilizados
para la edición no es negociable**. Por supuesto, esto presenta la
ventaja o la desventaja de tener un fuerte sentido ético al anunciar que
la apertura del producto final no es suficiente: que la liberación de
todo el material empleado es una necesidad para otros editores.

La edición libre también tiene implícito un carácter que incluso dentro
del _copyleft_ es problemático: la **reedición**. Con las ediciones
abiertas o en el acceso abierto el usuario tiene la libertad de usar
distintos aspectos de estas obras; p. ej., para citarlos o para hacer
obras derivadas. No obstante, el límite de esta práctica se vuelve
conflictivo cuando no solo hablamos de uso *derivado* de obras, sino la
modificación es estas mismas en pos de una mejor edición.

Cuando en un libro o en _paper_ como lectores encontramos alguna errata
o información incorrecta, lo usual es responderle al autor o al editor
para que este rectifique o corrija la información. Rara es la vez que al
propio lector se le permite modificar directamente la obra. **La edición
libre implica que cualquier usuario en potencia puede modificar, aunque
sea ligeramente, una obra**, incluso para tergiversarla…

La tergiversación o el plagio no son problemas resueltos en la edición
libre, pero tampoco lo son en las ediciones abiertas, las licencias +++CC+++,
el acceso abierto e incluso mediante fuertes restricciones de los
derechos de propiedad intelectual como el
[+++DRM+++](https://marianaeguaras.com/tipos-de-drm-drm-social-o-blando-versus-drm-duro/).
**La edición libre solo quiere decir que como editores no tenemos la
necesidad de hacer un doble trabajo**.

## **Licencias para la edición libre**

### **Licencia de Producción de Pares**

La [Licencia de Producción de
Pares](https://endefensadelsl.org/ppl_es.html) (+++LPP+++) está basada en la
licencia +++CC BY-NC-SA+++ 3.0, lo cual implica que se permite el uso de la
obra licenciada siempre y cuando no se ignore la atribución, no se
utilice para fines comerciales y se comparta con el mismo tipo de
licencia. Pero en lugar de pertenecer al movimiento de _copyleft_, se
inscribe en el **movimiento de _copyfarleft_**. Es decir, permite el uso
de la obra siempre y cuando sus autores [«conserven el valor del
producto de su
trabajo»](http://www.rebelion.org/noticias/2013/2/163592.pdf), lo cual
quiere decir que no solo es «no comercial» sino también «no
capitalista».

Muchos lectores podría causarles inquietud el carácter «no capitalista»
de la +++LPP+++. Lo dejaré como asunto pendiente, ya que lo que quiero tratar
sobre la +++LPP+++ es la cuestión de «libertad» de la edición de obras con
+++LPP+++…

**La Licencia de Producción de Pares no obliga explícitamente a liberar
todos los contenidos utilizados para la producción de una obra**. En sí
no es una licencia de edición libre ---ni creo que su interés sea
este---. Sin embargo, por la manera en cómo ha sido utilizada por
colectivos y organizaciones, en varias ocasiones funciona como una
licencia de edición libre, ya que no solo está disponible el producto
final, sino los archivos que ayudaron para la producción de la obra.

Un ejemplo de proyecto editorial que utiliza la +++LPP+++ como edición libre
es [Ediciones La Social](https://edicioneslasocial.wordpress.com/). A
diferencia de otras editoriales que liberan sus contenidos con licencias
+++CC+++, este proyecto editorial también facilita los archivos editables de
cada una de sus obras, lo que permite su reedición o creación de otros
formatos.

Un ejemplo que va al caso con este tema es el libro [_Muestra Libre.
Reflexiones sobre Piratería, Software Libre y Diseño_](https://edicioneslasocial.wordpress.com/2017/02/28/muestra-libre-reflexiones-sobre-pirateria-software-libre-y-diseno-guadalupe-rivera/),
que está disponible en +++PDF+++ o en el formato abierto de procesador de
textos (+++ODT+++). El acceso al archivo editable me permitió desarrollar sin
dificultades, junto con una amiga, su [versión
+++EPUB+++](http://libgen.io/book/index.php?md5=58CB73C5BACC0B507EA1B5F87A4B9FB8),
cuya publicación no causó molestia a Ediciones La Social.

Esto no es un caso aislado, sino una muestra de la tendencia de
proyectos editoriales que utilizan la +++LPP+++ en sus obras. Lo que en estos
colectivos u organización se hace patente es la búsqueda de crear redes
cooperativas de trabajo o al menos de no impedir o interferir en el
trabajo realizado por otros.

*Mucho ayuda el que no estorba*, y de este modo la +++LPP+++ funciona como
licencia de edición libre al no dificultar el quehacer editorial
mediante la reserva de los archivos editables. **De la intención de
desarrollar un +++EPUB+++ con la edición en español de** ***Free Culture***
**a la creación del +++EPUB+++ de** ***Muestra Libre*** **existe un mar de
diferencias que ejemplifica la distinción entre la edición abierta y la
edición libre**.

### **Licencia Editorial Abierta y Libre**

Hace algún tiempo en Perro Triste se lanzó una propuesta de licencia de
uso en pos de la edición libre: la [Licencia Editorial Abierta y
Libre](https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre)
(+++LEAL+++, acrónimo sugerido por un
«[hackeditor](https://twitter.com/hacklib)», porque en un principio fue
+++LELA+++). **Algunas de las características de la +++LEAL+++ son las siguientes**:

* Los autores o editores tienen la obligación de hacer pública los
  archivos editables utilizados durante la producción de una obra.
* La comercialización es posible, siempre y cuando no sea el único
  medio de adquisición de los archivos editables o del producto final.
* La licencia supone un acuerdo previo entre las personas involucradas
  en la producción de una obra para que esta sea abierta y libre.
* La atribución no es necesaria.

La +++LEAL+++ hace posible que cualquier usuario tenga acceso a los archivos
editables, con lo cual se evita la ingeniería inversa al mismo tiempo
que fomenta la reedición de las obras sin la necesidad de pedir permiso
a los autores o editores «originales».

Ejemplos de uso de la +++LEAL+++ están la misma +++LEAL+++, las [publicaciones
hechas por Perro
Triste](https://github.com/ColectivoPerroTriste/Ebooks), algunos [libros
elaborados por una compañera de
trabajo](https://github.com/MelisaPacheco/Libritos) y el material que se
publicó como la [«Historia de la edición
digital»](https://github.com/NikaZhenya/historia-de-la-edicion-digital),
[«Derecho de autor
cero»](https://github.com/NikaZhenya/derecho-de-autor-cero), [mis
entradas en este _blog_](https://github.com/NikaZhenya/entradas-eguaras)
y hasta escritos aún en proceso de redacción como
[«Cryptodoc»](https://github.com/NikaZhenya/cryptodoc).

Al permitirse la comercialización, la +++LEAL+++ no forma parte del movimiento
de _copyfarleft_ como la +++LPP+++. Más que un fomento al uso «capitalista» de
las obras licenciadas con +++LEAL+++, obedece a la intención ya presente en el
_software_ libre de no imposibilitar la comercialización al poder ser un
mecanismo de apoyo a los proyectos editoriales que publican con +++LEAL+++. No
obstante, es una de las características potencialmente problemáticas que
aún no se ha analizado a fondo, ya que en sí el _copyfarleft_ no prohíbe
la comercialización con el fin de mantener vivo un proyecto cultural.

La liberación de los archivos editables muchas veces implica la
publicación del trabajo realizado por un grupo de personas, las cuales
no en pocas ocasiones estarán de acuerdo en ello. Por esto, la +++LEAL+++
supone un acuerdo previo, para así evitar que los usuarios que se valen
de una obra con +++LEAL+++ tengan que pedir permisos de uso a cada uno de los
involucrados o, peor aún, que se vean implicados en un proceso legal.

El acuerdo previo no es resuelto por la +++LEAL+++, sino que este puede ser
desde un acuerdo informal o verbal, hasta la firma de contratos o
convenios de cada uno de los colaboradores. En este sentido el marco
legal presupuesto no está contemplado por la +++LEAL+++, lo que también la
constituye como un punto débil.

Por último, la ausencia de atribución es contraria a uno de los
supuestos dentro del _copyleft_ y del _copyright_: en uno u otro caso
supone que cada obra *derivada* ha de hacer mención a los autores y
editores de la obra *originaria*. Si bien una de las dos tipos de
licencias propuestas, la +++LEAL-A+++, añade la necesidad de atribución, la
+++LEAL+++ estándar prescinde de ello. ¿Por qué? La +++LEAL+++ no solo tiene la
intención de ser una licencia de uso para ediciones libres, sino abrir
la discusión a lo que suponen los conceptos de «atribución», «autoría» y
«obra» (*derivada*). Este es el principal punto conflictivo que le he
visto a la +++LEAL+++ y que, por supuesto, requiere discusión.

Como sea, la +++LEAL+++ es solo una propuesta por estas y otras dificultades
---por ejemplo, su relación con otras licencias de uso que solo se
enfocan a la creación, como las +++CC+++---. Lo relevante aquí es rescatar que
**la edición libre va más allá de las licencias Creative Commons, ya que
su objetivo no es solo la apertura del producto final, sino
explícitamente hacer públicos los archivos editables**.

Si gran parte del sector editorial continúa siendo escéptico a Creative
Commons, ¿qué tanta pertinencia tiene un movimiento ---la edición
libre--- que también pretende la publicación de los archivos editables
de una edición?

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «Edición libre, más allá de Creative Commons».
* Título en el _blog_ de Mariana: «Edición libre, más allá de Creative Commons».
* Fecha de publicación: 22 de noviembre del 2017.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/14%20-%20Edici%C3%B3n%20libre,%20m%C3%A1s%20all%C3%A1%20de%20Creative%20Commons/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/edicion-libre-mas-alla-creative-commons/).

</aside>
