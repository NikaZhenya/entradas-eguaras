# 12. Cómo crear un libro digital: de +++XHTML+++ a +++EPUB+++ con Sigil

En esta entrada se detallan los pasos para hacer un libro electrónico.
Es decir, qué acciones deben hacerse para obtener un archivo +++EPUB+++
desde un +++XHTML+++ con el programa Sigil.

## Sigil, ven a mí

De todas las herramientas conocidas que existen para crear +++EPUB+++,
[Sigil](https://github.com/Sigil-Ebook/Sigil/releases) es la que siempre
recomiendo. Esto se debe a los siguientes motivos:

* Es _software_ libre, por lo que no solo es gratuito o todo su código
  está disponible, sino que también implica que no hay necesidad de
  pedir permiso para usarlo o modificarlo.
* Es la opción popular más ligera, su peso va de \~45 a \~60 +++MB+++,
  según el sistema operativo que utilices.
* Es un entorno gráfico pensado únicamente para crear +++EPUB+++; es
  decir, tenemos la certeza que cuenta con las herramientas necesarias para
  hacer un buen +++EPUB+++ de modo visual.

**Sigil fue el único programa que satisfizo mis primeras necesidades
para el desarrollo de +++EPUB+++.** Uno de los principales motivos fue
porque en aquel entonces andaba sobre [Ubuntu](https://www.ubuntu.com/), por lo
que me era imposible usar _software_ creado para Windows o Mac.

Pero otra razón fue que mis conocimientos en +++HTML+++ y
+++CSS+++ eran muy básicos y tampoco tenía un dominio sobre la terminal
o RegEx. Muchos de ustedes se encuentran en esta situación, así que si quieren
experimentar en la creación de +++EPUB+++, en lugar de InDesign ---el
cual es menos controlable, como [ya lo indiqué](@index[3])---, los
invito a que prueben con Sigil.

¿Cómo empezamos? Una ***primera desventaja*** de Sigil es que la
documentación existente para su uso no es tan amigable como la de otras
herramientas populares como InDesign o Jutoh. Cuando conocí Sigil, su
instalación me provocó un dolor de cabeza. Por fortuna, hoy es fácil
instalarlo en cualquier computadora.

El área de su documentación ha estado mejorando con el tiempo y aunque
hace muchos años ya no uso Sigil, quizá [este
tutorial](http://www.jedisaber.com/eBooks/Sigil01.shtml) podría ser un
buen punto de partida. Después de ahí, mi sugerencia es que metan sus
manos al lodo y con los errores vean las posibilidades y límites de
Sigil.

## Ejemplo de +++EPUB+++

Como ya venía anticipado en mi [primera
colaboración](@index[1])
con Mariana, vamos a tomar la edición del Proyecto Gutenberg del _Don
Quijote_ junto con algunas modificaciones en su estructura para producir
un +++EPUB+++ más limpio y con mejor diseño. Los archivos de ejemplo
pueden descargarse
[aquí](https://www.clientes.cliteratu.re/eguaras/epubs.zip).

**Con estos archivos nos será posible obviar el más grande problema en
la edición digital: el formato.** Como siempre he indicado, el [cuello
de botella en la
edición](@index[7]),
sea impresa o digital, es la falta de un formato adecuado. Esto provoca
que gran parte del tiempo de desarrollo de una publicación sea,
precisamente, la limpieza del formato. Vuelvo a insistir en el asunto
solo para explicitar que:

1. este breve tutorial pasa por alto el tiempo necesario para dar un
   formato adecuado a una publicación y que muchos de ustedes, cuando
   empiecen a experimentar, se darán cuenta de la monotonía y
   frustración que implica trabajar de manera gráfica, y
2. esto implica que no nos enfocaremos en los aspectos básicos de la
   creación del +++EPUB+++, debido a que ya está cubierto en los
   archivos de ejemplo.

Orientaré esta entrada a ciertos errores comunes que he encontrado en
los libros hechos por otros con Sigil, y que será una buena guía para
que ustedes puedan evitarlos.

## Manos a la obra

### 1. Mínima configuración general

**Uno de los errores más comunes es olvidar la configuración general de
Sigil.** Existen dos casos donde esto afecta directamente al +++EPUB+++:

1. por defecto Sigil crea +++EPUB+++ versión 2.0.1 cuando por lo menos
   es necesario crear a partir de la versión 3.0.0;
2. usa el idioma inglés como lenguaje por defecto para los metadatos
   del _ebook_.

Para cambiar esto es necesario ir a `Edit > Preferences…` o presionar
`F5`. En el área de `General Settings` indicamos que deseamos la versión 3.

![Cambio de versión a la 3.0.0.](../img/img_10-01.jpg)

Para especificar el lenguaje deseado de nuestros metadatos en la misma
ventana vamos a `Language` y en `Default Language For Metadata`
seleccionamos el idioma y la región de nuestra preferencia.

![Cambio de idioma.](../img/img_10-02.jpg)

Para que los cambios surtan efecto lo recomendable es reiniciar Sigil.
Con esto ya podremos crear +++EPUB+++ versión 3.0.0 que, si bien es la
versión más popular y aceptada por distribuidores ---p. ej. Amazon, iTunes o
Google---, no es la más reciente. Una ***segunda desventaja*** de Sigil
es que aún no genera +++EPUB+++ versión 3.1 o al menos 3.0.1.

### 2. Importación de archivos

Cuando abrimos Sigil o generamos un nuevo proyecto veremos la siguiente
estructura:

![Estructura básica generada por Sigil.](../img/img_10-03.jpg)

Como puede observarse, tenemos una serie de carpetas para poner ahí
nuestros contenidos, como texto, hojas de estilo, imágenes, fuentes,
audios, videos u otros elementos.

Para importar los +++XHTML+++ de nuestros archivos de ejemplo hay que
seleccionar la carpeta `Text` y con clic derecho escoger
`Add Existing Files…`. Esto nos abrirá esta ventana:

![Ventana de importación.](../img/img_10-04.jpg)

De nuestros archivos ejemplo vamos a seleccionar todos los +++XHTML+++
presentes en `epub > 2-sigil > recursos > xhtml`. Así es como estos
documentos serán incrustados en la carpeta donde van los textos del
+++EPUB+++.

![Archivos importadas y archivo innecesario.](../img/img_10-05.jpg)

El archivo `Section001.xhtml` no lo necesitamos así que lo seleccionamos
y presionamos `Del` o clic derecho `Delete…` para eliminarlo.

> *Recomendación*. Cuando generamos documentos +++XHTML+++ desde Sigil,
> por defecto sigue la nomenclatura `Section` más un número consecutivo. Esto
> puede generar problemas al momento de verificar o hacer cambios ya que es un
> nombre genérico. Lo ideal sería darle nombre significativos y con una
> numeración inicial. Para mayor información,
> [consulta estas recomendaciones](https://github.com/NikaZhenya/cursitos/tree/master/modulos/practicos/P000-Estructura).

Ahora bien, aquí tenemos dos elementos muy curiosos. El primero es que
si damos doble clic al archivo `000-portada.xhtml`, ¡la imagen de
portada ya está ahí!

![Portada importada automáticamente.](../img/img_10-06.jpg)

Sin embargo, si damos doble clic a `006-rey.xhtml` ---u otro archivo que
tenga texto---, vemos que los estilos son un desastre…

![Estilos mal visualizados.](../img/img_10-07.jpg)

La ***tercera desventaja*** de Sigil es que en ocasiones no muestra el
diseño correcto, por lo que una edición visual del contenido puede
resultar muy problemática. Por fortuna, ahora sabemos que hay un error
en la renderización de los estilos, así que vamos a obviar esta
visualización ---al final veremos cómo se visualiza realmente---.

**¿Por qué se ven la portada y los estilos?** Por defecto, Sigil importa
todo aquello que esté vinculado a los archivos +++XHTML+++ importados y
los coloca en las carpetas correspondientes. En nuestro caso,
`000-portada.xhtml` tiene vinculada la imagen `portada.jpg` que también
fue importada a `Images`; además, todos los +++XHTML+++ tienen un
vínculo a la hoja de estilos `principal.css`, que fue colocada en la carpeta
`Styles`.

![Archivos adicionales importados automáticamente.](../img/img_10-08.jpg)

### 3. Tabla de contenidos

Hasta aquí ya hemos terminado con los ajustes en nuestro contenido por
lo que ya podríamos proceder a generar el +++EPUB+++. **Este es otro de
los errores más comunes: el contenido y el diseño no lo son todo**. Existen
otras tareas muy importantes que nos dan mejor control, mayor calidad y
un producto más amigable para el lector.

Una de estas tareas es la modificación de la tabla de contenidos. Para
acceder a esta vamos a
`Tool > Table Of Contents > Generate Table Of Contents…` o presionando
`Ctrl + T` o el icono resaltado.

![Ventana de edición de la tabla de contenidos.](../img/img_10-09.jpg)

Sigil, de manera automática, detecta todos los encabezados en nuestro
documento ---etiquetas +++HTML+++ de la `h1` a la `h6`---, podemos
limitar su jerarquía en `<Select headings to include in TOC>`,
eliminar elementos deseleccionando la caja `include` o crear una tabla anidada
al usar las flechas para asignar jerarquías.

La ***cuarta desventaja*** de Sigil es que la creación de tablas
anidadas también afectará la estructura de nuestro documento. Por
ejemplo, esta obra consta de dos partes, así que lo ideal sería que cada
una de las secciones interiores no fueran `h1` sino `h2` para que solo
las partes queden en un nivel `h1`. Esta modificación haría que en cada
uno de nuestros documentos también cambie el encabezado `h2` cuando, en
realidad, lo único que queremos es *afectar la tabla de contenidos, no
los contenidos*.

Por este problema, vamos a eliminar los elementos que se deseen pero
también vamos a mantenerlos a todos con el nivel detectado. De lo
contrario, tendremos una mala visualización en nuestro resultado final.

> *Aclaración*. Lo que en español conocemos por *índices* en el mundo anglófono
> se conoce como *table of contents*, y lo que nosotros denominados *índice
> analítico, índice onomástico*, etc., ellos lo llaman *indexes*. Para evitar
> confusiones, prefiero usar el término *tabla de contenidos*.

### 4. Metadatos

**Los metadatos son uno de los elementos más importantes de un
documento**, como ya lo [ha señalado
Mariana](https://marianaeguaras.com/que-son-los-metadatos-de-un-libro-y-cual-es-su-importancia/).
De manera general son el documento de identidad de nuestro libro, por lo
que no pueden ignorarse y han de llenarse correctamente.

Para editar los metadatos nos vamos a `Tools > Metadata Editor…` o
presionamos `F8` o el icono resaltado.

![Ventana de edición de los metadatos.](../img/img_10-10.jpg)

Para añadir metadatos damos clic sobre `Add Metadata` y para modificar
su valor hacemos doble clic sobre cada `value`. Para este ejemplo lo
mínimo que requerimos es el autor, el lenguaje, el título y el titular
de los derechos, pero ¿por qué no pruebas con agregar más campos?

> *Recomendación*. No existe consenso sobre la forma de escribir el nombre de
> los colaboradores; no obstante, es aconsejable usar la nomenclatura
> `Apellidos, Nombres`. Esto se debe a que en las bibliotecas digitales es
> posible ordenar los libros por nombre del autor.

En este punto, de nuevo podríamos generar nuestro +++EPUB+++, porque ya
contamos con los elementos esenciales de un _ebook_:

* Contenido
* Diseño
* Tabla de contenidos
* Metadatos

Sin embargo, más vale limpiar un poco el proyecto para evitar
potenciales errores en la verificación.

### 5. Regeneración y limpieza

Como hemos estado haciendo modificaciones a la tabla de contenidos y a
los metadatos, **tenemos la necesidad de volver a generar los archivos**
que guarden esta información. De no ser así, tengan por seguro que en la
verificación aparecerán errores o advertencias.

* Para regenerar el archivo con los metadatos vamos a
  `Tools > Epub3 Tools > Update Manifest Properties`.
* Para regenerar las tablas de contenidos hacemos clic en
  `Tools > Epub3 Tools > Generate NCX From Nav`.

> *Aclaración*. Los +++EPUB+++ versión 3 tienen dos tablas de
> contenidos: un `nav.xhtml` para lectores recientes y un `toc.ncx` para
> dispositivos viejos.

Ahora procedemos a limpiar nuestro proyecto:

* Para eliminar archivos innecesarios hacemos clic en
  `Tools > Delete Unused Media Files…`.
* Para eliminar estilos que no se usan de las hojas de estilo, sino
  que vamos a `Tools > Delete Unused Stylesheet Classes…`.

![Ventana de eliminación de estilos no usados.](../img/img_10-11.jpg)

La hoja de estilos `principal.css` que está vinculada a los +++XHTML+++
es un archivo +++CSS+++ con todas las clases que uso por defecto para
cualquier clase de obra. Por este motivo, existen clases que no fueron
necesarios para este libro, por lo que es detectado por Sigil y solo hacemos
clic en `Delete Marked Styles`.

### 6. Creación y verificación

Sigil nos ofrece una herramienta para verificar el estado de nuestro
+++EPUB+++, que está en `Tools > Well-Formed Check EPUB` o presionando
`F7`.

![Analizador de errores de Sigil.](../img/img_10-12.jpg)

Nuestro proyecto no tendría que tener errores, pero evitemos engañarnos:
esto no asegura que el resultado final esté exento de errores o
advertencias. Esto me parece una ***quinta desventaja***, ya que es
común que el usuario piense que, por la ausencia de errores en este
panel, no es necesario la verificación con herramientas externas.

Esto es tan común que la mayoría de los _ebooks_ que he descargado, por
lo general, tienen errores o advertencias que en un determinado momento
pueden afectar la experiencia de lectura.

Para demostrarlo, vamos a generar por primera vez nuestro libro desde
`File > Save` o presionando `Ctrl + S` o el icono resaltado.

![Generación de archivo +++EPUB+++.](../img/img_10-13.jpg)

Esto crea un archivo +++EPUB+++ que se puede visualizar y, al parecer,
no presenta problemas. Sin embargo, si usamos Epubcheck ---la herramienta
oficial para la verificación de +++EPUB+++ desarrollada por el
[+++IDPF+++](http://idpf.org/) y que está disponible [en
línea](http://validator.idpf.org/) o [para su
descarga](https://github.com/IDPF/Epubcheck/releases)--- tenemos el
siguiente resultado:

![Primer resultado de Epubcheck.](../img/img_10-14.jpg)

Se trata de una advertencia: no coincide el identificador presente en el
archivo `toc.ncx` ---el de las tablas de contenidos--- con en el archivo
`content.opf`, que contiene los metadatos, la declaración de contenidos
y el orden de lectura del libro.

Siendo más específicos, en el `toc.ncx` tenemos un identificador de 32
caracteres separado por guiones. Mientras tanto, en el `content.opf`
tenemos el mismo identificador pero con el sufijo `urn:uuid:`, por lo
que hace que carezca de igualdad. Para evitar la advertencia solo
tenemos que eliminar el sufijo. Sin embargo, estos vericuetos en la
comprobación de un +++EPUB+++ son fruto de la experiencia y uno que
otro golpe al teclado a medianoche…

No lo considero una desventaja por el simple hecho de que ningún
programa popular para la generación de +++EPUB+++ cuenta con una
herramienta externa de verificación o, mejor aún, una especie de
[depurador](https://es.wikipedia.org/wiki/Depuraci%C3%B3n_de_programas)
que nos asista durante la verificación del +++EPUB+++. Sin embargo, esto
implica que **para la generación de _ebooks_ sin ningún detalle técnico
es menester el conocimiento de la estructura del +++EPUB+++**, que va
más allá de lo visual.

Entonces, en Sigil vamos al archivo `content.opf` y eliminamos el
sufijo.

![Localización del elemento a eliminar.](../img/img_10-15.jpg)

Una vez hecho esto, tendremos un resultado como el siguiente:

![Archivo enmendado.](../img/img_10-16.jpg)

Ahora solo basta con volver a generar y verificar el +++EPUB+++ para
encontrar que ¡ya tenemos un +++EPUB+++ sin errores ni advertencias!

![Verificación exitosa del +++EPUB+++.](../img/img_10-17.jpg)

> *Recomendación*. La descarga de Epubcheck implica usar la terminal, lo que en
> un primer momento es más difícil de usar. Sin embargo, presenta estas ventajas
> en comparación con su uso en línea: menor tiempo de verificación, carencia de
> límite en el peso del +++EPUB+++ y posibilidad de empleo _offline_. En
> la medida posible, es mejor idea empezar a usar la versión descargable.

### 7. ¡Listo!

Ahora ya podemos abrir nuestro +++EPUB+++ para verlo y, como puede
observarse, tenemos la tabla de contenidos como la estipulamos y ¡los estilos
se ven correctamente!

![Visualización final del +++EPUB+++.](../img/img_10-18.jpg)

Este archivo podemos enviarlo a nuestro dispositivo favorito de lectura
o subirlo a alguna tienda. Ahora bien, ¿por qué no intentas hacer un
+++EPUB+++ con otra obra?

> *Aclaración*. Por defecto, Sigil no modifica el identificador de libro en cada
> nueva generación que hacemos, además de que conserva el mismo nombre de
> archivo. Esto provoca ciertos problemas en algunos lectores. En iBooks existe
> la posibilidad de que abra el archivo más viejo, ya que al detectar el mismo
> identificador, mostrará el +++EPUB+++ más antiguo sin importar que
> este haya sido eliminado. En Adobe Digital Edition se ha detectado la misma
> situación, pero con el nombre del archivo. Si experimentas este problema,
> prueba con cambiar el identificador en el `content.opf y el toc.ncx`, así como
> cambiar manualmente el nombre al archivo.

## Sigil, siempre te recordaré

Las cinco desventajas que indiqué no fueron los motivos por los que
decidí dejar de usar Sigil; al final, para la mayoría de los lectores,
editores o desarrolladores carece de importancia. **Con el tiempo opté
por la [creación desde
cero](@index[1]),
porque de manera visual no obtuve el control deseado y la verificación
se volvía muy escabrosa.**

Más que un abandono de esta gran herramienta, me vi obligado a buscar
otra metodología de desarrollo. La corrección de errores y advertencias
me permitió conocer a fondo la estructura del +++EPUB+++, tanto del lenguaje
+++HTML+++ y +++CSS+++ para el contenido, estructura y diseño del libro, como del
+++XML+++ que se emplea para la declaración de las tablas de contenidos, los
metadatos, el manifiesto, la espina y otras particularidades. Bajo este
aprendizaje, el desarrollo visual de un +++EPUB+++ me terminó por consumir más
tiempo.

Este empoderamiento tecnológico me ha permitido estar desarrollando una
serie de **herramientas para el quehacer editorial** que se llaman
**[Pecas](https://github.com/NikaZhenya/Pecas)**. El último ejemplo que
haremos con el _Don Quijote_ será usando esas herramientas.

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «De XHTML a EPUB con Sigil».
* Título en el _blog_ de Mariana: «Cómo crear un libro digital: de XHTML a EPUB con Sigil».
* Fecha de publicación: 3 de octubre de 2017.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/12%20-%20De%20XHTML%20a%20EPUB%20con%20Sigil/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/de-xhtml-a-epub-con-sigil/).

</aside>
