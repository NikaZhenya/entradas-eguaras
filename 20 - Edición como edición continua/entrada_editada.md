# 20. Edición como edición continua

En varias entradas pasadas me he dedicado a diferenciar los distintos
tipos de edición, como son la tradicional, la cíclica y la ramificada.
(Para más información, consulta la tríada de artículos «Edición cíclica
y edición ramificada» empezando por [aquí](@index[8]).

De manera concisa, la **edición tradicional** es el conjunto de
conocimientos, técnicas y métodos que se han empleado desde la invención
de la imprenta y por medio de medios análogos.

La **edición cíclica** es una extensión de esta tradición, pero
utilizando tecnologías digitales, por el cual el quehacer se centra en
un dispositivo: la computadora; así como surge el multiformato: el
_ebook_ _junto_ al impreso. **La edición es ya edición digital** en este
contexto.

Por último, la **edición ramificada** es la propuesta metodológica que
he estado trabajando y cuyo origen es el _single source and online
publishing_, el cual pretende crear una diversidad de formatos a partir
de un conjunto de «archivos madre».

La gran diferencia entre este último tipo de edición con el resto es que
la producción de los formatos se da de forma paralela y descentralizada:
**la edición ramificada invierte el problema de «a más formatos, mayor
tiempo, mayor uso de recursos y menor control»**.

Pero no solo eso, **la metodología ramificada también pone sobre la mesa
dos temas: la completud de una obra y la dependencia tecnológica
implicada en la producción de publicaciones**.

Es aquí donde al fin me dedico a hablar sobre un término que he
mencionado con brevedad en dos entradas anteriores: «edición continua».

## Más allá de las ediciones de una obra

Como lo mencioné en [otra entrada](@index[16]),
**la completud de una obra, por lo general, es resignación**: los
tiempos acotan las horas de trabajo, los recursos consumen su
posibilidad de realización y la carencia de conocimientos técnicos
limitan sus características. La producción de una publicación es un
proceso arduo y, en más de una occasion, no se desarrolla según lo
planificado.

Esto da pie a diversas implicaciones:

-   En un **contexto multiformato** abre la puerta para un mayor
    descontrol y pérdida de calidad, debido a que el personal tiene que
    estar capacitado sobre las características de cada formato.
-   En un **contexto cíclico** se traduce en tener que emplear menos
    recursos y tiempo para producir más formatos en un mismo periodo.
-   En un **contexto propietario** quiere decir que se ha de invertir en
    _software_ y _hardware_ cada vez más especializado para dar un
    cumplimiento afín a la calidad y el calendario estimados.

Debido a estas cuestiones **es común que las personas dedicadas a la
edición, por lo general, recuerden la publicación de una obra como un
proceso traumático o al menos insatisfactorio**: siempre se pudo haber
hecho más, pero los tiempos, el presupuesto, la computadora, el
personal…

La mayoría de los editores ya tienen serios problemas al momento de
lidiar con las diversas ediciones de una obra y esta dificultad se
exacerba cuando en el ámbito digital existe la posibilidad del uso de
versiones. Si cada edición indica la fecha de completud de una obra, en
el versionado se hace patente el nivel de maduración de un proyecto.

La diferencia no es nimia. **En el número de edición se apela a la
completud de la obra, mientras que en el número de versión se explicita
su incompletud**. En el versionado la completud es una idea que busca su
cumplimiento mediante el crecimiento paulatino de un proyecto.

La completud deja de ser concreta y pasa a ser una guía para el rumbo
que tome una publicación. La completud exhibe que la «obra» no es el
puerto destino, sino que esta está en constante «evolución», cuya
prioridad es la gestación del «proyecto». En fin, la completud es el
ensueño de la imaginación del editor.

El versionado da dinamismo, pero también acarrea un mayor nivel de
compromiso. Siempre es una ventaja suponer que la edición marca el fin
de un proyecto, aunque sea solo temporal: es la calma después de la
tempestad.

No obstante, en el versionado el proyecto permanece abierto: a cada
instante se está a la intemperie, por lo que un constante mantenimiento
es necesario para evitar el naufragio. **Cuando un proyecto deja de
tener versiones, en lugar de decir que se ha completo, se indica que se
ha abandonado**. Es decir, en el versionado la completud nunca llega.

Este dinamismo puede ser menos accidentado si se tiene una metodología
que permita automatizar y hacer menos tortuoso cada uno de los
procedimientos implicados en la publicación de una obra**. La edición
ramificada es una propuesta para subsanar las dificultades en esta
dinámica**, aunque implica el aprendizaje explícito de un método de
producción y las técnicas y los conocimientos que vienen embebidos en
ello.

## Más allá de las versiones de una obra

Pero **cuando esta conciencia metodológica y el control técnico son
alcanzados, el versionado deja de ser necesario** ---y no solo hablo
sobre el método ramificado, sino cualquier tipo de metodología que
permita los mismos resultados---.

No es que el versionado se torne anticuado o inconveniente, sino que en
este contexto es posible alcanzar tal grado de dinamismo ---un
movimiento vertiginoso de constantes cambios sobre el proyecto--- que el
versionado aumenta rápidamente. Por lo general, la idea de manejo de
versiones busca ir paso a paso, en cada nueva versión se pretende
incluir una serie de mejoras o arreglos en un solo paquete que vuelve a
instalarse nuevamente.

Sin embargo, cuando los cambios son tan constantes, puede incluso
resultar confuso qué mejoras o arreglos incluir en la siguiente versión,
así que por pragmatismo o pereza se recurre a la liberación continua. En
lugar de crear paquetes nuevos ---todo el proyecto una y otra vez---, se
actualizan los paquetes ---el proyecto es el mismo y solo se descargan
sus cambios---.

**«Libera pronto, libera continuamente»** es una frase atribuida a Eric
S. Raymond, personaje clave para el movimiento del código abierto. Y lo
que implica es la publicación constante de cambios que aunque parezca
caótico, marca un ritmo de desarrollo constante que permite a los demás
tener un contacto de primera mano con la gestación de un programa.

En inglés, este modelo de desarrollo se conoce como «_rolling release_».
Y la idea de «rodante» hace más clara la imagen de la vida de un
proyecto como una bola de nieve: movimiento cada vez más robusto y
acelerado, que continúa hasta colapsar.

## El _rolling release_ en la edición

**La publicación continua puede tener un mal sabor de boca para muchos
editores**. Implica que cada vez que se hace un cambio, la obra de nuevo
se publica: una práctica que atenta directamente a la idea de ediciones,
pero también a la de versiones.

Pero ¿cuántos no han hecho «trampa» y han colado algunas correcciones en
el archivo y aun así la siguen catalogando como la misma edición o
versión? **El dinamismo en la edición no es una novedad**, pero lo que
se admite en un modelo de liberación continua es que la obra ya no es la
misma. Caso contrario es fingir la identidad de la obra al conservar el
número de edición o de versión, cuando la publicación ya se ha
modificado.

**La liberación continua es franca con el lector**: «la obra no está
completa, sigue en constante cambio, disculpa las molestias». Incluso en
su colapso: «esta obra ya ha ido demasiado lejos que no puedo mantenerla
más». El abandono ya no sabe a fracaso, sino a un desvío de intereses o
de energía. Además, si el proyecto es abierto implica una apertura para
que otras personas le den continuidad.

Ejemplos de esta liberación continua la podemos ver en el historial de
cambios de [Pecas](http://pecas.perrotuerto.blog/), _una_ propuesta concreta
de la metodología ramificada de publicación y que a esta fecha tiene ya
[573 actualizaciones](https://github.com/NikaZhenya/pecas/commits/master) en
tan solo dos años.

Otro ejemplo de edición continua es [_Edición digital como metodología para una edición global_](http://ed.perrotuerto.blog/), 
la antología de estas entradas que a la fecha cuenta con [124 actualizaciones](https://github.com/NikaZhenya/entradas-eguaras/commits/master)
en casi dos años y 59 actualizaciones desde su [primera publicación](https://github.com/NikaZhenya/entradas-eguaras/commit/316dfa552f2dbb047862bc4ddf860145891b5a19),
hace cuatro meses y medio.

¿Imaginan lo inverosímil que sería indicar que en una obra que aún no ha
alcanzado ni medio año de vida ya cuenta con 59 ediciones o versiones?

## Identificación en la avalancha

Como se puede observar en los historiales de cambios, **la liberación
continua no es sinónimo de pérdida de control**. Al contrario, cada
cambio tiene un [identificador único universal (+++UUID+++)](https://es.wikipedia.org/wiki/Identificador_único_universal),
fecha ---incluyendo huso horario---, usuario que realizó el cambio
---con correo electrónico incluido--- y hasta qué modificación concreta
se hizo a cada archivo.

Este gran dominio es gracias a [git](https://es.wikipedia.org/wiki/Git),
un tipo de control de versiones cuyas características generales se han
catalogado como [el octavo elemento metodológico](@index[10])
de la edición ramificada.

**Siempre es posible saber el cuándo, el qué y el quién**. El nivel de
detalle es tal que incluso da más información sobre la «evolución» de
una obra que la consulta de formatos finales, también conocidos como
ediciones o versiones; o al menos facilitan su consulta.

Además, este proceso de identificación visibiliza dos cuestiones. La
primera es que no hay método más sencillo para identificar y mencionar
la antigüedad de una obra que su fecha. El uso de +++UUID+++ es mucho
más exacto, ya que en un mismo día puede haber varias actualizaciones,
pero es poco legible por humanos, _¿qué significado tendrá un conjunto
de caracteres alfanuméricos?_

Esta falta de legibilidad también es compartida con la numeración para
las ediciones o versiones: «primera edición», «versión 2». ¿Qué quiere
decir eso para conocer la antigüedad de una obra? ¡Qué año data esa
dichosa «primera edición» o «versión 2»!

## La obra evoluciona cuando deja de ser el centro y la publicación se desecha…

Esto abre paso a la segunda cuestión. Como también puede observarse en
el historial de cambios de la antología, muchas de las modificaciones no
se hicieron directamente a los formatos publicados: unas son adiciones
de nuevas entradas, otras son actualizaciones de los archivos madres y
algunas más son modificaciones del _script_ que produce los formatos o
implementaciones de nuevas funcionalidades de Pecas.

**El control en la liberación continua no solo es sobre la obra, sino
del proyecto**, sobre el material implicado para producirla. El
bibliófilo no solo puede ver de manera más amena la evolución de la
obra, sino que también tiene acceso a los procesos que subyacen detrás
de su producción.

La cuestión no es paupérrima. **La tradición editorial ha hecho del
libro su centro de atención** y agregaría que, más bien, lo ha hecho **a
un formato en particular: el impreso**. La imagen no parece problemática
e incluso su crítica parece contraintuitiva: si tu gremio se dedica a
publicar obras, ¿qué problema hay con que el libro sea el centro de ese
universo?

### El «librocentrismo» en la industria editorial

El empeño por querer tener formatos finales ---los únicos elegibles para
la publicación--- hace que la persona editora se centre más en el
producto final ---en muchos casos como «mercancía»--- que en los pasos
necesarios para obtenerlo. O bien, cuando nace la preocupación por los
procesos necesarios, **el enfoque centralizado en la obra supone que a
cada formato le corresponde su propia vía de gestación o, peor aún, que
un formato _precede_ a los demás**.

El «librocentrismo» hace de la publicación el desenlace _necesario_ de
los procesos editoriales. Para un público general, esto se oculta hasta 
el punto de minimizar el [quehacer editorial](@index[19]):
«¿por qué tarda tanto si solo es un montón de papel con tinta o lenguaje
de marcado?».

Ante las personas que dictaminan el curso de una editorial o incluso de
toda la industria en una región, es carencia de capacitación y falta de
recursos: «se busca persona con conocimientos de _x_ programa privativo;
del presupuesto cotiza un taller para usar _y_ paquetería de _software_
que en _z_ feria anunciaron como la panacea editorial; pide apoyos
gubernamentales o internacionales porque cada tres años _tenemos_ que
actualizar nuestro equipo de cómputo».

En el librocentrismo la dependencia tecnológica no importa, siempre y
cuando ese _software_ o _hardware_ cumplan con el cometido de publicar
la obra. Una paquetería de diseño o de oficina se vuelve omnipresente,
aunque en realidad pocos la dominan. Un conjunto de programas es
confundido con un método _único_ de producción de libros.

Y si un proyecto falla, el programa, la máquina o el personal es quien
tiene la culpa; cuando el problema es que la falta de capacitación nunca
ha sido en relación con los formatos deseados, sino la evasión de ver
que **la edición no solo es una tradición o una profesión, sino también
un método**.

Si preparas a una generación de editores o diseñadores para utilizar
programas propietarios determinados es casi seguro que una vez terminada
su formación esos conocimientos sean obsoletos ---ignoremos un poco la
diferencia entre el uso de estos programas en la escuela y en el
trabajo---.

De nueva cuenta hay que buscar capacitaciones, con la cruda realidad
donde lo que no ha cambiado es la dependencia a que alguna compañía o
institución te proveerá del _software_ y _hardware_ necesario para hacer
tu trabajo.

Pero si nos centramos en el método, en cómo publicar una obra en
diversos formatos con la mayor automatización y calidad posible, se hará
patente que lo relevante yace en los archivos de origen y en el camino
que va de ellos al multiformato.

Un formato determinado deja de tener mayor jerarquía. Los formatos dejan
de «competir» entre ellos. Los procesos de producción multiformato ya no
se perciben como ajenos. **La publicación deja de ser el objetivo: es
desechable**.

El centro se vuelca al cuidado de los archivos madres y al mantenimiento
de los procesos automatizados de publicación multiformato. Los formatos
finales, esos +++EPUB+++, +++MOBI+++ o +++PDF+++ a publicar, se producen
incesantemente y se eliminan sin el temor de haber perdido algo. No hay
merma porque el valor de la obra reside en sus orígenes, la publicación
es solo una muestra de los esfuerzos aglutinados en un objeto, que puede
reproducirse sin problemas.

Como editores, ¿cuántos proyectos yacen ahí en nuestras computadoras,
discos duros externos, nubes o repositorios que han quedado abandonados?
En la pretensión de publicar un texto hemos minado sus posibilidades al
atropellar, una y otra vez, la metodología necesaria para su gestación.

**Enseña a una generación de diseñadores y editores a concebir la
edición también como un método y no habrá _software_ o _hardware_ que
los limite.** Mejor aún, por [esa libertad](@index[17])
dejarán de estar a la espera de compañías o instituciones porque cada
quien, como persona, equipo de trabajo o editorial, será el arquitecto
de su propio método.

Un giro en la edición es necesario. Al tener conciencia clara del
método, la obra evolucionará de manera favorable y la publicación tendrá
los estándares de calidad deseados. **En el anhelo por cosechar sus
frutos, la tradición editorial ha hecho del libro un fetiche**. La
edición continua y más específicamente la edición ramificada y libre
podrían ser _uno de esos_ giros.

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «Edición como edición continua».
* Título en el _blog_ de Mariana: «La edición como edición continua».
* Fecha de publicación: 2 de julio del 2018.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/20%20-%20Edici%C3%B3n%20como%20edici%C3%B3n%20continua/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/la-edicion-como-edicion-continua).

</aside>
