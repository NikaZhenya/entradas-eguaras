# El formato de una publicación: cuello de botella en la edición

El formato es el principal punto de cuidado dentro de la edición, como lo he
mencionado en otras entradas. Su importancia es tal que **la calidad de una
publicación depende de su formato**. Sin embargo, esta relevancia ha sido
inversamente proporcional a su descuido: pocas son las personas que se preocupan
por el formato antes de empezar con la edición.

El resultado es predecible dentro del proceso editorial: las dificultades en la
producción de un libro aumentan —muchas veces de manera incontrolable— hasta
incluso volver a empezar de nuevo. No solo en este sentido **el formato es el cuello 
de botella en la edición**, sino que su mal manejo en diversas ocasiones también 
es perceptible para el lector. ¿Cuántas veces no hemos encontrado en una obra un 
error tan fundamental en la estructura?

## Calidad, control y apertura en la edición

Como punto de partida **el formato es una cuestión de cuidado editorial**. Un buen
formato nos permitiría mantener una alta calidad y control en la edición, así
como posibilitará su apertura para utilizarlo en cualquier entorno, sea una
publicación digital o impresa, sea que te guste usar *software* privativo
o libre, o sea que prefieras trabajar al *modo Adobe* o no.

Además, un formato adecuado nos permite evitar unas de las dificultades más comunes 
dentro de la edición:

* La estructura se mantiene con independencia al soporte final —EPUB o PDF, por
ejemplo—, por lo que se evita ambigüedad en los estilos, como falta de uniformidad
en encabezados, párrafos, etcétera.

* La conversión entre formatos es sencilla y sin prestarse a un descrontrol, lo
que permite poder maquetar el texto en el entorno que se prefiera, como puede
ser LaTeX, Scribus, InDesign, Sigil, por mencionar algunos.

* El tiempo de publicación en diversos soportes —EPUB, MOBI, IBOOKS, PDF para 
impresión, etcétera— deja de ser proporcional a la cantidad de archivos deseados,
incluso sus tiempos de producción disminuyen a segundos, 
[como se demostró](http://marianaeguaras.com/como-generar-4-formatos-de-un-contenido-en-un-dia/)
al editar el artículo «[Historia de la edición digital](http://marianaeguaras.com/historia-de-la-edicion-digital/)».

* El contenido se conserva a lo largo del tiempo, sin importar el cambio de tendencias
o que el *software* utilizado para hacer la publicación deje de tener soporte,
evitándose ese dolor de cabeza —y derroche de recursos— de tener que *recuperar*
información o, peor aún, tenerla que *rehacer*, como a muchas personas les ha
pasado cuando han tenido que obtener información de archivos de PageMaker o
QuarkXPress para usarlo en InDesign.

Pero ¿qué es un «formato adecuado»? Los requisitos mínimos para un formato óptimo 
son que

1. esté en un [lenguaje de marcado](https://es.wikipedia.org/wiki/Lenguaje_de_marcado);
2. el formateo sea previo a la edición o por lo menos antes de crear el soporte final;
3. se opte por un formato abierto y estandarizado, y que
4. tenga un mecanismo de control de versiones, para evitar sobrescrituras indeseadas.

Con estos elementos podemos acercarnos un poco más al ideal de una publicación de
alta calidad, ya que cabe resaltar que **el cuidado en la edición no se reduce
al cuidado en el formato**. No obstante, un buen formato nos ayudará a solventar
muchos de los errores que por lo general y de manera casi mágica aparecen cuando
la obra ya fue publicada; por ejemplo, dedazos donde antes no los había.

El formato también nos permitirá un alto control ya que al existir una estructura
uniforme —donde es posible identificar los distintos niveles de encabezados, párrafos,
bloques de cita, itálicas, negritás y más—, los cambios necesarios pueden hacerse
sin muchos desvaríos e incluso de manera automatizada, por lo que la migración
de un entorno a otro no es complicado.

No menos importante, un formato con estas características está pensado para la
longevidad. Un formato abierto y estandarizado permite que a pesar de los constantes
cambios en las tecnologías de la información o en el mundo de la edición, el contenido
y los metadatos de nuestra obra puedan ser reutilizados según las pautas editoriales
del momento.

Parecen obviedades pero el vaivén de los profesionales de la edición en cuanto 
adopción tecnológica ha evidenciado que en general **el sector editorial ha sido 
irresponsable al momento de tratar los contenidos editoriales**. Si la edición es 
el cuidado de la obra para su publicación, el formato es el principal elemento que 
definirá la calidad en la edición y su tiempo de producción.

## Del WYSIWYG al WYSIWYM

Un trato adecuado en el formato exige un cambio de enfoque al momento de tratar
el texto. Por tradición la edición tenía una dimensión visual. Tanto el editor, 
como el tipógrafo, el formador o el diseñador cuidaban del contenido acorde a lo 
que era visible. Una vez concluido su trabajo, se publicaba el libro y el lector 
se encargaba de juzgar el libro.

Esta tradición en el contexto digital no se ha perdido y lo más importante, *no
ha de abandonarse*. Sin embargo **actualmente existen al menos tres dimensiones
dentro de una publicación**, donde dos son necesarias para cualquier soporte:

1. Dimensión visual o de diseño: la capa perceptible para el público en general
cuya importancia reside en la legibilidad.
2. Dimensión estructural o de formato: la capa oculta que da cohesión al diseño,
por lo que es relevante para mantener el control en la edición.
3. Dimensión funcional o de programación: la capa que permite cambios dinámicos 
en el texto, la cual es propia de las publicaciones digitales y posibilita otras
experiencias de lectura.

**Lo ideal es que la dimensión estructural determine al menos las pautas mínimas de 
diseño**. Al marcar dónde hay encabezados, párrafos, bloques o epígrafes, es posible 
aplicar estilos acorde al tipo de contenido.

Sin embargo, **en el modo habitual de crear una publicación se va de la dimensión
visual para después determinar la dimensión estructural**. Si el diseño no es uniforme,
habrá errores de estructura que se heredarán a futuras ediciones o a otros soportes.
Pero eso no es lo más grave. Este modo de trabajo implica que quien crea la estructura
no es el editor, sino su programa de edición que, como es de esperarse, carece
de criterios editoriales al momento de *automáticamente* crear la estructura a
partir del diseño.

Esta predilección al diseño es la característica del enfoque WYSIWYG («lo que ves 
es lo que obtienes»). El enfoque que da preferencia a la estructura es el WYSIWYM 
(«lo que ves es lo que quieres decir»). Los procesadores de texto, como Word, y
los programas de maquetación, como InDesign, apuestan por el enfoque WYSIWYG.
La adopción de este modelo se debe a que es más fácil de aprender y porque le 
da mayor libertad al usuario. 

Sin embargo, en un ambiente donde la calidad del formato es esencial, el enfoque 
WYSIWYM sobresale por establecer pautas de control que muchas veces se traduce a 
una curva de aprendizaje más larga. Aunque aprender este enfoque tome más tiempo, 
en un mediano o largo plazo implicará un ahorro de tiempo, la evasión de erratas y, 
principalmente, un aumento en la calidad de las publicaciones.

## Formatos recomendados

De manera concreta, **¿cuáles son los formatos siguen el enfoque WYSIWYM y al mismo 
tiempo tienen las características de optimización que buscamos?** A continuación 
se hace un repaso somero sobre las posibilidades que contamos para mejorar el 
formato de nuestras publicaciones.

### Markdown para el contenido

Los lenguajes de marcado ligero son la primera opción para quienes empiezan a
trabajar con el enfoque WYSIWYM. Esta clase de lenguajes sobresalen porque son
fáciles de escribir y de leer, así como permiten su conversión sin problemas a 
otros formatos.

Un gran ejemplo de este tipo de lenguajes es Markdown. Con este formato podemos
empezar a marcar el contenido de nuestra publicación desde el mismo momento que
leemos sobre su sintaxis básica. Un ejemplo es el siguiente:

```
# Encabezado 1

Esto es un párrafo con una *itálica*.

## Escabezado 2

> Esto es un bloque de cita con una **negrita**.
```

La vesatilidad de este formato ha ayudado a que prescinda de procesadores de texto 
al momento de tener que redactar un documento, como son [las entradas](https://github.com/NikaZhenya/entradas-eguaras)
que publico aquí. Como es de esperarse, Mariana prefiere otros formatos para editarlos,
lo cual no es ningún problema porque gracias a [Pandoc](http://pandoc.org/) puedo 
generarle un documento de Word o cualquiera que sea de su preferencia.

Pero esta agilidad tiene un precio: no es posible dar formato a estructuras complejas,
al menos no con la sintaxis básica de Markdown. Ejemplos de estas estructuras
podrían ser epígrafes o párrafos franceses.

### HTML para el contenido

Otro formato que nos permite abordar estructuras más complejas es HTML. Su flexibilidad
es tal que es el formato empleado para los libros electrónicos estándar. Si bien
su estructura no es tan cómoda de leer, nos permite crear estructuras como la 
siguiente:

```
<h1>Encabezado 1</h1>

<p class="epigrafe">Esto es un epígrafe</p>

<p>Esto es un párrafo con una <em>itálica.</em></p>

<h2>Encabezado 2</h2>

<blockquote>Esto es un bloque de cita con una <strong>negrita</strong>.</blockquote>

<p class="frances">Esto es un párrafo francés.</p>
```

Con el atributo de `class` podemos definir estilos particulares que luego pueden
establecerse en el diseño. Pero una publicación no solo es su contenido, sino
también sus metadatos, lo cual no se soluciona de manera satisfactoria con
Markdown o HTML.

### YAML o JSON para los metadatos

Como Mariana lo mencionó en [otra entrada](http://marianaeguaras.com/que-son-los-metadatos-de-un-libro-y-cual-es-su-importancia/),
los metadatos permiten la catalogación de una obra. Alguna vez alguien en un taller
que impartimos resumió los metadatos como el acta de nacimiento de un libro. Pienso
que es una buena definición, ya que sin metadatos no es posible identificar una
publicación. Por esto no solo son importantes para los soportes digitales, sino
también para los impresos, porque solo así es posible dar con ellos más allá de
un encuentro fortuito en una librería.

Para un uso adecuado de los metadatos mi recomendación es [YAML](https://es.wikipedia.org/wiki/YAML)
o [JSON](https://es.wikipedia.org/wiki/JSON). YAML es un formato muy flexible
y fácil de usar, aunque puede fomentar una pérdida de control. En cambio, JSON es 
un formato que requiere de una mayor curva de aprendizaje pero con el fin de mantener 
un orden.

Un ejemplo de metadatos en YAML es:

```
---
# Generales
title: Sexo chilango
author: Braun, Mónica
publisher: Nieve de Chamoy
synopsis: Esta edición reúne todas las columnas…
category: Ficción, Narrativa
version: 2.0.0
```

Como se observa, su lectura y escritura es muy sencilla. Este es el motivo por el
que en Perro Triste [hemos optado por este formato](https://github.com/ColectivoPerroTriste/Herramientas/tree/master/EPUB/YAML)
para el manejo de los metadatos.

El mismo ejemplo en JSON nos da:

```
{
  "title": "Sexo chilango",
  "author": "Braun, Mónica",
  "publisher": "Nieve de Chamoy",
  "synopsis": "Esta edición reúne todas las columnas…",
  "category": "Ficción, Narrativa",
  "version": "2.0.0"
}
```

La estructura no es tan complicada pero, debido a su sintaxis, la lectura y 
escritura se vuelve muy accidentada. Además, habrá quien no le agrade manejar 
archivos distintos para el contenido y los metadatos, por lo que existe otra 
opción.

### XML para el contenido y metadatos

El formato XML es un veterano en cuanto al cuidado de los contenidos. Su estructura
está pensada para ser fácil de usar entre computadoras por lo que no es cómodo de 
leer o escribir. No obstante, su sintaxis no difiere mucho a la del HTML, como vemos 
en este ejemplo:

```
<?xml version="1.0" encoding="UTF-8" ?>
<publication>
    <metadata>
        <title>Sexo chilango</title>
        <author>Braun, Mónica</author>
        <publisher>Nieve de Chamoy</publisher>
        <synopsis>Esta edición reúne todas las columnas…</synopsis>
        <category>Ficción, Narrativa</category>
        <version>2.0.0</version>
    </metadata>
    <content>
        <h1>Encabezado 1</h1>
        <epigraph>Esto es un epígrafe</epigraph>
        <p>Esto es un párrafo con una <em>itálica.</em></p>
        <h2>Encabezado 2</h2>
        <blockquote>Esto es un bloque de cita con una <strong>negrita</strong>.</blockquote>
        <hanging>Esto es un párrafo francés.</hanging>
    </content>
</publication>
```

Debido a que el XML es extendible —la posibilidad de poder crear nuevas etiquetas—, 
permite la incorporación de estructuras complejas que involucren tanto los contenidos 
como los metadatos de una publicación. 

Un gran ejemplo del uso de XML lo tenemos en los artículos académicos. Journal 
Article Tag Suite (JATS) es el formato XML estándar para muchos repositorios de 
artículos académicos como [SciELO](http://www.scielo.org.mx/). Con esto se hace 
posible la creación de múltiples soportes de lectura al mismo tiempo que permite 
un uso flexible y longevo de la publicación. 

Otro ejemplo lo tenemos en la posiblidad de usar este formato para trabajar con 
InDesign como ya se había explicado en [otra entrada](http://marianaeguaras.com/de-xml-a-epub-con-indesign/).

## El fomato y nuestra tradición cultural

La sección pasada puede dejarnos la sensación que el formato se reduce a una
cuestión técnica en pos de un mejor cuidado editorial. No obstante, esto es solo
el inicio cuyo punto de llegada es que **el formato es una cuestión cultural**.
La conservación y prolongación de nuestra herencia cultural ya es indisociable
al mantenimiento de la infraestructura digital desde la cual creamos contenidos
culturales, como son las publicaciones.

Desde una perspectiva individual, el formato toma relevancia cuando por la migración
de *software* o de sistema operativo nos vemos imposibilitados de abrir algún
archivo que necesitamos. En un grupo de trabajo el formato adquiere importancia 
cuando entre los mismos compañeros les es imposible realizar una tarea ya que 
cierto archivo no es compatible o su formato es quebrado al usarlo en otra computadora. 
En una editorial el formato es significativo cuando es difícil recuperar la información 
almacenada en archivos privativos antiguos.

Si continuamos escalando las dificultades surgidas a partir de un mal manejo en
el formato de los documentos, tenemos casos alarmantes en donde parte de nuestra
herencia cultural queda «secuestrada». Es decir, la información está «ahí» pero
no es accesible y su recuperación es incosteable para diversas instituciones.
Casos lamentables de este problema lo podemos ver en archivos, bibliotecas y 
repositorios públicos cuyas consultas quedan obstaculizadas por un «mero
problema técnico».

En el trabajo del día a día el formato es una cuestión técnica que por lo general
se opta por la manera más sencilla de llevarlo a cabo, sin consideración alguna
sobre lo que esto puede implicar para el futuro. Sin embargo, en una dimensión
más general y a largo plazo, la calidad y apertura en el formato determinará qué
tan accesible y flexible será su uso para los siguientes años o décadas.

Dado la complejidad del asunto, **es comprensible que el sector editorial esté
harto del *software* milagro**, de aquellas «novedades» tecnológicas que supuestamente
facilitan su trabajo pero que al final terminan por complicarlo. A nadie le agrada
aprender a usar programas una y otra vez, mucho menos recuperar la información para
tener posibilidad de usarla.

En este sentido, quizá **el sector editorial en general debería empezar a plantearse
con seriedad la manera de manejar los contenidos** con una perspectiva a largo plazo
y con independencia al *software* en voga. Tal vez así sea posible mejorar la
calidad editorial sin el temor de siempre volver a empezar de nuevo…
