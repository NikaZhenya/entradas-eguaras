# Edición cíclica y edición ramificada

## Edición cíclica

### Antesala a la «revolución» digital

La edición de material impreso nunca ha sido un proceso sencillo. Desde los
amanuenses hasta la imprenta se han requerido diversas habilidades para
poder cumplir con cada uno de los procesos. *Por lo regular* en la 
publicación de una obra es necesario:

1. Editar y corregir.
2. Diseñar y maquetar.
3. Cotejar y revisar.
4. Imprimir.
5. Distribuir.
6. Comercializar y administrar.

Antes de la «revolución» digital, la minuciosidad puesta en cada proceso
marcaba el ritmo en el flujo de la edición. Si se descuidaba un elemento,
con seguridad la obra perdería calidad o tendría que empezarse de nuevo.
Por este motivo, **La edición tradicional iba paso a paso, tratando
de regresar lo menos posible**.

Este andar también iba al ritmo de la infraestructura, los
recursos y la demanda existente. Antes de la imprenta el flujo era
consistente y con un «lento» crecimiento. Con la llegada de la impresión 
la edición se aceleró, los costos decrecieron y la demanda creció. El libro 
pasó de ser un artículo poco asequible a ser un elemento de uso diario. 
Podemos decir que la imprenta no solo fue una «revolución» sino también 
una «apertura» de la información. 

El traslado a lo mecánico permitió que los oficios relacionados a la edición 
se especializaran a través de los siglos. Si bien las técnicas se fueron 
afinando o cambiando hasta mediados del siglo XX, la concepción metodológica 
permaneció invariable: **la edición era una progresión unilateral hasta la 
publicación y comercialización de la obra**.

### En la «revolución» digital

El uso de tecnologías análogas implicaba que solo unos pocos podían
solventar algún revés. Si una publicación salía con defectos, no quedaba más
que la adición de una fe de erratas o, en el peor de los casos, la bancarrota.

A partir de la segunda mitad del siglo XX esta condición empezó a 
flexibilizarse. La masificación de las computadoras abrió la puerta para 
facilitar la regresión en los procesos, al permitir la modificación del 
documento a diestra y siniestra hasta incluso durante su impresión.

La publicidad durante el surgimiento digital era clara: *la máquina de 
escribir en la oficina* siempre dejaba rastro en la corrección.
Este fue el primer frente en el que las computadoras personales buscaron un 
mercado, pero fue cuestión de tiempo para observar otras posibilidades:

* las computadoras en lugar de *la máquina de escribir en el hogar*,
para la redacción de documentos personales o domésticos;
* la teconología digital para «modernizar» tanto la escritura como
la impresión de documentos.

Las computadoras dividieron al mundo editorial. La confrontación no fue 
fortuita: el estado de la tecnología digital representaba un revés en la
calidad editorial alcanzada a través de siglos. Pero paulatinamente 
se fue puliendo (¿o ignorando?) la calidad de la edición digital gracias a [TeX](https://en.wikipedia.org/wiki/TeX) 
y el DTP ([*desktop publishing*](https://en.wikipedia.org/wiki/Desktop_publishing)).

Algunos elementos de la tradición editorial fueron rezagados. Sin embargo,
**la concepción metodológica de una progresión unilateral permaneció 
inalterable**. La diferencia estribó en que la tecnología digital 
permitió que el retorno en los procesos se hicieran por medio de un
teclado y un ratón. Al menos así fue como las compañías de *software* y 
*hardware* publicitaron la apuesta por la «modernización» en la edición
de documentos.

En los noventa el DTP como sinónimo de la edición estaba en proceso de 
consolidación. La edición era ya edición digital. De Corel Ventura, 
pasando por PageMaker y QuarkXPress, hasta InDesign, la publicación
de *impresos* se ha vuelto un proceso cada vez más centralizado en un 
paquete de *software*.

Junto a la «revolución» digital en los procesos de edición, también vino 
otra «apertura» en la información: el libro ya dejaría ser sinónimo de impreso. 
El sueño de un libro sin páginas se fue concretando en un archivo digital. 
Del papel al *byte*, los libros electrónicos surgieron como una alternativa.

El recelo ante otros soportes distintos al impreso aún sigue siendo una 
actitud muy común entre los editores. De nuevo la discusión se centra
en la calidad del soporte: todavía en la actualidad el *libro electrónico*
carece de los estándares de calidad *según el parámetro* del impreso.
En lugar de apoyar al mejoramiento de los formatos digitales, **el grueso
del mundo editorial ha decidido apostar por la cautela y la espera de que
agentes externos mejoren la calidad editorial del *ebook***.

Varios editores han decidido abrir la puerta al libro electrónico, 
en muchos casos más como una cuestión de «adaptación a los tiempos» que 
un sincero anhelo por diversificar los soportes de una obra. La discusión 
se traslada a una cuestión de costos: producir más soportes implica una 
inversión proporcional de tiempo y recursos según la cantidad de formatos 
«adicionales».

Para lidiar con la necesidad de múltiples soportes, se emplea una metodología 
donde primero se termina la publicación del impreso para después pasar a la 
elaboración de otros formatos. Esto no solo provoca un aumento en el tiempo 
de publicación según la cantidad de soportes, sino que al mismo tiempo alimenta 
la idea de que los procesos de producción de un libro electrónico es de 
distinta naturaleza a la producción de un impreso. Asimismo se ignora que en 
esta sucesión se heredan tanto contenidos como erratas y elementos 
innecesarios para el resto de los soportes.

La progresión unilateral y su posibilidad de regresión, característica de
la edición tradicional, muta en una serie de ciclos que brinca de un 
soporte a otro. **La metodología cíclica es la pretensión de mantener un 
desarrollo unilateral cuando el objetivo es la consecusión de diversos 
soportes**. El cambio de método queda invisibilizado al entenderse cada
nuevo ciclo de producción como *procesos adicionales* a la publicación
del impreso, que comúnmente se considera el soporte «nuclear» del resto 
de los formatos.

La sensibilización de los editores de que los procesos necesarios para
la publicación de otros soportes distintos al impreso no son ni 
*complementarios* ni *distintos* es y siempre ha sido el meollo en la
discusión sobre el supuesto antagonismo entre las publicaciones impresas
y las electrónicas. Más que una cuestión de calidad o de costos, el
escepticismo del mundo editorial gira en torno 1) al carácter mítico
de la inmutabilidad metodológica y 2) que existe la necesidad de 
una adopción conciente de un método que no sea unilateral.

Mientras se continúe pensando que los ciclos son soluciones *ad hoc*, 
en lugar de concebirse como un método, la mayoría de los editores solo 
entenderán la «revolución» digital como una «revolución» en soportes y
técnicas, y no como algo más profundo: un cambio y «apertura» en la
forma de «publicar» cultura.

Por estos motivos, aunque al parecer exista una continuidad, **la edición
cíclica es metodológicamente distinta a la edición tradicional**. La
edición cíclica aborda el desafío traído por el multiformato como una
presunta continuidad metodológica de la edición tradicional, pero
añadiendo ciertos «módulos» para la producción de otros formatos.

La edición cíclica se concentra en producir varios soportes al mismo tiempo 
que quiere conservar la idea que la publicación de un libro es un *solo*
camino a seguir. Esta concepción metodológica está tan arraigada que el 
«*software* milagro» da tranquilidad al concebir la conversión entre formatos 
como la solución final. Solo cuando se percibe la pérdida en la calidad 
editorial y técnica se hace patente que la edición cíclica no es el método 
más óptimo para la producción multiformato…
