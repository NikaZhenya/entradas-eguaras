# 17. Lo gratuito, lo abierto y lo libre

En el quehacer cultural —como ya sucedió en el desarrollo de _software_—
es cada vez más común escuchar sobre el acceso abierto y la cultura
libre. Pero **¿qué es eso de lo «abierto» y de lo «libre»?**

## «Bájalo, es gratis»

Uno de los primeros acercamientos hacia los productos o servicios
abiertos o libres es que son «gratuitos». El usuario, de repente,
se percata que no hay nada que lo restrinja en la descarga o uso de
algún programa o contenido, llegando incluso al acaparamiento digital
(_e-hoarding_).

La gratuidad puede ser por infracción de algún tipo de propiedad intelectual
—como los derechos de autor o de patentes— aunque también se da porque
el autor decidió liberar el contenido de esa manera.

Alphabet Inc. es un buen ejemplo de una compañía que ofrece a sus
usuarios varias aplicaciones de manera gratuita; por ejemplo, Chrome,
Gmail y el motor de búsqueda Google.

Facebook es otro ejemplo, así como Twitter, Airbnb, Uber, etcétera.
Para el caso editorial, tenemos el [catálogo mexicano de libros de
texto gratuito](http://libros.conaliteg.gob.mx/content/common/consulta-libros-gb/),
los diversos diccionarios de la +++RAE+++ o las consultas en Fundéu +++BBVA+++.

Aunque no sea posible generalizar, en la mayoría de los casos **el
usuario termina dependiendo de varios de estos servicios «gratuitos»**:
el explorador para navegar por internet; el gestor de correo para
el trabajo; el motor de búsqueda para encontrar información; las redes
sociales para estar en contacto con familiares, amigos o compañeros
de trabajo; los servicios de geolocalización para trasladarse, etcétera.

Algunos usuarios muestran su preocupación por este carácter de dependencia,
ya que, más allá de lo que se ofrece, no es posible saber qué tanto
se están utilizando nuestros datos personales para otros fines —lo
que se conoce como [minería de datos](https://es.wikipedia.org/wiki/Miner%C3%ADa_de_datos)—
o qué medidas de seguridad se están llevando a cabo para su protección.

El **caso de Cambridge Analytica y Facebook** son los más recientes
en este aspecto, pero no son el único, como puede verse en las diversas
demandas hechas a Alphabet Inc. por acaparamiento de datos del usuario
o a Uber por su fuga de información.

En este sentido puede decirse que **lo gratuito no es siempre sinónimo
de apertura o libertad**, ya que por lo general el interés detrás
de varios productos o servicios gratuitos es la obtención de datos
del usuario con procedimientos poco claros y sin que este tenga mucho
margen de maniobra.

## Abriendo caminos

Aunque la privacidad digital es un debate público, este tema ha estado
desde los inicios de la «era» digital. Durante los noventa y el surgimiento
de las empresas puntocom varias personas señalaron los riesgos que
el usuario podía tener por compartir su información.

Por un lado, hay compañías que controlan el flujo de la información,
muchas veces de manera innecesaria. Por el otro, se cuenta con una
infraestructura tecnológica para satisfacer ese fin que sobresale
por su falta de claridad en su funcionamiento, al mismo tiempo que
está fuertemente protegida para evitar que esta sea examinada.

La **iniciativa del código abierto** surgió para contrarrestar este
problema, ya que apuesta por:

* Abrir el código para que cualquiera pueda examinarlo y mejorarlo,
  posibilitando así la rápida identificación de problemas de seguridad.
* Disminuir las barreras legales que muchas veces impiden crear o
  mejorar tecnologías.

Esto implica un cambio en las metodologías de producción de _software_,
pero también de las formas de organización y las posturas en torno
a cómo se percibe la construcción del conocimiento.

Por estos motivos, a finales de los noventa, la iniciativa del código
abierto fue expandiéndose hacia otros ámbitos de la producción cultural,
y así surgieron los **movimientos de la «cultura libre» y el «acceso
abierto»**.

Con el código abierto los usuarios tienen la seguridad de que un producto
o servicio, muchas veces gratuito, no tiene nada oculto en detrimento
del usuario. Como ejemplos tenemos a [Firefox](https://www.mozilla.org/es-ES/firefox/),
[Thunderbird](https://es.wikipedia.org/wiki/Mozilla_Thunderbird),
[DuckDuckGo](https://es.wikipedia.org/wiki/DuckDuckGo), [Telegram](https://es.wikipedia.org/wiki/Telegram_Messenger)
u [OpenStreetMap](https://es.wikipedia.org/wiki/OpenStreetMap).

Para el caso del acceso abierto y la cultura libre los diversos productos
ofrecidos dejan claro que el usuario no está violando ningún tipo
de propiedad intelectual —siempre y cuando se respete la licencia
de uso—, así como se le ofrece, «tal cual».

Es decir, el usuario no está condicionado a dar su información personal
a cambio del producto; sin embargo, el autor no tiene la obligación
de ofrecerle «servicio al cliente». Ejemplos de esto son [Internet
Archive](https://archive.org/) o SciELO
que, a su vez, protegen su información con algún tipo de licencia
[Creative Commons](@index[14]).

Y aunque **la apertura deja claro que la prioridad no es tanto la
gratuidad de lo que se ofrece, sino el acceso y la privacidad de quien
lo usa**, en algunas ocasiones esto es insuficiente. ¿Qué tal si como
usuario o autor tu interés no es solo el acceso o la privacidad, sino
también la «libertad»?

> *Lecturas recomendadas*: «[Vender vino sin botellas](https://biblioweb.sindominio.net/telematica/barlow.html)»
> de John Perry Barlow, cofundador de la [Electronic Frontier Foundation](https://es.wikipedia.org/wiki/Electronic_Frontier_Foundation),
> y «[La Catedral y el Bazar](https://biblioweb.sindominio.net/telematica/catedral.html)»
> de Eric S. Raymond, «padre» de la iniciativa del código abierto.

## La ambigua libertad

Mucho antes de la iniciativa del código abierto surgió el movimiento
del _software_ libre —de hecho, el código abierto es una bifurcación
de la comunidad del _software_ libre—, que percibe a la libertad desde
cuatro puntos:

1. Libertad de ejecutar un programa.
2. Libertad de estudiar y modificar un programa.
3. Libertad de redistribuir un programa.
4. Libertad de mejorar y publicar las mejoras de un programa.

Para asegurar que nunca se rompan algunas de estas libertades, el
_software_ libre viene acompañado de licencias que garantizan que,
sin importar el rumbo que tome, todas las versiones tendrán que ser
compartidas de la misma manera.

Una de las ventajas que se obtiene con esto es que **nadie tiene la
necesidad de pedir permiso para ejercer alguna de estas libertades**,
así como el autor cuenta con la seguridad de que su producto siempre
permanecerá libre.

Una de las principales críticas que el movimiento del _software_ libre
ha hecho al código abierto es que, en algunos de los casos, sus licencias
de uso son demasiado restrictivas, ya que impiden la modificación
o la redistribución de un programa. Además, también ha criticado el
«miedo» hacia el uso de la palabra «libertad».

Por otro lado, algunas personas dentro de la iniciativa del código
abierto critican al _software_ libre por crear licencias «víricas»;
es decir, licencias que *impiden la libertad* de compartir un programa
con una licencia distinta, creando así una paradoja. Además, también
se ha argumentado el fuerte carácter normativo y la ambigüedad en
el uso del término «libertad».

Aunque **en la práctica lo abierto casi siempre es libre y viceversa,
existen diferencias con un trasfondo político y filosófico** que se
hacen perceptibles en esta oposición.

En la iniciativa del código abierto casi nada interesa más allá de
la esfera de organización y el acceso a la información: se enfoca
en el modo de producción y su transparencia.

Mientras tanto, el movimiento del _software_ libre pretende ser más
integral y situar al desarrollo del _software_ en un contexto cultural
y social más amplio, incluso desde una perspectiva de «ética kantiana»
—guiño al imperativo categórico—: obra de tal manera que se convierta
en ley universal.

## «Si nada de eso me interesa, ¿qué ventaja tiene el _software_ libre?»

De manera general, el _software_ libre evidencia que el acceso a la
información y la privacidad del usuario no es reducible al modo en
cómo se desarrolló la tecnología, sino que también abarca los supuestos
básicos que sirven de guía para este desarrollo y cómo estos nos afectan
como comunidad o como cultura.

En el caso del quehacer cultural, **la libertad en el uso, modificación
o mejora permite un acceso irrestricto y seguro a los contenidos**.
No es lo mismo tener acceso al +++PDF+++, al +++MP4+++ o al +++MP3+++ para hacer una
reedición o _remix_, que a cada uno de los archivos editables.

Tampoco es lo mismo decir que tu producto es abierto, al mismo tiempo
que restringes ciertos usos, que decir que es libre. Ni es lo mismo
abrir tu material por moda o porque es más redituable, que liberarlo
por el afán —sino que mera necedad— de percibir el valor no monetario
de la cultura.

¿Ambigüedad? Sin duda. ¿Utopía? Tal vez. Aún falta mucho por discutir
sobre lo libre y más cuando en el sector editorial se empieza a ver
en el horizonte no el acceso abierto, sino la **«edición continua
y libre»**…

> *Lecturas recomendadas*: «[El manifiesto de +++GNU+++](https://www.gnu.org/gnu/manifesto.es.html)»
> y «[Por qué el “código abierto” pierde de vista lo esencial del software
> libre](https://www.gnu.org/philosophy/open-source-misses-the-point.es.html)»
> de la Free Software Foundation.

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «Lo gratuito, lo abierto y lo libre».
* Título en el _blog_ de Mariana: «Lo gratuito, lo abierto y lo libre (o la gratuidad, el acceso abierto y la cultura libre)».
* Fecha de publicación: 27 de marzo del 2018.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/17%20-%20Lo%20gratuito,%20lo%20abierto%20y%20lo%20libre/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/lo-gratuito-lo-abierto-y-lo-libre-o-la-gratuidad-el-acceso-abierto-y-la-cultura-libre/).

</aside>
