# De XML a EPUB con InDesign

Como lo prometido es deuda, en la entrada «[Edición digital como edición desde
cero](http://marianaeguaras.com/edicion-digital-como-edicion-desde-cero/)» se
mencionó que se verían con mayor detenimiento tres de los métodos que se
compararon al momento de crear un EPUB de
[la edición del Proyecto Gutenberg del *Don Quijote*](https://www.gutenberg.org/ebooks/2000).
Para descansar *un poco* de las nociones relacionadas al código, el primer método
que veremos para hacer un EPUB será utilizando InDesign.

El desarrollo de EPUB con este *software* puede hacerse de muchas maneras.
Sin embargo, aquí nos enfocaremos en el proceso que ofrece más control, menos
tiempo y mejor calidad. A partir de un
[XML](https://es.wikipedia.org/wiki/Extensible_Markup_Language) y una hoja de
estilo [CSS](https://es.wikipedia.org/wiki/Hoja_de_estilos_en_cascada) crearemos
un EPUB de una manera muy sencilla, al contrario a lo que se podría pensar en
un principio.

## 1. Preparación de los archivos

Antes de abrir InDesign necesitamos tres archivos para nuestro EPUB: el XML, el
CSS y la portada. Solo el XML es necesario ya que en este vendrá todo el
contenido de nuestro libro. Más adelante explicaremos las particularidades de
este formato.

La portada puede ser en cualquier formato de imagen y de cualquier tamaño. En
mi opinión lo más recomendado es que esté en JPG, ya que este formato
tiende a pesar menos. En cuanto al tamaño, en Nieve de Chamoy usamos
una altura no mayor de 2048
pixeles por el único hecho de que en el iPad la portada se muestra en pantalla
completa por algunos segundos, tiempo suficiente para que el usuario note algún
pixeleo en la imagen. Si no se piensa distribuir para iOS, una altura no mayor
a 1024 pixeles tendría que ser suficiente.

Estas pequeñas particularidades de la
portada es un detalle importante, ya que por lo general su peso puede
representar entre el 25 y 50 por ciento del tamaño final del EPUB. Además,
recuérdese que, entre más ligero el libro, el costo de envío con Amazon será
menor (véase el apartado C de
[este enlace](https://kdp.amazon.com/help?topicId=A29FL26OKE7R7B)), o bien, la
descarga será más rápida y ocupará menos espacio en el dispositivo del usuario.

Si no se desea agregar una imagen, InDesign puede generar una portada a partir
de la primera página de nuestro libro o simplemente no añadirla. Esta
última opción se desaconseja, ya que por lo general la vista por defecto de la
estantería de los *ereaders* solo muestra la portada del libro...

La hoja de estilos CSS no es necesaria, sin embargo, su implementación nos
permite un mejor y mayor control en el diseño de nuestro EPUB. Si como editores
desconocen por completo este lenguaje de marcado, la recomendación sería que un
diseñador *web* les genere una plantilla que puedan utilizar en varios de sus libros.

Si no es posible la ayuda de un diseñador *web* sin ningún problema pueden
utilizar la plantilla usada para este EPUB, la cual es cortesía de Perro Triste.
El CSS puede verse
[aquí](https://github.com/ColectivoPerroTriste/Tapancazo/blob/master/taller/EPUB-MUESTRA/OEBPS/css/principal.css)
o descargarse mediante el enlace que está al final de este artículo.

El archivo XML es un formato que contiene una serie de etiquetas dentro
de las cuales se agrega el contenido de manera semántica. Es decir, si en
nuestro libro tenemos un párrafo, este estaría rodeado de unas etiquetas `<p>`,
por poner un ejemplo. Para generar este archivo existen dos medios generales:
convirtiendo el archivo de texto original, por lo general un `.doc` o `.docx`,
a XML o etiquetar directamente el texto que ya está presente en InDesign.

Esta última opción se desaconseja si el libro solo tendrá una salida para EPUB, ya
que por lo general implica una pérdida de control sobre el contenido semántico
del libro. Para agregar el texto ya existente a una estructura XML primero hay
que ir a `Ver > Estructura > Mostrar estructura`.

<br />![Viendo la estructura XML](recursos/imagen01.png)<br />

Con esto habilitaremos la visualización de la estructura XML del documento.
Para por fin agregar el texto es necesario ir al menú de esta nueva ventana
y hacer clic en `Añadir elementos no etiquetados`.

<br />![Añadiendo elementos no etiquetados](recursos/imagen02.png)<br />

Estructurar de esta forma puede llevarnos minutos o incluso horas. Para
mayor información al respecto lo mejor es comenzar con el sitio oficial
de soporte de Adobe. En
[este artículo](https://helpx.adobe.com/indesign/using/xml.html)
explica este proceso de manera muy completa.

En este ejercicio nosotros estaremos importando un archivo XML, en lugar de
generar su estructura desde InDesign. El motivo de ello es que previamente
a través de un «archivo madre» la obra es vaciada a etiquetas para después
convertirla según las herramietnas que se emplearán para crear el libro. El
«archivo madre» tiende a convertirse en alguno de estos formatos: HTML o XHTML
para EPUB «desde cero» o con Sigil, TeX para PDF a través de LaTeX o XML para
InDesign, independientemente de si se trate de un impreso (PDF) o un *ebook*
(EPUB). Este flujo de trabajo supone que:

1. una obra tendrá distintos formatos como pueden ser EPUB, PDF, MOBI o en
línea, que

2. el editor es el único responsable de asignar semánticamente el contenido,
evitando que este trabajo sea realizado por el desarrollador o diseñador que,
por lo general, no tienen un amplio conocimiento sobre la obra y que

3. InDesign solo forma una parte del proceso de producción del libro en
lugar de ser el único flujo de trabajo para la edición digital.

Esta metodología se conoce como *single source publishing*, para una
descripción más extensa recomendamos el artículo
«[Historia de la edición digital](http://marianaeguaras.com/historia-de-la-edicion-digital/)»
publicado en este sitio.

Una vez que el «archivo madre» se convierte a formato HTML, la generación del
XML es muy sencilla. Al HTML solo se le tiene que eliminar todo el código que
no forme parte del `body` así como guardar este archivo con la extensión `.xml`.
Si existen etiquetas con alguna clase se recomienda cambiar el nombre
de la etiqueta por el nombre de la clase, para así tener la posibilidad de
distinguirla una vez que estemos en InDesign. Por ejemplo, si en el HTML
tenemos `<p class="pre">Una párrafo con clase «pre»</p>`, habría que cambiarlo
por `<pre>Un párrafo con clase «pre»</pre>`.

Una vez hechas estas modificaciones, las cuales pueden automatizarse mediante
buscar todo, [RegEx](https://es.wikipedia.org/wiki/Expresi%C3%B3n_regular) o
reemplazar todo, la estructura será semejante a la imagen que se muestra a
continuación en donde los puntos suspensivos representan más etiquetas que no
se muestran por cuestiones de espacio.

<br />![Estructura del archivo XML](recursos/imagen03.png)<br />

Con esto ya tenemos listo un archivo que podremos importar a InDesign.

## 2. Importación del XML

Si se generó la estructura a partir de un texto ya existente en InDesign, como
se mencionó en el punto anterior, este paso no es necesario, porque ya se
habrá creado la estructura, aunque aún falte darle el orden deseado.
Si no fue así y solo se desea crear un EPUB, lo más recomendado es partir de
un documento nuevo de InDesign cuyas características son indiferentes. Como sea,
para incorporar el XML es necesario ir a `Archivo > Importar XML...`.

<br />![Importación del archivo XML](recursos/imagen04.png)<br />

La ventana que se abre nos da dos opciones: `Combinar contenido` o `Anexar
contenido`. Cuando se trata de la primera importación cualquiera de las opciones
es indiferente. Sin embargo, si previamente existe una estructura XML la primera
opción nos sustiturá el contenido (ideal cuando se quiere agregar una versión
más reciente), mientras que la segunda lo añadirá al final (ideal si la obra
no está en un solo archivo XML). Por último, seleccionamos nuestro contenido
sin necesidad de mostrar las opciones de importación, ya que esta opción es solo
para usuarios avanzados.

De esta manera se importará el XML y automáticamente nos mostrará la ventana
con su estructura. Con esto ya podemos pasar al siguiente paso.

<br />![Archivo XML importado](recursos/imagen05.png)<br />

## 3. Creación y asociación de los estilos de párrafo

Ahora es necesario crear una asociación de las etiquetas XML con los
[estilos de párrafo](https://helpx.adobe.com/es/indesign/using/paragraph-character-styles.html).
Este paso solo es necesario si se desea tener un control en el diseño del EPUB
a través de hojas de estilos CSS. Si los estilos se manejan de manera directa
o se quieren preservar los actuales, puede pasarse al siguiente paso.

Nosotros siempre recomendamos el uso de CSS para el EPUB, sin importar que el
libro ya tenga un diseño para la impresión, porque:

1. existe un gran control sobre el diseño del *ebook* evitándose que se tenga
optimizar mediante el uso de otras herramientas, como puede ser Sigil,

2. disminuye el peso del libro ya que por lo general el diseño creado con
InDesign carece de uniformidad, creando varias líneas de código que incluso
pueden ser conflictivas,

3. si se necesita modificar algo en el diseño, es más fácil la comprensión del
código presente en una plantilla CSS que el creado por InDesign y

4. si se trata de una colección, es posible una uniformidad en las obras.

Lo primero que se debe de hacer es crear los estilos de párrafo, uno por
cada tipo de etiqueta que contiene nuestro documento. Se recomienda que el
nombre del estilo sea el mismo que el de la etiqueta para su fácil y
automática asociación. Por ejemplo, si en el XML tenemos una etiqueta `<pre>`,
el nombre del estilo de párrafo sería `pre`.

En esta obra nosotros tenemos once estilos. De estos seis son de encabezados,
que corresponden a cinco niveles distintos más uno reservado para el título. Los
cinco restantes son tipos de párrafo como son los cuerpo de texto, centrado,
alineado a la derecha, cuerpo preformateado para la legal y los versos.

<br />![Estilos de párrafo creados](recursos/imagen06.png)<br />

De las opciones disponibles para los estilos de párrafo, para el EPUB solo
nos interesa el apartado `Etiquetas de exportación`. Aquí tenemos que indicar
cuál etiqueta se creará en el EPUB para sustituir el estilo de párrafo, así
como indicar si esta etiqueta tendrá alguna clase CSS asociada. Por ejemplo,
el estilo de párrafo `h1T` es para el título, el cual, según la hoja de estilos
CSS que vamos a incorporar, este debe de estar dentro de una etiqueta `h1` con
la clase `titulo` (para evitar conflictos, evítense las tildes). Otros ejemplos
son: para el estilo de párrafo `pre`, una etiqueta `p` (párrafo) con la clase
`pre`; para el estilo de párrafo `h1`, una etiqueta `h1`, sin necesidad de
agregar alguna clase CSS.

<br />![Etiquetas de exportación](recursos/imagen07.png)<br />

Si este paso no se realiza, InDesign no sabrá qué etiqueta HTML corresponde
a los estilos de párrafo y estructura XML de nuestro documento. Además, de esta
manera InDesign podrá ignorar toda la configuración tripográfica del párrafo
*únicamente* cuando cree un EPUB, por lo que es posible manejar el estilo para
impresión con la seguridad de que este no interferirá con el diseño del EPUB.

Ahora es necesario asociar etiquetas a estilos y estilos a etiquetas. Parece
redundante, pero son dos opciones distintas que ofrece InDesign al momento de
trabajar con estilos de párrafo y estructura XML. En el primer caso se indica
que, por ejemplo, a toda etiqueta `<pre>` le corresponde el estilo de
párrafo `pre`. En la otra opción se indicaría que, continuando con el ejemplo,
toda clase de párrafo `pre` ha de ser una etiqueta `<pre>` por lo que, de
existir un párrafo con esta clase pero con una etiqueta XML distinta, supongamos
que `<cen>`, automáticamente se cambiará a la etiqueta asociada, `<pre>` en
este caso.

Esta doble asoación es un tanto confusa; sin embargo, lo único que tenemos que
tener encuenta es que si no se realiza, el EPUB creado
no tomará en cuenta la hoja CSS que se añadirá.

Entonces, en el menú de la ventana de la estructura XML nos vamos a `Asignar
etiquetas a estilos...`.

<br />![Asignación de etiquetas a estilos](recursos/imagen08.png)<br />

Si nuestras etiquetas tienen el mismo nombre que los estilos de párrafo, la
asociación se puede automatizar al hacer clic en `Asignar por nombre`.

<br />![Asignación por nombre](recursos/imagen09.png)<br />

Ahora en el mismo menú nos vamos a `Asignar estilos a etiquetas...`. Nótese
que es indistinto el orden en como se asocian los elementos, bien se puede
asignar antes los estilos a las etiquetas. Lo relevante es que se lleve a cabo
esta doble asignación.

<br />![Asignación de estilos a etiquetas](recursos/imagen10.png)<br />

Ya solo es cuestión se realizar la asociación. Si nuestros estilos de párrafo
tienen el mismo nombre que las etiquetas, este proceso se puede automatizar al
presionar sobre `Asignar por nombre`. Una vez hecho esto, podemos pasar a
vertir el texto.

<br />![Asignación por nombre](recursos/imagen11.png)<br />

## 4. Incrustación del texto y creación de la tabla de contenidos

Si el texto aún no está en una página, solo es necesario arrastrar la etiqueta
`body` a algún espacio de esta. Si el libro solo se exportará en formato EPUB,
no es necesario preocuparnos por la caja ni números de página, por lo que
no hay inconveniente con tener texto desbordado o que la página sea
estéticamente desagradable. Solo se busca que InDesign identifique que ahí hay
un texto para convertir en EPUB.

<br />![Texto vertido](recursos/imagen12.png)<br />

Como se podrá notar, el texto mostrará corchetes no imprimibles y de colores.
Este es un apoyo visual de InDesign para poder distinguir el tipo de etiqueta
XML de cada párrafo.

Para la creación de la tabla de contenidos hay que ir a `Maquetación > Tabla de
contenido...`.

<br />![Creación de la tabla de contenidos](recursos/imagen13.png)<br />

La única opción que a nos interesa en la ventana que se abre es el apartado de
`Estilos de la tabla de contenido` en donde vamos a incluir los estilos de
párrafo que queremos que formen parte de nuestro índice. En este libro solo
nos interesa que la tabla de contenido contenga los encabezados de primera
jerarquía, excepto el título, por lo que solo agregamos el estilo de párrafo
`h1`.

<br />![Ventana de la tabla de contenidos](recursos/imagen14.png)<br />

Ya solo es necesario agregar la tabla de contenidos al documento. De nueva
cuenta, para un libro que solo será EPUB no es necesario colocarlo en un lugar
en especial. Incluso es posible dejarlo en un área exterior a la página, como
se muestra en la siguiente imagen. Lo único que queremos explicitar a InDesign
es que este documento contiene un índice que ha de incorporarse al EPUB.

<br />![Incrustación de la tabla de contenidos](recursos/imagen15.png)<br />

## 5. Exportación para EPUB

Por fin tenemos todo lo necesario para crear un EPUB con InDesign. A
continuación nos vamos a `Archivo > Exportar...`.

<br />![Exportación](recursos/imagen16.png)<br />

Las características de la obra nos permite que no nos preocupemos por un diseño
fijo, el cual por lo general se destina para libros de apoyo didáctico o para
niños. Entonces en el formato eligiremos `EPUB (ajustable)`, también conocido
como de «diseño fluido» o «diseño responsivo», ya que automáticamente se
adaptará al tamaño de la pantalla.

<br />![Selección de EPUB con diseño fluido](recursos/imagen17.png)<br />

Se abrirá una nueva ventana para darle los últimos ajustes al EPUB. La mayoría
de los casos aquí solo tenemos que prestar atención a tres apartados. Para una
descripción sobre las opciones que no se verán aquí, puede echarse un vistazo
[aquí](https://helpx.adobe.com/indesign/using/export-content-epub-cc.html).

El primer apartado es `General`. Aquí se definirán las características
globales del EPUB.

* Seleccionaremos la versión EPUB. Nosotros recomendamos la versión `3.0`
por sus mayores posibilidades y flexibilidad. Para una información detallada
sobre las diferencias entre la versión `2.0.1` y la `3.0`, visítese este
[enlace](http://www.idpf.org/epub/30/spec/epub30-changes.html).

* Elección de la portada. En este caso añadiremos una imagen externa, por
lo que seleccionaremos `Elegir imagen`.

* Para que nuestra tabla de contenidos sea añadida en la sección `TDC para
navegación` hay que seleccionar la opción `Varios niveles`.

* En `Contenido` seleccionaremos la opción `Según maquetación de página`. Así
nos aseguraremos que el texto se mostrará tal cual como está vertido en las
páginas, muy oportuno si el libro sufrió cambios directos en el contenido que
no fueron inscrustados en la estructura XML, como es muy común cuando la obra
se maquetó para impresión.

* Habilitación de la opción `Dividir documento`. Esta obra tiene la intención
de comenzar un nuevo apartado en cada encabezado de primera jerarquía, así que la
división será con un `h1` en `Estilo de párrafo único`. Podemos decir que la
división es al EPUB lo que el salto de página es al PDF.

<br />![Configuración general](recursos/imagen18.png)<br />

El segundo apartado es `CSS`. Si no se desea agregar una hoja de estilos CSS
puede ignorarse lo siguiente.

* Para evitar que InDesign creé estilos CSS hay que desactivar
la opción `Generar CSS`.

* Para agregar una hoja de estilos externa hay que hacer clic en `Añadir hoja
de estilos...`.

<br />![Configuración de CSS](recursos/imagen19.png)<br />

Por último nos vamos al apartado `Metadatos`. Los metadatos son
[muy importantes](http://marianaeguaras.com/que-son-los-metadatos-de-un-libro-y-cual-es-su-importancia/)
para un *ebook* ya que, como dice Mariana, son «imprescindibles para la
identificación de un libro». Cabe decir que los metadatos son al EPUB lo que
la ficha catalográfica es al impreso. De esta manera nos será muy fácil
saber si cierto archivo es tal obra y no otra, sea en alguna tienda o en una
biblioteca digital.

* `Identificador`. Este dato no es visible al usuario y sirve como mecanismo
de control para quien edita. Nosotros preferimos los identificadores
complicados por lo que solo incluimos el nombre del libro y la versión.

* `Título`. Aquí va el nombre de la obra que será visible para el usuario.

* `Creador`. En este campo se coloca el nombre del autor. Por convención se
recomienda escribir antes el apellido y después el nombre, separados por una
coma. Si existe más de un autor, nosotros hemos decidido separarlo con punto
y coma.

* `Fecha`. Aquí se recomienda colocar la fecha de publicación de la presente
edición en lugar de la fecha de la primera edición.

* `Descripción`. Se trata de la sinopsis del libro, visible en algunas
bibliotecas digitales o tiendas.

* `Editor`. Se indica el responsable de la edición.

* `Derechos`. En este espacio se indica qué tipos de derechos de autor tiene
la obra.

* `Asunto`. Por último mencionamos la categoría del libro. Nosotros por lo
general separamos las categorías por comas, por ejemplo «Ficción, Novela de
caballerías». Si se desea una categorización más entandarizada, pueden
utilizarse [códigos BISAC](http://bisg.org/page/BISAC2015Edition) tal como es
necesario indicarlos si el libro se sube a iBooks, Google Play Books o Amazon.

<br />![Configuración de los metadatos](recursos/imagen20.png)<br />

Ahora sí ya hemos configurado el EPUB por lo que solo resta dar clic en `OK`
¡para generar el EPUB!

## El EPUB

Nuestro EPUB está listo. Este tiene:

* una portada visible en la estantería,

<br />![Portada del EPUB](recursos/imagen21.png)<br />

* un índice y

<br />![Tabla de contenidos del EPUB](recursos/imagen22.png)<br />

* un contenido controlado a través de una estructura XML y una hoja de estilos CSS.

<br />![Contenido del EPUB](recursos/imagen23.png)<br />

Para acceder a todos los documentos empleados para esta edición, pueden descargar
[este archivo](http://www.nievedechamoy.com.mx/recursos/eguaras/01/epubs.zip).
Al descomprimirse de podrá observar que también están presentes los archivos
emplados para los otros dos métodos que quedan por describir: con Sigil y
«desde cero». Para los archivos usados en la versión de InDesign solo hay que
ir a `epubs > 1-indesign-cc`.

## Balance

Para el usuario de InDesign que está acostumbrado a trabajar con estilos
directos o con estilos de párrafo y caracteres, esta descripción
puede parecerle compleja ya que involucra elaboración de
archivos de manera externa al flujo común de este *software*. Como puede
observarse, estos documentos externos requieren algún conocimiento básico
de HTML y CSS. Por este motivo, se mencionó que se descansaría *un poco* del
código.

El proceso aquí descrito está pensado para alcanzar el máximo control y la
mayor calidad en un libro EPUB hecho con InDesign. Sin embargo, de manera
irremediable implica vérselas con lenguajes de marcado. Es decir, incluso
en el programa más común para la creación de libros es menester trabajar desde
las entrañas de un EPUB.

Gracias a esto podemos indicar que:

1. para fortuna o desgracia el «código» llegó para quedarse en la edición
digital si el objetivo es la creación de publicaciones de gran calidad,

2. InDesign no puede abarcar todo el flujo de trabajo como hace algunos años
lo había hecho ya que su misma arquitectura está pensada para la maquetación de
libros en formato PDF que con los años se ha visto obligada a posibilitar
la creación de *ebooks*,

3. es necesaria un conjunto de herramientas o un *software* que nos permita
atajar los retos de la publicación digital de una manera más directa y aún más
controlada, y

4. por esta necesidad de diversidad en el ecosistema del libro se hace
necesario optar por un proceso de producción que simplifique y automatice el
reto de publicar una obra en diversos formatos.

Por estas cuestiones y las desventajas que presenta un EPUB creado en InDesign
(indicadas en «[Edición digital como edición desde
cero](http://marianaeguaras.com/edicion-digital-como-edicion-desde-cero/)») es
posible concluir que InDesign nos permite crear libros EPUB de gran calidad, pero a
través de una metodología que puede complicar aún más el proceso de producción
o que potencialmente implica una pérdida de control tanto en la estructura,
como en la edición y el diseño.

¿Cuánto es el volumen de producción a manejar? ¿Qué tanto control se desea
sobre la edición? ¿Cuánta es la disponibilidad para analizar y probar otras
metodologías de producción? Son solo algunas de las preguntas por las cuales
podríamos saber si InDesign es la herramienta que necesitamos para el desarrollo
de *ebooks*.
