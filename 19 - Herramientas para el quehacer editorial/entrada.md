# Herramientas para el quehacer editorial

Unas de las características que distinguen al cuidado editorial son
la meticulosidad puesta en la redacción, la ortotipografía, la 
verificación de datos y la uniformidad en la estructura y los estilos.
Sin embargo, **muchas veces mantener la calidad se vuelve una tarea
de difícil cumplimiento**: tiempos acotados, bajo presupuesto, 
proyectos que han pasado por muchas manos o una mera carencia de
organización o de las habilidades necesarias son algunos elementos que
van en detrimento de la calidad editorial.

Las diversas técnicas tradicionales o las recomendaciones de editores
con un amplio legado son fundamentales para ir subsanando estas 
debilidades. Pero no tenemos que detenernos ahí, ahora que **la 
edición es edición digital**, existen una diversidad de programas que
pueden ayudarnos a tener un mayor control en el quehacer editorial.

## El antiguo aliado y enemigo: los diccionarios

Con el surgimiento de los procesadores de texto nació una característica
que modificaría la manera en como un escritor revisaba sus propios
textos. Me refiero a los correctores ortográficos que en el documento
subrayan con rojo alguna palabra mal escrita. O eso creíamos las
primeras veces que usábamos la herramienta, ya que ¿cuántas veces no
se dio clic en «Corregir todo» o «Reemplazar todo» y se hacían los
cambios en lo que el corrector consideraba erróneo pero en realidad
simplemente eran palabras que no estaban en su diccionario?

Vaya dolor de cabeza darnos cuenta que nombres, regionalismos o 
ciertas conjugaciones desaparecían para abrir paso a un documento
casi ininteligible…

El entusiasmo de la mayoría de quienes usaban los correctores —«¿para
qué queremos editores, si el Word y Amazon ya hace eso», dice alguien 
cuando habla con un protoeditor cualquiera— fue opacado con el 
recibimiento negativo dentro del mundo editorial. ¿Qué clase de editor 
usa semejante herramienta —aunque muchas veces se trata de un gusto culposo—?

**Hay que distinguir entre los propósitos del corrector ortográfico de
su base y sus posibles usos; a saber, los diccionarios**. La manera en
como el corrector identifica las palabras, para posteriormente indicar
si es «errónea» y de ahí permitir la sustitución, es gracias a una 
colección de palabras, también llamadas diccionarios. Un ejemplo de
las primeras diez entradas de un diccionario en español de México es el 
siguiente:

```
57160
=======
a
ababa/S
ababol/S
abacería/S
abacero/GS
ábaco/S
abada/S
abadejo/S
abadengo/GS
abadengo/S
```

Este diccionario tiene poco más de cincuenta y siete mil entradas,
cualquier persona podría seguir llenándolo o crearlo desde cero. Pero
para fines editoriales esto no es común, ni tampoco necesario. Lo
importante es que ya se tiene una lista de palabras existentes en un
idioma, *sin importar que no esté completa*.

¿Para qué nos puede servir este diccionario, si lo usamos de manera
independiente a los correctores? Muy sencillo: automatización de 
procesos monótonos. Hay que tener cuidado, como es común cuando
se habla de «automatización» se tiende a entender que alguna máquina
hará todo el trabajo por nosotros, pero este no es el único significado.

**La automatización como sustitución del trabajo humano es, por así decirlo,
la acepción fuerte del término**. El propósito de los correctores es
automatizar en este sentido el trabajo de un corrector humano —nótese
que no de un editor humano—. Sin embargo, **la automatización también
puede ser un auxiliar del trabajo humano, principalmente en tareas
monótonas, una acepción que bien podemos catalogarla de «débil»**. Los 
«diccionarios» que usan estos correctores no pretenden reemplazar el
trabajo humano, incluso tampoco pretenden sustituir a lo que durante
siglos hemos entendido por «diccionario» —¡ni siquiera tienen 
definiciones!—. Su propósito es ser un auxiliar para distintas actividades,
principalmente de análisis, pero también para mejorar la calidad editorial.

Su uso dentro de la edición es sencillo. Como es común o al menos deseable
en el quehacer editorial, varias personas revisan un texto para corregir
erratas y otras cuestiones. Debido a que uno es ciego ante sus errores,
la lectura de diferentes personas permite encontrar deficiencias pasadas
por alto. La norma es: *muchos ojos, pero solo dos manos*. Es decir,
el editor a cargo cotejará y aplicará los cambios que diversos editores
o correctores anotaron.

No obstante, también es común que en el camino puedan añadirse dedazos
u otros horrores ortográficos, cuya relectura puede resultar costosa
o consumir mucho tiempo. Para estos casos finales, quizá el uso de un
diccionario puede ayudar a mostrar posibles erratas en un texto mediante
los siguientes pasos:

1. La persona a cargo de la edición manifiesta que el trabajo ya está
   listo para su maquetación o desarrollo de [EPUB]{.versalita}.
2. Antes de pasarlo al departamento de diseño o de desarrollo, se utiliza
   un programa para solo generar una lista de palabras únicas que no se encuentran
   en el diccionario. Ejemplo de programas tenemos a [hunspell](https://hunspell.github.io/)
   o a [aspell](http://aspell.net/) —ambos gratuitos y libres—.
3. Se revisa esta lista para descartar las palabras que sí son correctas
   —incluso pueden añadirse al diccionario para evitarse los falsos
   positivos—.
4. Las palabras restantes se cotejan en su contexto y, de ser erróneas,
   se corrigen.
5. Termina esta revisión y por fin se sigue con el flujo normal dentro
   de los procesos editoriales.

Aunque parece que esto consume mucho tiempo, en realidad el cotejo es
muy rápido, ya que solo se trata de una lista donde rápidamente es
posible eliminar los falsos positivos y así concentrarnos únicamente en
los casos dudosos. Esta forma de cotejo es mucho más rápida que estar 
saltando sobre el texto, más aún si paulatinamente se van agregando
otros términos.

De esta manera tenemos al menos mayor certeza sobre la calidad ortográfica
de nuestro proyecto, pero ¿para qué quedarnos ahí? Este modelo de trabajo
puede servir para otras características más allá de la ortografía.

## Lo que al autor no le importa pero que da dolores de cabeza: los enlaces.

Por experiencia he notado que **la mayoría de los autores tienen un 
completo desinterés en el estado actual de los enlaces** que ponen en
su documento, incluso entre aquellos que aman los enlaces a páginas
*web* o referencias cruzadas. Cuando se trata de enlaces internos, en
más de una ocasión me topo con que el elemento referenciado ya no 
existe o que su identificador ha cambiado. En el caso de los enlaces 
externos, es casi seguro que al menos de la mitad de los enlaces estarán 
mal escritos o serán ya inválidos.

Cuando la obra solo tiene un par, no hay ningún problema con revisarlos
manualmente. Pero ¿qué pasa cuando estamos ante una obra muy extensa,
con más de cincuenta enlaces y muchos de ellos repetidos a lo largo de 
diferentes secciones? En el peor de los casos se ignora por completo y 
ya será el lector quien se dé cuenta de esta falta de cuidado. En la 
mejor situación manualmente se revisa cada enlace y se corrige, tomando 
desde una jornada hasta una semana en completarse el trabajo, lo que 
al final ya no es tan bueno, más si los enlaces externos cambian su 
estatus de manera constante…

Para estos casos, se pueden seguir los mismos pasos de los diccionarios,
pero en lugar de analizar palabras, se analizan enlaces y de manera automática
se verifica su estatus. Esto también genera una lista que no solo permite
saber si un enlace es válido o no, sino también los motivos por los que
son inválidos, pudiendo descartar así si es un enlace muerto, el servidor 
actualmente está saturado o es un llano error de sintaxis.

Así ya tenemos un mayor cuidado en la ortografía y en los enlaces, pero
¿para qué detenernos ahí?

## Las analíticas como parte del quehacer editorial

Las analíticas de manera común se asocian al análisis de mercado, por
ejemplo, ¿qué dispositivos usan los lectores?, ¿cuáles son los formatos
más comunes?, ¿qué tipo de obra se consume más? Incluso a casos que
ya rayan en la invasión de la privacidad, como horarios de lectura,
velocidad de cambio de página, palabras más buscadas en el diccionario,
palabras subrayadas y más hábitos de lectura —¿o no, Amazon?—.

Pero **las analíticas en el ámbito editorial también pueden ayudarnos
a mejorar el cuidado**. Como ejemplos tenemos:

* Palabras más comunes en la obra: permite dar etiquetas más atinadas
  e incluso descubrir elementos que en una lectura lineal no habíamos
  sospechado. Por ejemplo, que en la antología de estas entradas [el
  programa más mencionado es InDesign](https://nikazhenya.github.io/entradas-eguaras/ebooks/recursos/epub-automata/logs/pc-analytics/analytics.html), 
  pese a que es público y notorio mi preferencia a no usar paquetería
  de Adobe y, en general, cualquier *software* que no sea libre o abierto…

* Palabras sin identificar: es decir, palabras que el análisis no pudo
  reconocer como meras letras o cifras, sino una combinación de ambos 
  e incluso con otros caracteres. Esto permite evidenciar de manera
  sencilla posibles deficiencias en la redacción o arcaísmos. Por ejemplo:
  «post-moderno» en lugar de «posmoderno» en un texto donde ambos términos
  se usan indistintamente. En este caso no solo corregimos arcaísmos,
  sino que obtenemos una mayor uniformidad.

* Palabras con versal inicial: esta clase de listado nos permite cotejar
  rápidamente posibles variantes en la escritura de un mismo personaje.
  Los ejemplos clásicos son los nombres rusos ¿cuántas veces no ha pasado
  que en una obra se mezcla indistintamente dos o más variantes, como 
  «Trotski» y «Trotsky»? O peor aún, variantes incorrectas como «Nietzche» 
  o «Nietsche» en lugar de «Nietzsche».

* Índice de diversidad: esto indica la frecuencia con la que una nueva
  palabra aparece en la obra; entre más tienda a cero, más diversidad
  tiene la obra; entre más extensa, la tendencia se aleja del cero.
  Más que mera curiosidad entre la comparación de estilos de diversos
  escritores, en casos extremos podríamos detectar potenciales plagios.
  Por ejemplo, cuando la obra de un autor tiene un índice de diversidad
  muy distinto a lo que es habitual en sus escritos: o es algo muy
  experimental, o en poco tiempo olvidó o aprendió nuevas palabras, o
  bien otra persona escribió la obra.

* Estructura de la obra: es posible tener un listado de los elementos
  que componen nuestra obra, como párrafos, encabezados, bloques de cita,
  etcétera, sin importar que sea para un formato impreso o digital. Esto
  puede ayudar a detectar inconsistencias o errores en la estructura.
  Por ejemplo, en el análisis de [*Edición digital como metodología para una edición global*](https://nikazhenya.github.io/entradas-eguaras/)
  existe una itálica no semántica (etiqueta `<i>`) cuando yo siempre
  empleo itálicas semánticas (etiqueta `<em>`), resulta que en la legal
  por accidente usé una en lugar de la otra. Otro ejemplo se da cuando,
  mientras revisamos las entradas Mariana y yo, algunos encabezados
  cambian de jerarquía; sin este tipo de revisión, las posibilidades
  de dar con esta inconsistencia —y corregirla— serían mucho menores.

Y estos son algunos ejemplos no de cómo la analítica podría mejorar
el cuidado editorial, sino cómo ya la empleamos con nuestros libros
—[Perro Tuerto](https://twitter.com/_perroTuerto), [Nieve de Chamoy](http://nievedechamoy.com.mx/) 
y [clientes](http://independientes.nievedechamoy.com.mx/)—. Entre las
herramientas de [Pecas](https://github.com/NikaZhenya/pecas) tenemos 
[una para crear esta analítica](https://github.com/NikaZhenya/pecas/tree/master/base-files/analytics)
que aunque aún le falta pulirse, ya ha demostrado su pertinencia al 
momento de mejorar la calidad de los libros con los que trabajamos.
