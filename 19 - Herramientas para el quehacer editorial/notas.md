Un falso positivo se da cuando una palabra es marcada como errónea, 
aunque en realidad sí es una palabra existente, pero no contenida en 
el diccionario.
