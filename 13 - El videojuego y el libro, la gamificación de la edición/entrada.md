# Videojuegos y libros, la gamificación de la edición

La tecnología digital ha traído dos fenómenos dentro del mundo de la edición:
1) la automatización, control y mejoramiento editorial mediante *software* y
2) la apertura del la noción de «libro» más allá de lo que lectores y editores
conocieron durante cinco siglos. El primer fenómeno tiene su máxima expresión
en la publicación estandarizada, principalmente en el ámbito académico. 
**El segundo caso nos lleva a los límites de la tradición editorial: el videojuego.**

## El extraño caso de una alianza y un antagonismo

Entre los muchos de los interminables debates dentro del mundo de la edición,
como si la publicación digital viene a *sustituir* al impreso, hay un tema que
de manera constante también es motivo de discusión: «el libro o el videojuego, 
¿de qué lado estás?». El libro, no como este u aquel objeto, sino como idea y
como tradición que atraviesa y tiene raíces profundas en nuestras cultura, es
tanto un medio como un fin. El libro se ha entendido como un vehículo de
entretenimiento, pero también de transmisión de conocimiento e incluso de
sabiduría. Y al mismo tiempo el libro ha representado una de las formas más
acabadas de expresión cultural, o al menos como el cierre de los complejos
actos de la creación literaria o la expresión de una idea.

El libro es tanto ese objeto que adquirimos, prestamos, maltratamos, sacamos
fotocopias, hasta que del libro no queda nada sino puras hojas sueltas, como
ese ideal donde el libro es la constatación del cumplimiento de una actividad
literaria, filosófica o científica. Cuando decimos «libro» no solo pensamos
en letras y papel, sino también en la idea de cumplimiento.

Entre las múltiples posibilidades de la tecnología digital, unas de ellas han
empezado a visibilizar e incluso diluir la idea de la completud de una obra o
la definición de lo que es un «libro». En otra ocasión nos ocuparemos de la
idea —inconveniente según mi opinión— de la completud. **El libro ahora no
puede entenderse como texto y papel, porque ni todos los libros han tenido texto
ni todos están compuestos de papel pero, principalmente, debido a que hay 
libros cuya comprensión es mediante el juego**.

Hay libros cuya experiencia no es tanto mediante la lectura o la observación
de imágenes, sino a través de una posición activa por parte del lector para
solucionar alguna clase de desafío o simplemente para darle continuidad a la
trama de la obra. Estos elementos están presentes en el videojuego. Con esto
no se quiere decir que el «videojuego» trajo consigo estas posibilidades, el
juego y, especialmente, el carácter lúdico de un libro es anterior a ello 
—piénsese en los libros de sopas de letras o en *Rayuela*—. Sin embargo, es
con el videojuego donde encontramos una de sus expresiones más radicales.

Por desgracia, esta semajanza y posibilidad que trae el videojuego para generar
otras experiencias de lectura por lo general se ve opacada por el contacto que
la mayoría de los lectores han tenido con los videojuegos. Si se compara la
narrativa de alguna franquicia como Mario Bros, Call of Duty, Grand Theft Auto
o Minecraft, poco o nada un lector podrá encontrar de interesante, ¿qué valor
tiene el rescate de una princesa por parte de un fontanero, si está Don
Quijote y Dulcinea?

**Un juicio valorativo hacia el videojuego basado en las franquicias que más
ganancias han generado es como emitir una opinión sobre la cultura letrada
basándose en los libros más vendidos**, que al menos en el caso mexicano son
los libros de autoayuda y los libros de texto. En este sentido, quizá lo más
pertinente es pensar el videojuego desde otros ejemplos concretos como
[The Journey](), [Braid](), [The Black Swan](), [Limbo]() o [Machinarium]().

## Del lápiz al papel, pasando por el teclado y los gestos hasta la realidad virtual y más allá

Llenar un crucigrama o jugar Calabozos y Dragones es una de las primeras lúdicas
que se tienen gracias a alguna clase de libro, aunque solo sea un instructivo.
A la par de la idea de «libro» como pilar de nuestra cultura, también está
patente su concepción como un producto más de entretenimiento, a tal punto que
desde sus comienzos se ha experimentado para que este carácter lúdico permita
una mayor actividad del lector.

Con el advenimiento de las computadoras personales y el internet se posibilita
tener mayor participación ya no como lector, sino también como jugador. Uno de
los ejemplos que más me agradan son los [MUD](), mundos ficticios —por lo 
general ficción medieval— construido con puro texto y cuya interacción es
escribiendo instrucciones con un teclado. Del entretenimiento que puede
encontrarse en el Don Quijote al hacer sátira de las noveles de caballerías,
en el [MUD]() el jugador puede ser el lector de su propia historia inserta en
un mundo tan artificial como es uno cuya representación es mera tipografía
digital. Si en uno nos entretiene las aventuras del Don Quijote, en el otro
el usuario disfruta de ser él mismo un Don Quijote.

Junto con el teclado, otra interface es el ratón. El *mouse* sintetiza una
serie de comandos escritos en un teclado en simples clics, *scrolls* y *drags*.
La experiencia de un libro también permite otro nivel de interactividad: las
aventuras *point-and-click* donde se interacciona con objetos en una escena
para darle continuidad a una historia. Uno de mis ejemplos favoritos es
Machinarium, donde un robot se adentra a una ciudad de robots para terminar
descubriendo lo que es la amistad en un mundo tan miserable y que hasta cierto 
punto recuerda a *El principito* y su continua búsqueda.

Una mezcla del uso del teclado y el ratón también permite otro tipo de
interacción con el lector-jugador, especialmente en contextos gráficos y en
«tiempo real». Solucinar una serie de problemas es una de las formas más básicas
de interactividad, como puede verse en Braid o Limbo. Aunque parezca sencillo, 
su solución requiere de ingenio hasta que al final del juego se percibe una 
tendencia existencial: el pasado que se ha sido, como en Braid, o el 
desconocimiento de no saber si se está vivo o muerto, como en Limbo. Entre
la nostalgia al pasada provocada por una magdalena con Proust y la mecánica de
regresión del tiempo en Braid existe un gran abismo, pero la sensación al leerlo
y al jugarlo es muy similar: el retorno es posible aunque no puede modificar el
tiempo presente.

La exploración de mundos abiertos, es decir, de entornos donde no es necesario
cumplir la historia como una receta, sino según como se antoje, es otra de
las experiencias posibles con un teclado y un ratón, y claro está, con un mando.
The Journey o The Black Swan son buenos ejemplos. En el primero se habita en un 
mundo posapocalíptico que no causa error sino desasosiego. En el segundo un niño 
huérfano empieza a seguir un cisne, lo cual lo llevará a conocer sobre su 
historia y la de sus padres. Existe cierto paralelismo entre Oliver Twist y 
The Black Swan ya que ambos son niños huérfanos que por algún u otro motivo se 
ven incertos en una ciudad que es un acertijo. Pero también está la gran 
diferencia en que en la ciudad de The Black Swan no hay ni un solo habitante, 
cuyo medio para aprender a moverse entre vericuetos es… aventar globos rellenos 
de agua o de pintura.

A partir de este punto, **la experiencia lúdica o lectora empieza a ser más
experimental y fragmentaria**. No porque no sea posible «contar una historia» 
como tradicionalmente lo entendemos, sino porque el *hardware* disponible es
relativamente reciente y faltan algunos años para que una persona amante de los
libros también tenga el control técnico sobre estas herramientas.

El caso más inmediato son los gestos con los dedos. Las pantallas táctiles
permiten otro tipo de interacción donde el intermediario entre la computadora
y nosotros no es una herramienta que pueda tomarse con las manos, sino un
dispositivo que puede interpretar nuestro movimiento con los dedos. Un ejemplo
narrativo que también genera una experiencia lectora es [The Noble Circle](),
donde se es un punto que va saltando para evitar obstáculos pero al mismo
tiempo te va explicando el porqué este círculo tiene la «necesidad» de saltar,
concluyendo en la descripción de un mundo compuesto por figuras geométricas y
sonidos.

Las tecnologías de reconocimiento, además de potenciales problemas de 
privacidad, también permiten otra interacción que en determinado punto puede
permitir otras experiencias de lectura. Desde el reconocimiento facial, hasta
el reconocimiento corporal y del entorno físico, como está presente en la
realidad virtual o aumentada, hay un enorme campo para la experimentación que
el mundo editorial recién está empezado a tomar en cuenta, con especial atención
a la realidad aumentada, debido a la posibilidad de mostrar contenidos
multimedia a través de un impreso.

**Pero no nos quedemos ahí, existe otro tipo de interfaz aún no comercial que
puede tener un fuerte impacto no solo en la industria del entretenimiento, sino
también en la del libro: la interfaz mente-máquina**. El usuario se coloca un
gorro en la cabeza que tienen una red de nodos que miden la actividad cerebral.
A partir de esta actividad, el usuario tiene la posibilidad de interactuar con
computadoras o con máquinas. Ya se ha hecho pruebas jugando [World of Warcraft](),
así que, ¿qué pasaría si un día la experiencia lectora no necesita nada más
que nuestra mente conectada a una máquina?

Para atenuar un poco, no se habla tanto de la Matrix, sino la forma en como
hemos accedido al contenido de una obra. Si las pantallas y la flexibilidad de
la publicación digital está revolucionando de tal modo al mundo editorial, al
destruir la estaticidad de la «página» o la definición de un «libro» acorde a
sus propiedades físicas, ¿qué podemos pensar cuando ya ni siquiera la vista 
sea necesaria para «leer», cuando la «lectura» ya no sea definible por una
actividad ocular? Quizá el camino que está abriendo el audiolibro, al desafiar
la idea de la vista como medio predilecto para el acceso a un libro, sea solo
la punta del *iceberg* de lo que se avecina en el futuro…

## Más presente y menos futuro

En lo personal no me gusta indagar sobre el «futuro» del libro, principalmente
porque lo que en muchos foros se trata como un tema futurista es más bien algo
que ya está ocurriendo pero se desconoce o, peor aún, no se percibe su potencial
importancia. La gamificación de la edición es un ejemplo de ello. Durante ya
mucho tiempo se ha estado gestando la interrelación entre el libro y el
videojuego; sin embargo, la mayoría de las veces estos experimentos fueron
etiquetados como «videojuegos» o, en el mejor de los casos, como un «estudio de
caso».

Pienso que esto sucede porque muchos de estos experimentos ni cumplen con lo
que de manera tradicional se ha entendido por «libro» ni a muchos de sus 
ejecutantes les interesa que sus obras sean catalogadas de esta manera. Pero 
también es debido a que **el editor promedio no le interesa nada que va más allá 
del horizonte de su profesión**. Son pocas las personas en el mundo de la edición 
que están al tanto de lo que se puede aprender al jugar videojuegos. Entre 
estas, son menos las que *efectivamente* el videojuego no solo se constituye 
como una forma de entretenimiento sino como una cultura. Todavía más reducido 
es el grupo de personas que a su vez tienen los medios técnicos para dejar de 
ser un expectador y meterse de lleno con la experimentación de estas 
posibilidades.

Así que, antes de imaginarnos experiencia de lecturas tipo Matrix, ¿por qué no
mejor probamos con no solo leer, sino también jugar un videojuego? ¿Por qué
no empezamos a ver al videojuego no como un objeto de entretenimiento o como
un producto infantil, sino como una creación cultural que en su dinámica nos
permite repensar y recrear lo que durante cinco siglos conocimos como libro?
