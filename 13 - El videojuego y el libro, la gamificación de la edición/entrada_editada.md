# 13. El videojuego y el libro: la gamificación de la edición

La [tecnología
digital](@index[2]) ha
traído dos fenómenos al mundo de la edición:

1. la automatización, el control y el mejoramiento editorial mediante el
   uso de _software_, y
2. la apertura de la noción de «libro» más allá de lo que los lectores y
   los editores conocieron durante cinco siglos.

El primer fenómeno tiene su máxima expresión en la publicación
estandarizada, principalmente en el ámbito académico. El segundo caso
nos lleva a **los límites de la tradición editorial: el videojuego.**

## El extraño caso de una alianza y un antagonismo

Entre los muchos de los interminables debates dentro del mundo de la
edición ---como si la publicación digital viene a *sustituir* al
impreso--- hay un tema que de manera constante también es motivo de
discusión: «el libro o el videojuego, ¿de qué lado estás?».

El libro, no como este u aquel objeto, sino como idea y como tradición
que atraviesa y tiene raíces profundas en nuestras cultura, es tanto un
medio como un fin. El libro se ha **entendido como un vehículo de
entretenimiento, pero también de transmisión de conocimiento** e incluso
de sabiduría.

Al mismo tiempo, el libro ha representado una de las formas más acabadas
de expresión cultural, o al menos como el cierre de los complejos actos
de la creación literaria o la expresión de una idea.

El libro es tanto ese objeto que adquirimos, prestamos, maltratamos,
sacamos fotocopias, hasta que del libro no queda nada sino puras hojas
sueltas, como también ese ideal donde se constata el cumplimiento de una
actividad literaria, filosófica o científica. Cuando decimos «libro» no
solo pensamos en letras y papel, sino también en la idea de
cumplimiento.

Entre las múltiples posibilidades de la tecnología digital, unas de
ellas han empezado a visibilizar e incluso diluir la idea de la
completud de una obra o la definición de lo que es un «libro».

En otra ocasión nos ocuparemos de la idea ---inconveniente, según mi
opinión--- de la completud. **El libro ahora no puede entenderse como
texto y papel**, porque ni todos los libros han tenido texto ni todos
están compuestos de papel, **y, principalmente, debido a que hay libros
cuya comprensión es mediante el juego**.

Hay libros cuya experiencia no es tanto mediante la lectura o la
observación de imágenes, sino a través de una posición activa por parte
del lector para solucionar alguna clase de desafío o, simplemente, para
darle continuidad a la trama de la obra. Estos elementos están presentes
en el videojuego.

Con esto no se quiere decir que el videojuego trajo consigo estas
posibilidades, ya que el juego y, especialmente, el carácter lúdico de
un libro es anterior a ello ---piénsese en los libros de sopas de letras
o en _Rayuela_, de Cortázar---. Sin embargo, es con el videojuego donde
encontramos una de sus expresiones más radicales.

Por desgracia, esta semejanza y posibilidad que trae el videojuego para
generar otras experiencias de lectura, por lo general, se ve opacada por
el contacto que la mayoría de los lectores han tenido con los
videojuegos.

Si se compara la narrativa de alguna franquicia como _Mario Bros, Call
of Duty, Grand Theft Auto_ o _Minecraft_, un lector poco o nada podrá
encontrar interesante. ¿Qué valor tiene el rescate de una princesa por
parte de un fontanero si existen Don Quijote y Dulcinea?

**Un juicio valorativo hacia el videojuego basado en las franquicias que
más ganancias han generado es como emitir una opinión sobre la cultura
letrada basándose en los libros más vendidos**, que al menos en el caso
mexicano son los libros de autoayuda y los libros de texto.

En este sentido, quizá lo más pertinente es pensar el videojuego desde
otros ejemplos concretos, como
[_Journey_](http://thatgamecompany.com/journey/),
[_Braid_](http://braid-game.com/),
[_The Unfinished Swan_](http://www.giantsparrow.com/games/swan/),
[_Limbo_](http://playdead.com/games/limbo/),
[_Machinarium_](http://machinarium.net/) _o_ [_A Noble
Circle_](https://itunes.apple.com/us/app/a-noble-circle/id977865620?mt=8).

## Del lápiz al papel; el teclado y los gestos, la realidad virtual y más allá

Llenar un crucigrama o jugar [_Calabozos y Dragones_](http://dnd.wizards.com/)
(_Dragones y mazmorras_, en España) es una de las primeras experiencias 
lúdicas que se tienen gracias a alguna clase de libro, aunque solo sea 
un instructivo.

A la par de la idea de «libro» como pilar de nuestra cultura, también
está patente su concepción como un **producto más de entretenimiento**,
a tal punto que desde sus comienzos se ha experimentado para que este
carácter lúdico permita una mayor actividad del lector.

### Mundos ficticios y _Don Quijote_

Con el advenimiento de las computadoras personales y el internet se
posibilita **tener mayor participación ya no como lector, sino también
como jugador**.

Uno de los ejemplos que más me agradan son los
[+++MUD+++](https://es.wikipedia.org/wiki/MUD), mundos ficticios (por lo
general, ficción medieval) construidos con puro texto y cuya interacción
es escribiendo instrucciones con un teclado.

Del entretenimiento que puede encontrarse en _Don Quijote_ al hacer
sátira de las novelas de caballerías, en el +++MUD+++ el jugador puede ser el
lector de su propia historia inserta en un mundo tan artificial como es
uno cuya representación es mera tipografía digital.

Si en uno nos entretiene las aventuras de Don Quijote, en el otro el
usuario disfruta de ser él mismo Don Quijote.

### _Machinarium_, _Braid_, _Limbo_ y la magdalena de Proust

El _mouse_ sintetiza una serie de comandos escritos en un teclado en
simples clics, _scrolls_ y _drags_. La experiencia de **un libro**
también **permite otro nivel de interactividad**: las **aventuras
_point-and-click_** donde se interacciona con objetos en una escena para
darle continuidad a una historia.

Uno de mis ejemplos favoritos es _Machinarium_, donde un robot se
adentra en una ciudad de robots para terminar descubriendo lo que es la
amistad en un mundo tan miserable, y que hasta cierto punto recuerda a
_El principito_ y su continua búsqueda.

Una mezcla del uso del teclado y el ratón también permite otro tipo de
interacción con el lector-jugador, especialmente en contextos gráficos y
en «tiempo real». Solucionar una serie de problemas es una de las formas
más básicas de interactividad, como puede verse en _Braid_ o _Limbo_.

Aunque parezca sencillo, su solución requiere de ingenio hasta que al
final del juego se percibe una tendencia existencial: el pasado que se
ha sido, como en _Braid_, o el desconocimiento de no saber si se está
vivo o muerto, como en _Limbo_.

Entre la nostalgia al pasado provocada por una magdalena en _En busca
del tiempo perdido_ con Proust y la mecánica de regresión del tiempo en
_Braid_ existe un gran abismo. Sin embargo, la sensación al leerlo y al
jugarlo es muy similar: el retorno es posible aunque no puede modificar
el tiempo presente.

### Mundos abiertos: _Journey_, _The Unfinished Swan_ y _Oliver Twist_

La exploración de mundos abiertos; es decir, de entornos donde no es
necesario cumplir la historia como una receta, sino según como se
antoje, es otra de las experiencias posibles con un teclado y un ratón,
y claro está, con un mando.

_Journey_ o _The Unfinished Swan_ son buenos ejemplos. En el primero
se habita en un mundo posapocalíptico que no causa terror sino
desasosiego. En el segundo un niño huérfano empieza a seguir un cisne,
lo cual lo llevará a conocer sobre su historia y la de sus padres.

Existe cierto paralelismo entre _Oliver Twist_ y _The Unfinished Swan_,
ya que ambos son niños huérfanos que por algún u otro motivo se ven
insertos en una ciudad que es un acertijo. Pero también está la gran
diferencia en que en la ciudad de _The Unfinished Swan_ no hay ni un
solo habitante, cuyo medio para aprender a moverse entre vericuetos es
aventar globos rellenos de agua o de pintura.

A partir de este punto, **la experiencia lúdica o lectora empieza a ser
más experimental y fragmentaria**. No porque no sea posible «contar una
historia», como tradicionalmente lo entendemos, sino porque el
_hardware_ disponible es relativamente reciente y faltan algunos años
para que una persona amante de los libros también tenga el control
técnico sobre estas herramientas.

### Pantallas táctiles y realidad virtual o aumentada

El caso más inmediato son los gestos con los dedos. Las pantallas
táctiles permiten otro tipo de interacción donde el intermediario entre
la computadora y nosotros no es una herramienta que pueda tomarse con
las manos, sino un dispositivo que puede interpretar nuestro movimiento
con los dedos.

Un ejemplo narrativo que también genera una experiencia lectora es _A
Noble Circle_, donde el jugador es un círculo que mediante toques con el
dedo va saltando para evitar obstáculos pero, al mismo tiempo, un
narrador te va explicando el porqué este círculo tiene la «necesidad» de
saltar, concluyendo en la descripción de un mundo compuesto por figuras
geométricas y sonidos.

Las tecnologías de reconocimiento, además de potenciales problemas de
privacidad, también permiten otra interacción que en determinado punto
puede permitir otras experiencias de lectura.

Desde el reconocimiento facial, hasta el reconocimiento corporal y del
entorno físico, como está presente en la **realidad virtual o
aumentada**, hay un enorme [campo para la
experimentación](@index[6])
que el mundo editorial recién está empezado a tomar en cuenta, con
especial atención a la realidad aumentada, debido a la posibilidad de
mostrar contenidos multimedia a través de un impreso.

### ¿Es posible leer sin ver?

Pero no nos quedemos ahí, existe **otro tipo de interfaz aún no comercial**
que puede tener un fuerte impacto no solo en la industria del
entretenimiento, sino también en la del libro: **la interfaz
cerebro-máquina**.

El usuario se coloca un gorro que tiene una red de nodos que miden la
actividad cerebral. A partir de esta actividad, el usuario tiene la
posibilidad de interactuar con computadoras o con máquinas.

Ya se [han hecho pruebas jugando _World of
Warcraft_](https://www.youtube.com/watch?v=jXpjRwPQC5Q&t=32s). Por
tanto, ¿qué pasaría si un día la experiencia lectora no necesita nada
más que nuestra mente conectada a una máquina?

Para atenuar un poco, no se habla tanto de la Matrix, sino de la forma
en cómo hemos accedido al contenido de una obra.

Las pantallas y la flexibilidad de la publicación digital están
revolucionando el mundo editorial, al punto de destruir la estaticidad
de la «página» o la definición de un «libro» acorde a sus propiedades
físicas. Entonces, ¿qué podemos pensar cuando ya ni siquiera la vista
sea necesaria para «leer», cuando la «lectura» ya no sea definible por
una actividad ocular?

Quizá el camino que está abriendo el __audiolibro__, al desafiar la idea
de la vista como medio predilecto para el acceso a un libro, sea solo la
punta del _iceberg_ de lo que se avecina en el futuro…

## Más presente y menos futuro

En lo personal no me gusta indagar sobre el «futuro» del libro,
principalmente porque lo que en muchos foros se trata como un tema
futurista es más bien algo que ya está ocurriendo, pero que se desconoce
o, peor aún, no se percibe su potencial importancia.

La gamificación de la edición es un ejemplo de ello. Durante ya mucho
tiempo se ha estado gestando la interrelación entre el libro y el
videojuego. Sin embargo, la mayoría de las veces estos experimentos
fueron etiquetados como «videojuegos» o, en el mejor de los casos, como
un «estudio de caso».

O bien, sus ejecuciones, al desarrolladas por estudios independientes de
videojuegos y no por grandes compañías con mucho presupuesto para
publicidad y desarrollo, quedan fuera del panorama de un amante de los
libros. Los ejemplos empleados aquí son solo una pequeña muestra de cómo
el desarrollo independiente de videojuegos es una de las principales
ramas que se acerca al discurso o tramas literarias.

Una plataforma para conocer más de este tipo de videojuegos es
[Itch](https://itch.io/), por si gustan visitarla, sus videojuegos son
gratuitos o de bajo precio.

Pienso que si han sido etiquetados de esta forma es porque muchos de
estos experimentos no cumplen con lo que de manera tradicional se ha
entendido por «libro» ni a muchos de sus ejecutantes les interesa que
sus obras sean catalogadas de esta manera.

Pero también es debido a que **el editor promedio no le interesa nada
que vaya más allá del horizonte de su profesión**. Son pocas las
personas en el mundo de la edición que están al tanto de lo que se puede
aprender al jugar videojuegos.

Entre estas, son menos las que *efectivamente* el videojuego no solo se
constituye como una forma de entretenimiento sino como una cultura.
Todavía más reducido es el grupo de personas que, a su vez, tiene los
medios técnicos para dejar de ser un espectador y meterse de lleno con
la experimentación de estas posibilidades.

Así que, antes de imaginarnos experiencia de lecturas tipo Matrix, ¿por
qué no mejor probamos con no solo leer, sino también jugar un
videojuego?

**¿Por qué no empezamos a ver al videojuego** no como un objeto de
entretenimiento o como un producto infantil, sino **como una creación
cultural que** en su dinámica nos **permite repensar y recrear lo que
durante cinco siglos conocimos como «libro»?**

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «Videojuegos y libros, la gamificación de la edición».
* Título en el _blog_ de Mariana: «El videojuego y el libro: la gamificación de la edición».
* Fecha de publicación: 17 de octubre del 2017.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/13%20-%20El%20videojuego%20y%20el%20libro,%20la%20gamificaci%C3%B3n%20de%20la%20edici%C3%B3n/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/el-videojuego-y-el-libro-la-gamificacion-de-la-edicion/).

</aside>
