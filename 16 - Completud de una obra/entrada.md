# ¿Completud de una obra?

## Entre la edición y la versión

No son pocas veces cuando el tiempo consume un proyecto editorial.
La obra se publica y quizá se encuentre alguna errata, tal vez no
encontramos un error, pero sin importar el caso queda la sensación
de que pudo haber estado mejor. Si hubiéramos tenido más tiempo, si
no nos hubiéramos enfocado tanto en alguna cuestión…

La obra sale, se distribuye, se comercializa y la mayoría de los
lectores ven al libro como un producto acabado que va a su estantería,
que les agradará, se arrepentirán de comprarlo o lo devolverán. Pocos 
son los que saben que en la página cuarenta y cinco hay un callejón 
que solo saltó después de la publicación; o más irritante, siempre 
quedará ese sabor de que la obra salió demasiado rápido, que quizá una 
lectura adicional no hubiera estado mal.

A muchas de las editoriales que hemos apoyado con el desarrollo de 
[EPUB]{.versalita} o con capacitación contemplan estos síntomas. 
Algunos editores son francos y aceptan que la cantidad de tiempo no es 
suficiente para la cantidad de obras que *tienen que* publicar. Otros 
son más recelosos de sus procesos, pero la estructura de sus archivos 
los delata. **El formato, aunque por lo general «invisible», demuestra 
el modo de trabajo, e incluso anticipa dónde habrán inconsistencias 
antes de leer una obra**.

Pese a toda esa batalla librada tras bambalinas, sale *x* edición
de una obra. Ya será para la siguiente edición donde se tomará
más cuidado… si es que con los años ese `archivo-final-final-final` no 
se pierde, aún se puede abrir o no se confunde con el `archivo-final3`.

¿Qué tanto este problema es por tratar de imponer un ritmo de trabajo
a una profesión que ha sobresalido por la paciencia que se requiere
para ejecutar un proyecto? ¿No será más bien alguna deficiencia
metodológica? ¿Y si es una falta de capacitación? Como sea, quienes
se involucran en la producción de un libro en más de una ocasión sus
herramientas representan un obstáculo.

Bajo este contexto, la tradición editorial se las ve ante un nuevo
fenómeno: las versiones. En las obras el número de edición sirve para
explicitar la completud de toda una serie de procesos. Aunque sea
parte del imaginario del lector o un ideal de quienes editan libros, 
**la completud rara vez significa que una obra ya no requiere de más 
trabajo, más bien indica la resignación o imposición de tiempos a un
proyecto**. ¿Cuántas veces en una presentación de algún libro se 
indica ese deseo de haber tenido más tiempo? ¿Cuántas veces no se 
admite que tuvo que cerrarse la edición, como si se tratase de una 
publicación periódica?

Sin embargo, en las versiones no sucede eso. En el ámbito del 
*software* **el versionado solo indica el estado de desarrollo de un
proyecto**. Cada nueva versión, sea para grandes o pequeños cambios,
o para corrección *o adición* de errores, no indica un estado de
completud, sino el grado de madurez de un programa. Lo interesante es 
que el *software* se percibe como un producto en constante crecimiento 
y mantenimiento, mientras que en la edición la obra se busca hacerla 
pública cuando se considera que está acabada.

Si en el número de edición se tiene el problema que su «completud» se
vuelve ambigua —¿qué se «completó», los procesos para publicar una 
obra o la cantidad de trabajo que esta requiere para ya no necesitar 
más cambios?—, las versiones comparten el supuesto de que un producto 
yace sobre un proceso evolutivo indefinido.

Al parecer una nueva versión siempre es mejor que la versión anterior.
Pero las versiones recientes también pueden acarrear inconvenientes:
la creación de [*bugs*](https://es.wikipedia.org/wiki/Error_de_software),
un mayor consumo de recursos o la pérdida de soporte a dispositivos 
antiguos. ¿Qué tan «buena» es una nueva versión de un programa cuando
este empieza a correr lento en nuestra computadoras?

Por fortuna en el ámbito editorial nada se volverá más lento si además
de ediciones se empiezan a trabajar con versiones. El uso de versiones
en la edición permite la adición de mejoras o la corrección de errores
sin tener que volver a iniciar todo un ciclo de trabajo. **Las 
versiones pueden permitir una mayor versatilidad en pos de la 
experiencia de lectura**. La idea suena muy bien, pero su 
implementación puede ser un martirio técnico, de cuidado editorial o
de índole legal.

Si continuamos en el contexto donde muchas de las editoriales batallan
con sus propias herramientas y formatos para producir al menos un
producto, la complejidad técnica aumenta cuando se empiezan a meter
más productos —claro está, si la metodología para publicar es la [edición cíclica](https://marianaeguaras.com/edicion-ciclica-y-edicion-ramificada-de-la-antesala-a-la-revolucion-digital/).
El desafío aumenta cuando ya no solo se tiene que crear una edición,
sino que de manera constante se han de actualizar los archivos según
cada nueva versión. En el caso del impreso, si no se trata de 
impresión digital de pocos y constantes tirajes, la idea de publicar
cada nueva versión es simplemente una locura. 

El caso de la publicación digital no es menos compleja: **entre las 
deficiencias metodológicas, las prisas y la falta de capacitación o de 
organización en los archivos, la actualización del archivo puede 
demorar varios horas o días**, más si la persona responsable está poco 
familiarizada con las distintas plataformas —ojalá que así como hay 
estándares en los formatos, estos distribuidores llegarán a un 
estándar para la subida de libros. Esto obviando el visto de bueno
de los responsables de la edición o, si se trata de instituciones, los
trámites administrativos requeridos para la publicación de una obra.

En el ámbito editorial, así como en una nueva versión de *software* se 
pueden colar *bugs*, en una versión más reciente de una obra pueden
provocar nuevas erratas o errores tipográficos. ¿Qué tal si quien puso
el cambio por descuido presionó una tecla y coló alguna letra en el
texto? ¿Qué tal si en una corrección sin advertirse se movió un 
párrafo, una página o una sección entera, creando callejones, viudas
o huérfanas? Para los editores más controladores, estos cambios al 
menos requerirían una vista al vuelo de toda la obra… ¡pero no hay 
tiempo suficiente!

En el contexto multiformato puede también empezar a crearse una 
disparidad en los diversos formatos. **El cuidado editorial también
vela por la uniformidad**, y esta constante metida de cambios poco a
poco puede provocar que los formatos sean distintos. Lo más probable
es que la mayoría de los lectores no lo noten, pero en muchos casos
no se trata del lector, sino la mera insatisfacción del editor de
saber que algo no cuaja, que poco a poco se pierde el control sobre
la edición —¿tiene que agregarse que muchos profesionales de la 
edición se preocupan más por lo que pensarán sus colegas que por el 
uso que le dé el lector meta a sus libros?—.

El ámbito legal no queda fuera. En una obra por lo general están 
involucradas una serie de contratos con autores y colaboradores, 
convenios con instituciones o distribuidores, y trámites 
administrativos. Quizá mucho de este papeleo se pueda simplificar para
que el uso de versiones no requiera mucha administración, pero si de
por sí la «modernización» de contratos y convenios ya es un reto en
manos del jurídico —por ejemplo, el ofrecimiento de licencias de uso—,
hay una gran incógnita aún sin resolver: los [ISBN]{.versalita}.

El [ISBN]{.versalita} es una especie de cédula de identidad para cada
edición y *cada formato*. Esta identidad se pierde con las versiones,
ya que entre versión y versión la obra ya no es la misma. Es decir,
**en teoría cada nueva versión requeriría un nuevo [ISBN]{.versalita}
para ¡cada formato!** —o abandonamos el [ISBN]{.versalita} y buscamos
una solución estandarizada más dinámica: guiño a las [llaves públicas](https://es.wikipedia.org/wiki/Criptograf%C3%ADa_asim%C3%A9trica)
o a la [cadena de bloques](https://www.creativechain.org/proyecto/).

Los retos técnicos y legales que pueden representar el versionado es
una cuestión que en el desarollo de *software* se ha estado trabajando
desde hace mucho tiempo. Por un lado se tienen las licencias de uso
de *software* que simplifican las cuestiones legales. Por el otro
se tiene que en el desarrollo de *software* las versiones no es un
reto técnico porque:

* o solo se produce un ejecutable (para Windows, Mac o Linux),
* o se producen diferentes ejecutables a partir del mismo código fuente,
* o se tienen equipos independientes según la plataforma (un equipo para Android, otro para iOS).

En la edición la producción de un solo formato de por sí ya es un reto
por la falta de capacitación o de organización de los archivos. Las 
concepciones metodológicas multiformato, como la [edición cíclica](http://marianaeguaras.com/edicion-ciclica-y-edicion-ramificada-del-surgimiento-al-tronco-y-las-ramas/),
son prácticamente desconocidas o ignoradas, por lo que no es posible
crear diferentes formatos a partir de los mismos archivos madre. Lo
que queda es la publicación de distintos formatos de manera 
independiente, que en muchos de los casos es un despilfarro de recursos.

En el ámbito de *software* solo grandes empresas u organizaciones, o
grupos de trabajo muy comprometidos, tienen los recursos suficientes
para poder tener equipos independientes según la plataforma. Por ejemplo,
Snapchat o Telegram tienen personal destinado para plataformas 
específicas; o bien, la comunidad de linux que se encarga de adecuar 
el kernel según el tipo de arquitectura.

Si hacemos la analogía, **en el ámbito editorial los únicos con la
capacidad de actualizar sus formatos con pocos inconvenientes son las
grandes casas editoriales**. Por ejemplo, a Penguin Random House o a
Grupo Planeta el uso de versiones más que un reto, sería un *plus* a
sus ediciones, porque tienen la capacidad de destinar distintos 
equipos. Pero a la mayoría de las editoriales —medianas, pequeñas o 
independientes— y sin duda a los autores que se autopublican este modo 
de producción no es el más conveniente.

**El uso de versiones entre pequeños editores solo será factible 
cuando exista un cambio metodológico de fondo**. Pero cuando eso sea
posible, quizá el uso de versiones ya no sea necesario porque existe
un modelo de desarrollo que podría ser más apto: el *rolling release*,
que bien podemos llamar «edición continua»…
