# 16. Completud de una obra: ¿se acaba de editar con la publicación de un libro?

## Entre la edición y la versión

El tiempo puede consumir un proyecto editorial. La obra se publica y
quizá se encuentre alguna errata o tal vez no encontramos un error,
pero, sin importar el caso, queda la sensación de que pudo haber estado
mejor. Si hubiéramos tenido más tiempo, si no nos hubiéramos enfocado
tanto en alguna cuestión, si…

La obra sale, se distribuye, se comercializa y la mayoría de los
lectores ven al libro como un producto acabado que va a su estantería,
que les agradará, o se arrepentirán de comprarlo y lo devolverán.

Pocos son los que saben que en la página cuarenta y cinco hay un
callejón que solo se vio después de la publicación. O, más irritante,
siempre quedará ese sabor de que la obra salió demasiado rápido, que
quizá una lectura adicional no hubiera estado mal.

A muchas de las editoriales que hemos apoyado con el desarrollo de +++EPUB+++
o con capacitación sienten estos síntomas. Algunos editores son francos
y aceptan que la cantidad de tiempo no es suficiente para la cantidad de
obras que *tienen que* publicar. Otros son más recelosos de sus
procesos, pero la estructura de sus archivos los delata.

**El formato**, aunque por lo general «invisible», **demuestra el modo
de trabajo e incluso anticipa dónde habrá inconsistencias antes de leer
una obra**.

Pese a toda esa batalla librada tras bambalinas, sale _x_ edición de una
obra. Ya será para la siguiente edición donde se tomará más cuidado…
Si es que con los años ese `archivo-final-final-final` no se pierde, aún
se puede abrir o no se confunde con el `archivo-final3`.

¿Cuánto de este problema se debe a tratar de imponer un ritmo de trabajo
a una profesión que ha sobresalido por la paciencia que se requiere para
ejecutar un proyecto?

¿No será más bien alguna deficiencia metodológica? ¿Y si es falta de
capacitación? Como sea, quienes se involucran en la producción de un
libro en más de una ocasión sus herramientas representan un obstáculo.

## Las versiones de una obra y el desarrollo de un proyecto editorial

Bajo este contexto, la tradición editorial se ve ante un nuevo fenómeno:
las versiones. En los libros, el número de edición sirve para explicitar
la completud de toda una serie de procesos.

Aunque sea parte del imaginario del lector o un ideal de quienes editan
libros, **la completud rara vez significa que una obra ya no requiere**
**más trabajo, más bien indica la resignación o imposición de tiempos a
un proyecto**.

¿Cuántas veces en una presentación de un libro se indica ese deseo de
haber tenido más tiempo? ¿Cuántas veces se admite que tuvo que cerrarse
la edición, como si se tratase de una publicación periódica?

Sin embargo, en las versiones no sucede eso. En el ámbito del
_software_, **el versionado solo indica el estado de desarrollo de un
proyecto**. Cada nueva versión, sea para grandes o pequeños cambios, o
para corrección *o adición* de errores, no indica un estado de
completud, sino el grado de madurez de un programa.

El _software_ se percibe como un producto en constante crecimiento y
mantenimiento, mientras que en la edición la obra se busca hacerla
pública cuando se considera que está acabada.

Si en el número de edición su «completud» se vuelve ambigua ---¿qué se 
«completó»: los procesos para publicar una obra o la cantidad de 
trabajo que esta requiere para ya no necesitar más cambios?---, las 
versiones comparten el supuesto de que un producto yace sobre un 
proceso evolutivo indefinido.

Al parecer una nueva versión siempre es mejor que la versión anterior.
Pero las versiones recientes también pueden acarrear inconvenientes: la
creación de [_bugs_](https://es.wikipedia.org/wiki/Error_de_software),
un mayor consumo de recursos o la pérdida de soporte a dispositivos
antiguos.

¿Qué tan «buena» es una nueva versión de un programa cuando este empieza
a correr lento en nuestras computadoras?

## Versiones y ediciones de una obra

Por fortuna, en el ámbito editorial nada se volverá más lento si además
de ediciones se empiezan a trabajar con versiones. El uso de versiones
en la edición permite la adición de mejoras o la corrección de errores
sin tener que volver a iniciar todo un ciclo de trabajo.

**Las versiones pueden permitir una mayor versatilidad en pos de la
experiencia de lectura**. La idea suena muy bien, pero su implementación
puede ser un martirio técnico, de [cuidado editorial](https://marianaeguaras.com/el-cuidado-editorial-durante-la-produccion-de-un-libro/) 
o de índole legal.

Si continuamos en el contexto donde muchas de las editoriales batallan
con sus propias herramientas y formatos para producir al menos un
producto, la complejidad técnica aumenta cuando se empiezan a meter más
productos ---claro está, si la metodología para publicar es la [edición
cíclica](@index[8])---.

El desafío aumenta cuando ya no solo se tiene que crear una edición,
sino que de manera constante se han de actualizar los archivos según
cada nueva versión. En el caso del impreso, si no se trata de impresión
digital, de pocos y constantes tirajes, la idea de publicar cada nueva
versión es simplemente una locura.

El caso de la publicación digital la situación no es menos compleja:
entre las deficiencias metodológicas, las prisas y la falta de
capacitación o de organización en los archivos, la actualización del
archivo puede demorar varias horas o días; más si la persona responsable
está poco familiarizada con las distintas plataformas. Esto obviando el
visto de bueno de los responsables de la edición o, si se trata de
instituciones, los trámites administrativos requeridos para la
publicación de una obra.

## Cambios y correcciones que introducen y producen errores

En el ámbito editorial, así como en una nueva versión de _software_, se
pueden colar _bugs_ que pueden provocar nuevas erratas o errores
tipográficos en una versión más reciente de una obra. ¿Qué tal si quien
introdujo el cambio presionó, por descuido, una tecla y coló una letra
en el texto?

¿Qué tal si en una corrección, sin advertirse, se movió un párrafo, una
página o una sección entera, creando callejones, viudas o huérfanas?
Para los editores más exigentes, estos cambios requerirían, al menos,
una lectura rápida de toda la obra… ¡pero no hay tiempo suficiente!

En el contexto multiformato puede también empezar a crearse una
disparidad en los diversos formatos. **El cuidado editorial también vela
por la uniformidad**, y esta constante introducción de cambios poco a
poco puede provocar que los formatos sean distintos.

Lo más probable es que la mayoría de los lectores no lo noten, pero en
muchos casos no se trata del lector, sino de la mera insatisfacción del
editor de saber que algo no cuaja, que poco a poco se pierde el control
sobre la edición.

El ámbito legal no queda fuera. En una obra, por lo general, están
involucrados una serie de contratos con autores y colaboradores,
convenios con instituciones o distribuidores y trámites administrativos.
Quizá todo este papeleo se pueda simplificar para que el uso de
versiones no requiera mucha administración, pero si la «modernización»
de contratos y convenios ya es un reto en manos del jurídico ---por
ejemplo, el ofrecimiento de licencias de uso---, hay una gran incógnita
aún sin resolver: los +++ISBN+++.

## El +++ISBN+++

El +++ISBN+++ es una especie de cédula de identidad para cada edición y *cada
formato*. Esta identidad se pierde con las versiones, porque entre
versión y versión la obra ya no es la misma. Es decir, **en teoría, cada
nueva versión requeriría un nuevo +++ISBN+++ para ¡cada formato!** O
abandonamos el +++ISBN+++ y buscamos una solución estandarizada más dinámica:
guiño a las [llaves
públicas](https://es.wikipedia.org/wiki/Criptografía_asimétrica) o a las
[cadenas de bloques](https://creaproject.io/crea-es/).

Los retos técnicos y legales que pueden representar el versionado es una
cuestión que en el desarrollo de _software_ se ha estado trabajando
desde hace mucho tiempo. Por un lado, están las licencias de uso de
_software_ que simplifican las cuestiones legales. Por el otro, en el 
desarrollo de _software_ las versiones no son un reto técnico porque:

-   solo se produce un ejecutable (para Windows, Mac o Linux);
-   o se producen diferentes ejecutables a partir del mismo código
    fuente;
-   o se tienen equipos independientes según la plataforma (un equipo
    para Android, otro para iOS).

En la edición, la producción de un solo formato de por sí ya es un reto
debido a la falta de capacitación o de organización de los archivos. Las
concepciones metodológicas multiformato, como la [edición ramificada](@index[9]),
son prácticamente desconocidas o ignoradas, por lo que no es posible
crear diferentes formatos a partir de los mismos archivos madre. Solo
queda la publicación de distintos formatos de manera independiente que,
en muchos casos, es un despilfarro de recursos.

En el ámbito de _software_ solo grandes empresas u organizaciones, o
grupos de trabajo muy comprometidos, tienen los recursos suficientes
para poder tener equipos independientes según la plataforma. Por
ejemplo, Snapchat o Telegram tienen personal destinado para plataformas
específicas; o bien, la comunidad de Linux que se encarga de adecuar el
kernel según el tipo de arquitectura.

Si hacemos la analogía, en el ámbito editorial los únicos con la
capacidad de actualizar sus formatos con pocos inconvenientes son las
grandes casas editoriales. Por ejemplo, a Penguin Random House o a Grupo
Planeta el uso de versiones más que un reto, sería un _plus_ a sus
ediciones, porque tienen la capacidad de destinar distintos equipos.
Pero a la mayoría de las editoriales ---medianas, pequeñas o
independientes--- y, sin duda, a los autores que se autopublican este
modo de producción no es el más conveniente.

**El uso de versiones entre pequeños editores solo será factible cuando
exista un cambio metodológico de fondo**. Pero cuando eso sea posible,
quizá el uso de versiones ya no sea necesario porque existe un modelo de
desarrollo que podría ser más apto: el _rolling release_, que bien
podemos llamar «edición continua».

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «¿Completud de una obra?».
* Título en el _blog_ de Mariana: «Completud de una obra: ¿se acaba de editar con la publicación de un libro?».
* Fecha de publicación: 20 de febrero del 2018.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/16%20-%20Completud%20de%20una%20obra/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/completud-de-una-obra-se-acaba-de-editar-con-la-publicacion-de-un-libro).

</aside>
